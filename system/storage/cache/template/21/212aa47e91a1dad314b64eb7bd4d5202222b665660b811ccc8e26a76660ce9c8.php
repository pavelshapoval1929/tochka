<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/extension/seopack.twig */
class __TwigTemplate_c52bab0d46b6e527e5e4a40cf2826992c6de535b014e02b1ed5ead545b3b8533 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " ";
        echo ($context["column_left"] ?? null);
        echo " 

<div id=\"content\"> 
 \t<link type=\"text/css\" href=\"view/stylesheet/stylesheet2.css\" rel=\"stylesheet\" media=\"screen\" />
\t  <div class=\"breadcrumb\">
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
    ";
            // line 7
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "separator", [], "any", false, false, false, 7);
            echo " <a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 7);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 7);
            echo " </a>
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo " 
  </div>
  ";
        // line 10
        if (($context["error_warning"] ?? null)) {
            echo " 
  <div class=\"warning\">";
            // line 11
            echo ($context["error_warning"] ?? null);
            echo " </div>
  ";
        }
        // line 12
        echo " 
  ";
        // line 13
        if (($context["success"] ?? null)) {
            echo " 
  <div class=\"success\">";
            // line 14
            echo ($context["success"] ?? null);
            echo " </div>
  ";
        }
        // line 15
        echo " 
\t<div class=\"box\">
    <div class=\"heading\">
      <h1><img src=\"view/image/review.png\" alt=\"\" /> ";
        // line 18
        echo ($context["heading_title"] ?? null);
        echo " </h1>
\t<div class=\"buttons\"><a onclick=\"\$('#form').submit();\" class=\"button\">Save Parameters</a></div>
\t</div>
    <div class=\"content\">
\t<form action=\"";
        // line 22
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
\t\t\t<input type=\"hidden\" name=\"seopack_parameters[gkey]\" value=\"";
        // line 23
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 23)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 23)) : (($context["gkey"] ?? null)));
        echo "\">
\t\t   <table class=\"list\">
            <thead>
              <tr>
                <td class=\"left\" width=\"200\">Extension</td>
\t\t\t\t<td class=\"left\">About</td>
\t\t\t\t<td class=\"left\" width=\"50\">Parameters</td>
\t\t\t\t<td class=\"right\" width=\"100\">Action</td>
              </tr>
            </thead>
            
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Keywords Generator</b></td>
                <td class=\"left\"><span class=\"help\">Keywords Generator generates meta keywords from relevant words from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can add keywords from product's model(%m), sku(%s), upc(%u) or brand(%b).<br>
\t\t\t\tAvailable parameters: %p, %c, %m, %s and %u. Use them withat spaces or any other characters.<br>
\t\t\t\t<b>Example: %p%c%m%u</b> - will generate keywords from product name, category name, model and product's upc.<br>\t\t\t\t
\t\t\t\t<i>Before generating keywords, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t </span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[keywords]\" value=\"";
        // line 43
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "keywords", [], "any", false, false, false, 43);
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/keywords_generator.php?gkey=";
        // line 46
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 46);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Meta Description Generator</b></td>
                <td class=\"left\"><span class=\"help\"> Meta Description Generator generates meta description for products using a pattern which is set in Parameters.<br>
\t\t\t\tThe default pattern is '%p - %f' which means product's name, followed by ' - ', followed by the first sentence from product's description.<br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThe are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand, <b>%\$</b> - product's price and <b>%f</b> - the first sentence from product's description.<br>
\t\t\t\t<b>Example: %p (%m) - %f (by www.mysite.com)</b> - will generate the following meta description for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) - The first sentence from iPod's description. (by www.mysite.com)</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating meta descriptions, if you have modified parameters, don't forget to save them using Save Parameters button.</i>
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[metas]\" value=\"";
        // line 62
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "metas", [], "any", false, false, false, 62);
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/meta_description_generator.php?gkey=";
        // line 64
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 64);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>\t\t\t\t
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Alt Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Alt Tags Generator generates custom alts for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom alt for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom alts, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[calts]\" value=\"";
        // line 79
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "calts", [], "any", false, false, false, 79)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "calts", [], "any", false, false, false, 79)) : (""));
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_alt_generator.php?gkey=";
        // line 82
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 82);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom H1 Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom H1 Generator generates custom h1s for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom h1 for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom h1s, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ch1s]\" value=\"";
        // line 97
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ch1s", [], "any", false, false, false, 97)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ch1s", [], "any", false, false, false, 97)) : (""));
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_h1_generator.php?gkey=";
        // line 100
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 100);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom H2 Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom H2 Generator generates custom h2s for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom h2 for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom h2s, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ch2s]\" value=\"";
        // line 115
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ch2s", [], "any", false, false, false, 115)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ch2s", [], "any", false, false, false, 115)) : (""));
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_h2_generator.php?gkey=";
        // line 118
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 118);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Image Title Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Image Title Tags Generator generates custom imgtitles for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom imgtitle for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom imgtitles, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[cimgtitles]\" value=\"";
        // line 133
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "cimgtitles", [], "any", false, false, false, 133)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "cimgtitles", [], "any", false, false, false, 133)) : (""));
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_imgtitle_generator.php?gkey=";
        // line 136
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 136);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Meta Titles Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Titles Generator generates custom titles for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom title for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom titles, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ctitles]\" value=\"";
        // line 151
        echo ((twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ctitles", [], "any", false, false, false, 151)) ? (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ctitles", [], "any", false, false, false, 151)) : (""));
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_title_generator.php?gkey=";
        // line 154
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 154);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Tag Generator generates product tags from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can add tags from product's model(%m), sku(%s) upc(%u) or brand(%b).<br>
\t\t\t\tAvailable parameters: %p, %c, %m, %s and %u. Use them withat spaces or any other characters.<br>
\t\t\t\t<b>Example: %p%c%m</b> - will generate tags from product name, category name and model.<br>\t\t\t\t
\t\t\t\t<i>Before generating tags, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[tags]\" value=\"";
        // line 169
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "tags", [], "any", false, false, false, 169);
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/tag_generator.php?gkey=";
        // line 171
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 171);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Related Products Generator</b></td>
                <td class=\"left\"><span class=\"help\"> Related Products Generator, based on a complex algorithm, is a powerful tool which generates up to 5 related product for each product.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can change the default number (5) of related products in parameters field for Related Products Generator. <br>
\t\t\t\t<i>Before generating related products, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t\t\t\t
\t\t\t\t</span></td>
\t\t\t\t<td class=\"left\"><input type=\"text\" name=\"seopack_parameters[related]\" value=\"";
        // line 185
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "related", [], "any", false, false, false, 185);
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/rp_generator.php?gkey=";
        // line 187
        echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "gkey", [], "any", false, false, false, 187);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>SEO Urls Generator</b></td>
                <td class=\"left\"><span class=\"help\"> SEO URLS Generator generates SEO URLS for products, categories, manufacturers and information. 
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can change the default extension (.html) of generated urls. <br>
\t\t\t\t<i>Before generating SEO URLs, if you have modified parameters, don't forget to save them using Save Parameters button.</i></span></td>
\t\t\t\t<td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ext]\" value=\"";
        // line 200
        if (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ext", [], "any", false, false, false, 200)) {
            echo twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "ext", [], "any", false, false, false, 200);
        }
        echo "\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = '";
        // line 202
        echo ($context["seourls"] ?? null);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
            
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>SEO Friendly Urls Generator</b></td>
                <td class=\"left\"><span class=\"help\"> SEO Friendly URLs Generator transforms non-SEO friendly links like:<br>
\t\t\t\t<i>yoursite.com/index.php?route=account/login</i><br>
\t\t\t\tinto SEO friendly links:<br>
\t\t\t\t<i>yoursite.com/login</i></span></td>
                <td class=\"right\" colspan=\"2\">
\t\t\t\t\t<a onclick=\"location = '";
        // line 216
        echo ($context["friendlyurls"] ?? null);
        echo "'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
            
            <tbody>
              <tr>
                <td class=\"left\"><b>Clear SEO</b></td>
                <td class=\"left\">
\t\t\t\t\t<span class=\"help\"> With Clear SEO you can easily delete ALL:<br><br>
\t\t\t\t\t- product tags<br>
\t\t\t\t\t- meta descriptions<br>
\t\t\t\t\t- meta keywords<br>
\t\t\t\t\t- seo urls<br>
\t\t\t\t\t- related products<br>
\t\t\t\t\t<br>
\t\t\t\t\t<span style=\"color:red\">A database backup is recommended before using Clear SEO, because you may lose SEO data!</span><br>
\t\t\t\t\t</span></td>
                <td class=\"right\" colspan=\"2\">
\t\t\t\t\t<p><a onclick=\"clearseo('Products Keywords', '";
        // line 236
        echo ($context["clearkeywords"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Keywords</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Products Meta Descriptions', '";
        // line 237
        echo ($context["clearmetas"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Meta Description</a></p>
<p><a onclick=\"clearseo('Product Custom Alt Tags', '";
        // line 238
        echo ($context["clearalts"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Custom Alt Tags</a></p>
<p><a onclick=\"clearseo('Product Custom H1s', '";
        // line 239
        echo ($context["clearh1s"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Custom H1s</a></p>
<p><a onclick=\"clearseo('Product Custom H2s', '";
        // line 240
        echo ($context["clearh2s"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Custom H2s</a></p>
<p><a onclick=\"clearseo('Product Custom Image Title Tags', '";
        // line 241
        echo ($context["clearimgtitles"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Custom Image Title Tags</a></p>
<p><a onclick=\"clearseo('Product Custom Titles', '";
        // line 242
        echo ($context["cleartitles"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Custom Titles</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Products Tags', '";
        // line 243
        echo ($context["cleartags"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Tags</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('SEO Urls', '";
        // line 244
        echo ($context["clearurls"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear SEO Urls</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Related Products', '";
        // line 245
        echo ($context["clearproducts"] ?? null);
        echo "');\" class=\"button\" style=\"background:red\">Clear Related Products</a>
\t\t\t\t</td>
              </tr>
            </tbody>
            
            

\t\t\t<tbody>
              <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create SEO URLS for products/categories/brands/info on insert if seo urls don't exist\t\t\t\t
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t";
        // line 258
        if (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "autourls", [], "any", false, false, false, 258)) {
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"0\" /> No
                ";
        } else {
            // line 261
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"0\" checked=\"checked\" /> No
                ";
        }
        // line 265
        echo "\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td  class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Meta Keywords for products on insert if meta keywords don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t";
        // line 272
        if (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "autokeywords", [], "any", false, false, false, 272)) {
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"0\" /> No
               ";
        } else {
            // line 275
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"0\" checked=\"checked\" /> No
               ";
        }
        // line 279
        echo "\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Meta Descriptions for products on insert if meta descriptions don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t";
        // line 286
        if (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "autometa", [], "any", false, false, false, 286)) {
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"0\" /> No
                ";
        } else {
            // line 289
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"0\" checked=\"checked\" /> No
                ";
        }
        // line 293
        echo "\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Tags for products on insert if tags don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t";
        // line 300
        if (twig_get_attribute($this->env, $this->source, ($context["seopack_parameters"] ?? null), "autotags", [], "any", false, false, false, 300)) {
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"0\" /> No
                ";
        } else {
            // line 303
            echo " 
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"0\" checked=\"checked\" /> No
                ";
        }
        // line 307
        echo "\t\t\t\t</td>
\t\t\t  </tr>\t\t\t  
            </tbody>
\t\t\t
          </table>
\t</form>
\t<span style=\"color:red\" class=\"help\">* If you want to generate custom h1 tags for products for example, make sure you will:<br>
\t\t- Set a parameter for <b>Product Custom H1 Generator</b> (eg. %p)<br>
\t\t- Click on <b>Save Parameters</b> button.<br>
\t\t- Get the \"<b>Success: You have successfully saved parameters!</b>\" message.<br>
\t\t- Click on generator's <b>Generate</b> button<br>
\t\tSame for the other SEO generators.</span>
\t</div>
   </div>

<script type=\"text/javascript\">
\t\t\t\tfunction clearseo(data, link){\t\t\t\t\t\t
\t\t\t\t\tif (!confirm('Are you sure you want to delete ALL ' + data + '?\\n\\nA database backup is recommended! \\n\\nThis action will delete ALL ' + data + '!!!')) 
\t\t\t\t\t\t{
\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t}
\t\t\t\t\t\telse 
\t\t\t\t\t\t{
\t\t\t\t\t\t\tlocation = link;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t</script>

";
        // line 336
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "extension/extension/seopack.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  549 => 336,  518 => 307,  512 => 303,  505 => 300,  496 => 293,  490 => 289,  483 => 286,  474 => 279,  468 => 275,  461 => 272,  452 => 265,  446 => 261,  439 => 258,  423 => 245,  419 => 244,  415 => 243,  411 => 242,  407 => 241,  403 => 240,  399 => 239,  395 => 238,  391 => 237,  387 => 236,  364 => 216,  347 => 202,  340 => 200,  324 => 187,  319 => 185,  302 => 171,  297 => 169,  279 => 154,  273 => 151,  255 => 136,  249 => 133,  231 => 118,  225 => 115,  207 => 100,  201 => 97,  183 => 82,  177 => 79,  159 => 64,  154 => 62,  135 => 46,  129 => 43,  106 => 23,  102 => 22,  95 => 18,  90 => 15,  85 => 14,  81 => 13,  78 => 12,  73 => 11,  69 => 10,  65 => 8,  53 => 7,  47 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }} {{ column_left }} 

<div id=\"content\"> 
 \t<link type=\"text/css\" href=\"view/stylesheet/stylesheet2.css\" rel=\"stylesheet\" media=\"screen\" />
\t  <div class=\"breadcrumb\">
    {% for breadcrumb in breadcrumbs %} 
    {{ breadcrumb.separator }} <a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }} </a>
     {% endfor %} 
  </div>
  {% if error_warning %} 
  <div class=\"warning\">{{ error_warning }} </div>
  {% endif %} 
  {% if success %} 
  <div class=\"success\">{{ success }} </div>
  {% endif %} 
\t<div class=\"box\">
    <div class=\"heading\">
      <h1><img src=\"view/image/review.png\" alt=\"\" /> {{ heading_title }} </h1>
\t<div class=\"buttons\"><a onclick=\"\$('#form').submit();\" class=\"button\">Save Parameters</a></div>
\t</div>
    <div class=\"content\">
\t<form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
\t\t\t<input type=\"hidden\" name=\"seopack_parameters[gkey]\" value=\"{{ seopack_parameters.gkey ? seopack_parameters.gkey :  gkey}}\">
\t\t   <table class=\"list\">
            <thead>
              <tr>
                <td class=\"left\" width=\"200\">Extension</td>
\t\t\t\t<td class=\"left\">About</td>
\t\t\t\t<td class=\"left\" width=\"50\">Parameters</td>
\t\t\t\t<td class=\"right\" width=\"100\">Action</td>
              </tr>
            </thead>
            
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Keywords Generator</b></td>
                <td class=\"left\"><span class=\"help\">Keywords Generator generates meta keywords from relevant words from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can add keywords from product's model(%m), sku(%s), upc(%u) or brand(%b).<br>
\t\t\t\tAvailable parameters: %p, %c, %m, %s and %u. Use them withat spaces or any other characters.<br>
\t\t\t\t<b>Example: %p%c%m%u</b> - will generate keywords from product name, category name, model and product's upc.<br>\t\t\t\t
\t\t\t\t<i>Before generating keywords, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t </span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[keywords]\" value=\"{{ seopack_parameters.keywords }}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/keywords_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Meta Description Generator</b></td>
                <td class=\"left\"><span class=\"help\"> Meta Description Generator generates meta description for products using a pattern which is set in Parameters.<br>
\t\t\t\tThe default pattern is '%p - %f' which means product's name, followed by ' - ', followed by the first sentence from product's description.<br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThe are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand, <b>%\$</b> - product's price and <b>%f</b> - the first sentence from product's description.<br>
\t\t\t\t<b>Example: %p (%m) - %f (by www.mysite.com)</b> - will generate the following meta description for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) - The first sentence from iPod's description. (by www.mysite.com)</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating meta descriptions, if you have modified parameters, don't forget to save them using Save Parameters button.</i>
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[metas]\" value=\"{{ seopack_parameters.metas}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/meta_description_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>\t\t\t\t
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Alt Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Alt Tags Generator generates custom alts for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom alt for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom alts, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[calts]\" value=\"{{ seopack_parameters.calts ? seopack_parameters.calts}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_alt_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom H1 Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom H1 Generator generates custom h1s for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom h1 for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom h1s, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ch1s]\" value=\"{{ seopack_parameters.ch1s ? seopack_parameters.ch1s}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_h1_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom H2 Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom H2 Generator generates custom h2s for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom h2 for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom h2s, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ch2s]\" value=\"{{ seopack_parameters.ch2s ? seopack_parameters.ch2s}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_h2_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Image Title Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Image Title Tags Generator generates custom imgtitles for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom imgtitle for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom imgtitles, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[cimgtitles]\" value=\"{{ seopack_parameters.cimgtitles ? seopack_parameters.cimgtitles}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_imgtitle_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
<td class=\"left\"><b>Product Custom Meta Titles Generator</b></td>
                <td class=\"left\"><span class=\"help\">Product Custom Titles Generator generates custom titles for products from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tThere are available the following parameters and they will be replaced by their actual value: <b>%p</b> - product's name, <b>%c</b> - category's name, <b>%m</b> - model, <b>%s</b> - product's sku, <b>%u</b> - product's upc, <b>%b</b> - product's brand and <b>%\$</b> - product's price.<br>
\t\t\t\t<b>Example: %p (%m) by www.mysite.com</b> - will generate the following cutom title for a product called 'iPod' with model = 'iPod4': <b>iPod (iPod4) by www.mysite.com</b>.<br>\t\t\t\t
\t\t\t\t<i>Before generating custom titles, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ctitles]\" value=\"{{ seopack_parameters.ctitles ? seopack_parameters.ctitles}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/custom_title_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Tags Generator</b></td>
                <td class=\"left\"><span class=\"help\">Tag Generator generates product tags from relevant keywords from product(%p) and category(%c) names.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can add tags from product's model(%m), sku(%s) upc(%u) or brand(%b).<br>
\t\t\t\tAvailable parameters: %p, %c, %m, %s and %u. Use them withat spaces or any other characters.<br>
\t\t\t\t<b>Example: %p%c%m</b> - will generate tags from product name, category name and model.<br>\t\t\t\t
\t\t\t\t<i>Before generating tags, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t
\t\t\t\t</span></td>
                <td class=\"left\"><input type=\"text\" name=\"seopack_parameters[tags]\" value=\"{{ seopack_parameters.tags}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/tag_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t 
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>Related Products Generator</b></td>
                <td class=\"left\"><span class=\"help\"> Related Products Generator, based on a complex algorithm, is a powerful tool which generates up to 5 related product for each product.<br><br>
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can change the default number (5) of related products in parameters field for Related Products Generator. <br>
\t\t\t\t<i>Before generating related products, if you have modified parameters, don't forget to save them using Save Parameters button.</i>\t\t\t\t
\t\t\t\t</span></td>
\t\t\t\t<td class=\"left\"><input type=\"text\" name=\"seopack_parameters[related]\" value=\"{{ seopack_parameters.related}}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = 'controller/extension/extension/rp_generator.php?gkey={{ seopack_parameters.gkey}}'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
\t\t\t
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>SEO Urls Generator</b></td>
                <td class=\"left\"><span class=\"help\"> SEO URLS Generator generates SEO URLS for products, categories, manufacturers and information. 
\t\t\t\t<b>Parameters</b><br>
\t\t\t\tYou can change the default extension (.html) of generated urls. <br>
\t\t\t\t<i>Before generating SEO URLs, if you have modified parameters, don't forget to save them using Save Parameters button.</i></span></td>
\t\t\t\t<td class=\"left\"><input type=\"text\" name=\"seopack_parameters[ext]\" value=\"{% if seopack_parameters.ext %}{{ seopack_parameters.ext}}{% endif %}\" size=\"10\"/></td>
                <td class=\"right\">
\t\t\t\t\t<a onclick=\"location = '{{ seourls }}'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
            
\t\t\t<tbody>
              <tr>
                <td class=\"left\"><b>SEO Friendly Urls Generator</b></td>
                <td class=\"left\"><span class=\"help\"> SEO Friendly URLs Generator transforms non-SEO friendly links like:<br>
\t\t\t\t<i>yoursite.com/index.php?route=account/login</i><br>
\t\t\t\tinto SEO friendly links:<br>
\t\t\t\t<i>yoursite.com/login</i></span></td>
                <td class=\"right\" colspan=\"2\">
\t\t\t\t\t<a onclick=\"location = '{{ friendlyurls }}'\" class=\"button\">Generate</a>
\t\t\t\t\t \t\t\t\t\t
\t\t\t\t</td>
              </tr>
            </tbody>
            
            <tbody>
              <tr>
                <td class=\"left\"><b>Clear SEO</b></td>
                <td class=\"left\">
\t\t\t\t\t<span class=\"help\"> With Clear SEO you can easily delete ALL:<br><br>
\t\t\t\t\t- product tags<br>
\t\t\t\t\t- meta descriptions<br>
\t\t\t\t\t- meta keywords<br>
\t\t\t\t\t- seo urls<br>
\t\t\t\t\t- related products<br>
\t\t\t\t\t<br>
\t\t\t\t\t<span style=\"color:red\">A database backup is recommended before using Clear SEO, because you may lose SEO data!</span><br>
\t\t\t\t\t</span></td>
                <td class=\"right\" colspan=\"2\">
\t\t\t\t\t<p><a onclick=\"clearseo('Products Keywords', '{{ clearkeywords }}');\" class=\"button\" style=\"background:red\">Clear Keywords</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Products Meta Descriptions', '{{ clearmetas }}');\" class=\"button\" style=\"background:red\">Clear Meta Description</a></p>
<p><a onclick=\"clearseo('Product Custom Alt Tags', '{{ clearalts  }}');\" class=\"button\" style=\"background:red\">Clear Custom Alt Tags</a></p>
<p><a onclick=\"clearseo('Product Custom H1s', '{{ clearh1s }}');\" class=\"button\" style=\"background:red\">Clear Custom H1s</a></p>
<p><a onclick=\"clearseo('Product Custom H2s', '{{ clearh2s  }}');\" class=\"button\" style=\"background:red\">Clear Custom H2s</a></p>
<p><a onclick=\"clearseo('Product Custom Image Title Tags', '{{ clearimgtitles  }}');\" class=\"button\" style=\"background:red\">Clear Custom Image Title Tags</a></p>
<p><a onclick=\"clearseo('Product Custom Titles', '{{ cleartitles }}');\" class=\"button\" style=\"background:red\">Clear Custom Titles</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Products Tags', '{{ cleartags }}');\" class=\"button\" style=\"background:red\">Clear Tags</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('SEO Urls', '{{ clearurls }}');\" class=\"button\" style=\"background:red\">Clear SEO Urls</a></p>
\t\t\t\t\t<p><a onclick=\"clearseo('Related Products', '{{ clearproducts }}');\" class=\"button\" style=\"background:red\">Clear Related Products</a>
\t\t\t\t</td>
              </tr>
            </tbody>
            
            

\t\t\t<tbody>
              <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create SEO URLS for products/categories/brands/info on insert if seo urls don't exist\t\t\t\t
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t{% if seopack_parameters.autourls %} 
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"0\" /> No
                {% else %} 
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autourls]\" value=\"0\" checked=\"checked\" /> No
                {% endif %}
\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td  class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Meta Keywords for products on insert if meta keywords don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t{% if seopack_parameters.autokeywords %} 
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"0\" /> No
               {% else %} 
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autokeywords]\" value=\"0\" checked=\"checked\" /> No
               {% endif %}
\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Meta Descriptions for products on insert if meta descriptions don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t{% if seopack_parameters.autometa %} 
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"0\" /> No
                {% else %} 
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autometa]\" value=\"0\" checked=\"checked\" /> No
                {% endif %}
\t\t\t\t</td>
\t\t\t  </tr>
\t\t\t  <tr>
                <td class=\"left\" colspan=\"2\">
\t\t\t\tAuto create Tags for products on insert if tags don't exist
\t\t\t\t</td>
\t\t\t\t<td class=\"right\" colspan=\"2\">
\t\t\t\t{% if seopack_parameters.autotags %} 
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"1\" checked=\"checked\" /> Yes                
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"0\" /> No
                {% else %} 
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"1\" /> Yes
                <input type=\"radio\" name=\"seopack_parameters[autotags]\" value=\"0\" checked=\"checked\" /> No
                {% endif %}
\t\t\t\t</td>
\t\t\t  </tr>\t\t\t  
            </tbody>
\t\t\t
          </table>
\t</form>
\t<span style=\"color:red\" class=\"help\">* If you want to generate custom h1 tags for products for example, make sure you will:<br>
\t\t- Set a parameter for <b>Product Custom H1 Generator</b> (eg. %p)<br>
\t\t- Click on <b>Save Parameters</b> button.<br>
\t\t- Get the \"<b>Success: You have successfully saved parameters!</b>\" message.<br>
\t\t- Click on generator's <b>Generate</b> button<br>
\t\tSame for the other SEO generators.</span>
\t</div>
   </div>

<script type=\"text/javascript\">
\t\t\t\tfunction clearseo(data, link){\t\t\t\t\t\t
\t\t\t\t\tif (!confirm('Are you sure you want to delete ALL ' + data + '?\\n\\nA database backup is recommended! \\n\\nThis action will delete ALL ' + data + '!!!')) 
\t\t\t\t\t\t{
\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t}
\t\t\t\t\t\telse 
\t\t\t\t\t\t{
\t\t\t\t\t\t\tlocation = link;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t</script>

{{ footer }} ", "extension/extension/seopack.twig", "");
    }
}
