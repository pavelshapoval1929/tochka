<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/category.twig */
class __TwigTemplate_f4369332398123f752b8bc5153bd132e4b8c56d57bad979463f24feb8aa0e905 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"list-group list-block categories-list\">
  ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 3
            echo "      ";
            // line 4
            echo "      ";
            // line 7
            echo "      ";
            if ((twig_get_attribute($this->env, $this->source, $context["category"], "category_id", [], "any", false, false, false, 7) && (twig_get_attribute($this->env, $this->source, $context["category"], "page", [], "any", false, false, false, 7) != "front_page"))) {
                // line 8
                echo "          <div class=\"parent-block\">
        <a href=\"";
                // line 9
                echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 9);
                echo "\" class=\"list-group-item active parent-cat\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 9);
                echo "</a>

          ";
                // line 11
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 11)) {
                    // line 12
                    echo "            <div class=\"children-cats\">
            ";
                    // line 13
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 13));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 14
                        echo "                ";
                        if ((twig_get_attribute($this->env, $this->source, $context["child"], "category_id", [], "any", false, false, false, 14) == ($context["child_id"] ?? null))) {
                            // line 15
                            echo "                    <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 15);
                            echo "\" class=\"list-group-item active 2\">&nbsp;&nbsp;&nbsp;- ";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 15);
                            echo "</a>
                ";
                        } else {
                            // line 17
                            echo "                    <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 17);
                            echo "\" class=\"list-group-item 3\">&nbsp;&nbsp;&nbsp;- ";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 17);
                            echo "</a>
                ";
                        }
                        // line 19
                        echo "            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 20
                    echo "            </div>
          ";
                }
                // line 22
                echo "          </div>
      ";
            } else {
                // line 23
                echo " ";
                // line 24
                echo "      <div class=\"category-block\">
          <a href=\"";
                // line 25
                echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 25);
                echo "\" class=\"list-group-item category-front 4\">
              <img src=\"";
                // line 26
                echo twig_get_attribute($this->env, $this->source, $context["category"], "image", [], "any", false, false, false, 26);
                echo "\" alt=\"\" class=\"category-img\">
              <span class=\"cat-name\">";
                // line 27
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 27);
                echo "</span>
          </a>
      </div>
    ";
            }
            // line 31
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 32,  123 => 31,  116 => 27,  112 => 26,  108 => 25,  105 => 24,  103 => 23,  99 => 22,  95 => 20,  89 => 19,  81 => 17,  73 => 15,  70 => 14,  66 => 13,  63 => 12,  61 => 11,  54 => 9,  51 => 8,  48 => 7,  46 => 4,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"list-group list-block categories-list\">
  {% for category in categories %}
      {#{% if category.category_id == category_id %}#}
      {#<pre>
      {{  dump(category)}}
      </pre>#}
      {% if (category.category_id) and (category.page != 'front_page') %}
          <div class=\"parent-block\">
        <a href=\"{{ category.href }}\" class=\"list-group-item active parent-cat\">{{ category.name }}</a>

          {% if category.children %}
            <div class=\"children-cats\">
            {% for child in category.children %}
                {% if child.category_id == child_id %}
                    <a href=\"{{ child.href }}\" class=\"list-group-item active 2\">&nbsp;&nbsp;&nbsp;- {{ child.name }}</a>
                {% else %}
                    <a href=\"{{ child.href }}\" class=\"list-group-item 3\">&nbsp;&nbsp;&nbsp;- {{ child.name }}</a>
                {% endif %}
            {% endfor %}
            </div>
          {% endif %}
          </div>
      {% else %} {#{{ dump(category.image) }}#}
      <div class=\"category-block\">
          <a href=\"{{ category.href }}\" class=\"list-group-item category-front 4\">
              <img src=\"{{ category.image }}\" alt=\"\" class=\"category-img\">
              <span class=\"cat-name\">{{ category.name }}</span>
          </a>
      </div>
    {% endif %}
  {% endfor %}
</div>
", "default/template/extension/module/category.twig", "");
    }
}
