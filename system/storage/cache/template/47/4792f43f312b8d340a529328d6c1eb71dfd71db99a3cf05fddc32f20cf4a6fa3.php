<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/brainyfilter.twig */
class __TwigTemplate_93864fc0bf122048bba61fe81705474d977e0735ff61207b5f0af2b8f3e674fe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    ";
        // line 9
        if (($context["success"] ?? null)) {
            // line 10
            echo "        <div class=\"alert alert-success\">";
            echo ($context["success"] ?? null);
            echo "</div>
    ";
        }
        // line 12
        echo "    ";
        if (twig_length_filter($this->env, ($context["error_warning"] ?? null))) {
            // line 13
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["error_warning"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 14
                echo "            <div class=\"warning\">";
                echo $context["err"];
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "    ";
        }
        // line 17
        echo "    <div class=\"box\">
        <div class=\"heading page-header\">
            <div class=\"container-fluid\">
                <h1>";
        // line 20
        echo ($context["heading_title"] ?? null);
        echo "</h1>
                <ul class=\"breadcrumb\">
                    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 23
            echo "                    <li><a href=\"";
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
            echo "\">";
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                </ul>
                <div class=\"pull-right\">
                    <a onclick=\"jQuery('[name=action]').val('apply');BF.submitForm();\" class=\"btn btn-success\" data-toggle=\"tooltip\" title=\"";
        // line 27
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "button_save", [], "any", false, false, false, 27);
        echo "\"><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"BF.submitForm();\" class=\"btn btn-primary\" data-toggle=\"tooltip\" title=\"";
        // line 28
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "button_save_n_close", [], "any", false, false, false, 28);
        echo "\"><span class=\"icon\"></span><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"location = '";
        // line 29
        echo ($context["cancel"] ?? null);
        echo "';\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "button_cancel", [], "any", false, false, false, 29);
        echo "\"><i class=\"fa fa-reply\"></i></a>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 35
        echo "    <form action=\"";
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"application/x-www-form-urlencoded\" id=\"form\">
        <input type=\"hidden\" name=\"action\" value=\"save\" />
        <input type=\"hidden\" name=\"bf\" value=\"\" />
    </form>
    <form action=\"\" id=\"bf-form\" class=\"container-fluid\">
        <input type=\"hidden\" name=\"bf[module_id]\" value=\"";
        // line 40
        echo ((($context["isNewInstance"] ?? null)) ? ("new") : (($context["moduleId"] ?? null)));
        echo "\" />
        <input type=\"hidden\" name=\"bf[current_adm_tab]\" value=\"";
        // line 41
        echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["settings"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["current_adm_tab"] ?? null) : null);
        echo "\" />
        ";
        // line 43
        echo "        <div id=\"bf-adm-main-menu\">
            <ul class=\"clearfix\">
                <li class=\"";
        // line 45
        if ((($context["moduleId"] ?? null) === "basic")) {
            echo " selected ";
        }
        echo "\">
                    <div>
                        <a href=\"";
        // line 47
        echo (($context["instanceUrl"] ?? null) . "basic");
        echo "\">
                            <span class=\"icon basic\"></span>
                            ";
        // line 49
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "top_menu_basic_settings", [], "any", false, false, false, 49);
        echo "
                        </a>
                    </div>
                </li>
                <li class=\"";
        // line 53
        if ((($context["moduleId"] ?? null) && (($context["moduleId"] ?? null) != "basic"))) {
            echo " selected ";
        }
        echo "\">
                    <div class=\"dropdown\">
                        <a id=\"dLabel\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            <span class=\"icon layouts\"></span>
                            ";
        // line 57
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "top_menu_module_instances", [], "any", false, false, false, 57);
        echo "
                        </a>
                        <ul class=\"dropdown-menu\" aria-labelledby=\"dLabel\">
                            ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 61
            echo "                            <li>
                                <a href=\"";
            // line 62
            echo (($context["instanceUrl"] ?? null) . (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["module"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["module_id"] ?? null) : null));
            echo "\" ";
            if ((($context["moduleId"] ?? null) == (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["module"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["module_id"] ?? null) : null))) {
                echo " class=\"bf-selected\" ";
            }
            echo ">
                                    ";
            // line 63
            echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["module"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["name"] ?? null) : null);
            echo "
                                </a>
                            </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"";
        // line 68
        echo (($context["instanceUrl"] ?? null) . "new");
        echo "\" ";
        if ((($context["moduleId"] ?? null) == "new")) {
            echo " class=\"bf-selected\" ";
        }
        echo ">
                                    <i class=\"fa fa-plus\"></i> ";
        // line 69
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "top_menu_add_new_instance", [], "any", false, false, false, 69);
        echo "</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <div class=\"clearfix\"></div>
        </div>
        ";
        // line 78
        echo "        
        <div id=\"bf-adm-main-container\">
            
            ";
        // line 82
        echo "            <div id=\"bf-adm-basic-settings\" class=\"tab-content\" data-group=\"main\" style=\"display:block\">
                ";
        // line 83
        if ((($context["moduleId"] ?? null) === "basic")) {
            // line 84
            echo "                <p class=\"bf-info\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_basic_settings_info", [], "any", false, false, false, 84);
            echo "</p>
                ";
        } elseif ((        // line 85
($context["moduleId"] ?? null) === "new")) {
            // line 86
            echo "                <p class=\"bf-info\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_new_layout_notice", [], "any", false, false, false, 86);
            echo "</p>
                ";
        }
        // line 88
        echo "                <div class=\"bf-panel\">
                    <div id=\"bf-create-instance-alert\" class=\"bf-alert\">
                        ";
        // line 90
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "message_new_instance", [], "any", false, false, false, 90);
        echo "
                    </div>
                    ";
        // line 92
        if ((($context["moduleId"] ?? null) != "basic")) {
            // line 93
            echo "                    <div class=\"bf-panel-row clearfix\">
                        <div class=\"bf-notice\"></div>
                    </div>
                    ";
        }
        // line 97
        echo "                    <div class=\"tab-content-inner\">
                        ";
        // line 98
        if ((($context["moduleId"] ?? null) != "basic")) {
            // line 99
            echo "                        <div class=\"bf-panel-row bf-local-settings clearfix\">
                            <div class=\"left\">
                                <label for=\"bf-layout-id\">";
            // line 101
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_layout", [], "any", false, false, false, 101);
            echo "</label>
                                <select name=\"bf[layout_id]\" id=\"bf-layout-id\" class=\"bf-layout-select bf-w195\">
                                    <option value=\"0\" selected=\"selected\">";
            // line 103
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "select", [], "any", false, false, false, 103);
            echo "</option>
                                    ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
            foreach ($context['_seq'] as $context["id"] => $context["layout"]) {
                // line 105
                echo "                                        <option value=\"";
                echo $context["id"];
                echo "\">";
                echo $context["layout"];
                echo "</option>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['id'], $context['layout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-position\">";
            // line 110
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_position", [], "any", false, false, false, 110);
            echo "</label>
                                <select name=\"bf[layout_position]\" id=\"bf-layout-position\" class=\"bf-layout-position bf-w195\">
                                    <option value=\"content_top\">";
            // line 112
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_content_top", [], "any", false, false, false, 112);
            echo "</option>
                                    <option value=\"content_bottom\">";
            // line 113
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_content_bottom", [], "any", false, false, false, 113);
            echo "</option>
                                    <option value=\"column_left\">";
            // line 114
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_column_left", [], "any", false, false, false, 114);
            echo "</option>
                                    <option value=\"column_right\">";
            // line 115
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_column_right", [], "any", false, false, false, 115);
            echo "</option>
                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-sort-order\">";
            // line 119
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "sort_order", [], "any", false, false, false, 119);
            echo "</label>
                                <input type=\"text\" name=\"bf[layout_sort_order]\" id=\"bf-layout-sort-order\" class=\"bf-layout-sort bf-w65\" />
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label center\">";
            // line 123
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 123);
            echo "</span>
                                <div class=\"bf-layout-enable yesno\">
                                    <span class=\"bf-switcher\">
                                        <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[layout_enabled]\" value=\"0\" />
                                        <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                    </span>
                                </div>
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label\">&nbsp;</span>
                                <a href=\"";
            // line 133
            echo ($context["removeInstanceAction"] ?? null);
            echo "\" class=\"bf-remove-layout\" onclick=\"if (!window.confirm(BF.lang.confirm_remove_layout)) return false;\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_remove", [], "any", false, false, false, 133);
            echo "</a>
                            </div>

                        </div>
                        ";
        }
        // line 138
        echo "                        ";
        // line 139
        echo "                        <ul class=\"tabs vertical clearfix\">
                            ";
        // line 140
        if ((($context["moduleId"] ?? null) != "basic")) {
            // line 141
            echo "                            <li class=\"tab cat-tab
                                ";
            // line 142
            if (( !twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "layout_id", [], "array", true, true, false, 142) || !twig_in_filter((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["settings"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["layout_id"] ?? null) : null), [0 => (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["category_layouts"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[0] ?? null) : null)]))) {
                echo " hidden ";
            }
            // line 143
            echo "                                ";
            if (((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["settings"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["current_adm_tab"] ?? null) : null) === "categories")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"categories\" data-target=\"#bf-categories\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_categories", [], "any", false, false, false, 143);
            echo "</li>
                            ";
        }
        // line 145
        echo "                            <li class=\"tab ";
        if (((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["settings"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["current_adm_tab"] ?? null) : null) === "embedding")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"embedding\" data-target=\"#bf-filter-embedding\">";
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_embedding", [], "any", false, false, false, 145);
        echo "</li>
                            ";
        // line 146
        if ((($context["moduleId"] ?? null) === "basic")) {
            // line 147
            echo "                            <li class=\"tab ";
            if (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["settings"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["current_adm_tab"] ?? null) : null) === "blocks")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"blocks\" data-target=\"#bf-filter-blocks-display\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_attributes_display", [], "any", false, false, false, 147);
            echo "</li>
                            <li class=\"tab ";
            // line 148
            if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["settings"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["current_adm_tab"] ?? null) : null) === "layout")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"layout\" data-target=\"#bf-filter-layout-view\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_filter_layout_view", [], "any", false, false, false, 148);
            echo "</li>
                            <li class=\"tab ";
            // line 149
            if (((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["settings"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["current_adm_tab"] ?? null) : null) === "submission")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"submission\" data-target=\"#bf-data-submission\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_data_submission", [], "any", false, false, false, 149);
            echo "</li>
                            <li class=\"tab ";
            // line 150
            if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["settings"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["current_adm_tab"] ?? null) : null) === "attributes")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"attributes\" data-target=\"#bf-attributes\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_attributes", [], "any", false, false, false, 150);
            echo "</li>
                            <li class=\"tab ";
            // line 151
            if (((($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["settings"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["current_adm_tab"] ?? null) : null) === "options")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"options\" data-target=\"#bf-options\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_options", [], "any", false, false, false, 151);
            echo "</li>
                            <li class=\"tab ";
            // line 152
            if (((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["settings"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["current_adm_tab"] ?? null) : null) === "filters")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"filters\" data-target=\"#bf-filters\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_filters", [], "any", false, false, false, 152);
            echo "</li>
                            <li class=\"tab ";
            // line 153
            if (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["settings"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["current_adm_tab"] ?? null) : null) === "responsive")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"responsive\" data-target=\"#bf-responsive\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_responsive_view", [], "any", false, false, false, 153);
            echo "</li>
                            <li class=\"tab ";
            // line 154
            if (((($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["settings"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["current_adm_tab"] ?? null) : null) === "style")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"style\" data-target=\"#bf-style\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_style", [], "any", false, false, false, 154);
            echo "</li>
                            <li class=\"tab ";
            // line 155
            if (((($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["settings"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["current_adm_tab"] ?? null) : null) === "global")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"global\" data-target=\"#bf-global-settings\">";
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_global_settings", [], "any", false, false, false, 155);
            echo "</li>
                            ";
        }
        // line 157
        echo "                            <li class=\"tab ";
        if (((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["settings"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["current_adm_tab"] ?? null) : null) === "attr_values")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"attr_values\" data-target=\"#bf-attr-values\">";
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_attr_values", [], "any", false, false, false, 157);
        echo "</li>
                            <li class=\"tab ";
        // line 158
        if (((($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["settings"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["current_adm_tab"] ?? null) : null) === "help")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"help\" data-target=\"#bf-help\">";
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "tab_help", [], "any", false, false, false, 158);
        echo "</li>
                            <li id=\"refresh-btn-wrapper\">
                                <button onclick=\"BF.refreshDB();return false;\" class=\"bf-button\" id=\"bf-refresh-db\">
                                    <span class=\"icon bf-update\"></span><span class=\"lbl\">";
        // line 161
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "update_cache", [], "any", false, false, false, 161);
        echo "</span>
                                </button>
                            </li>
                        </ul>
                        ";
        // line 166
        echo "                        <div id=\"bf-filter-embedding\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 167
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "embedding_header", [], "any", false, false, false, 167);
        echo "</div>
                            <div class=\"bf-alert\" style=\"display: block;\">";
        // line 168
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "embedding_warning", [], "any", false, false, false, 168);
        echo "</div>
                            <p class=\"bf-info\">";
        // line 169
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "embedding_description", [], "any", false, false, false, 169);
        echo "</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-container-selector\">";
        // line 173
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "embedding_container_selector", [], "any", false, false, false, 173);
        echo "</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][containerSelector]\" value=\"\" id=\"bf-container-selector\" placeholder=\"";
        // line 176
        echo (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["basicSettings"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["behaviour"] ?? null) : null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["containerSelector"] ?? null) : null);
        echo "\" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-paginator-selector\">";
        // line 181
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "embedding_paginator_selector", [], "any", false, false, false, 181);
        echo "</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][paginatorSelector]\" value=\"\" id=\"bf-paginator-selector\" placeholder=\"";
        // line 184
        echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["basicSettings"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["behaviour"] ?? null) : null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["paginatorSelector"] ?? null) : null);
        echo "\" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        ";
        // line 189
        if ((($context["moduleId"] ?? null) === "basic")) {
            // line 190
            echo "                        ";
            // line 191
            echo "                        <div id=\"bf-filter-blocks-display\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 192
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filter_blocks_header", [], "any", false, false, false, 192);
            echo "</div>
                            <p class=\"bf-info\">";
            // line 193
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filter_blocks_descr", [], "any", false, false, false, 193);
            echo "</p>
                            <table class=\"bf-adm-table\" id=\"bf-filter-sections\">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class=\"center\">";
            // line 198
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 198);
            echo "</th>
                                    <th class=\"bf-collapse-td\">";
            // line 199
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "collapse", [], "any", false, false, false, 199);
            echo "</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                ";
            // line 204
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["filterBlocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                // line 205
                echo "                                <tr class=\"bf-sort\" data-section=\"";
                echo (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["filter"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["name"] ?? null) : null);
                echo "\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">";
                // line 207
                echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["filter"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["label"] ?? null) : null);
                echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-";
                // line 211
                echo (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["filter"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["name"] ?? null) : null);
                echo "-filter\" type=\"hidden\" name=\"bf[behaviour][sections][";
                echo (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["filter"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["name"] ?? null) : null);
                echo "][enabled]\" value=\"0\" data-disable-adv=\"section-";
                echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["filter"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["name"] ?? null) : null);
                echo "\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td class=\"center bf-collapse-td\">
                                        <input id=\"bf-";
                // line 216
                echo (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["filter"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["name"] ?? null) : null);
                echo "-collapse\" type=\"checkbox\" name=\"bf[behaviour][sections][";
                echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["filter"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["name"] ?? null) : null);
                echo "][collapsed]\" value=\"1\" data-adv-group=\"section-";
                echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["filter"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["name"] ?? null) : null);
                echo "\" />
                                    </td>
                                    <td>
                                        ";
                // line 219
                if ((twig_get_attribute($this->env, $this->source, $context["filter"], "control", [], "array", true, true, false, 219) && twig_get_attribute($this->env, $this->source, ($context["possible_controls"] ?? null), (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["filter"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["name"] ?? null) : null), [], "array", true, true, false, 219))) {
                    // line 220
                    echo "                                        <select name=\"bf[behaviour][sections][";
                    echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["filter"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["name"] ?? null) : null);
                    echo "][control]\" data-adv-group=\"section-";
                    echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["filter"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["name"] ?? null) : null);
                    echo "\">
                                            ";
                    // line 221
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = ($context["possible_controls"] ?? null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55[(($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["filter"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["name"] ?? null) : null)] ?? null) : null));
                    foreach ($context['_seq'] as $context["val"] => $context["lbl"]) {
                        // line 222
                        echo "                                            <option value=\"";
                        echo $context["val"];
                        echo "\">";
                        echo $context["lbl"];
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['val'], $context['lbl'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 224
                    echo "                                        </select>
                                        ";
                }
                // line 226
                echo "                                    </td>
                                </tr>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 229
            echo "                                </tbody>
                            </table>
                        </div>
                        ";
            // line 233
            echo "                        <div id=\"bf-filter-layout-view\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 234
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_header", [], "any", false, false, false, 234);
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center bf-w165\">";
            // line 239
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 239);
            echo "</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                            ";
            // line 247
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_show_attr_groups", [], "any", false, false, false, 247);
            echo "
                                            </span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-attr-group\" type=\"hidden\" name=\"bf[behaviour][attribute_groups]\" value=\"0\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-adv-group-cont\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 262
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_product_count", [], "any", false, false, false, 262);
            echo "</span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-product-count\" type=\"hidden\" name=\"bf[behaviour][product_count]\" value=\"0\" data-disable-adv=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 274
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_hide_empty_attr", [], "any", false, false, false, 274);
            echo "</span>
                                        </td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-hide-empty\" type=\"hidden\" name=\"bf[behaviour][hide_empty]\" value=\"0\" data-adv-group=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-intersect-cont\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 288
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_sliding", [], "any", false, false, false, 288);
            echo "</span>
                                        </td>
                                        <td class=\"bf-intersect center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-sliding\" type=\"hidden\" name=\"bf[behaviour][limit_items][enabled]\" value=\"0\" data-disable-adv=\"sliding\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\" style=\"padding-top: 5px;padding-bottom: 5px;\"> 
                                            <input id=\"bf-number-to-show\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_show]\" value=\"\" data-adv-group=\"sliding\" />
                                            <label for=\"bf-number-to-show\">";
            // line 298
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_sliding_num_to_show", [], "any", false, false, false, 298);
            echo "</label>
                                            <div class=\"bf-suboption\">
                                                <input id=\"bf-number-to-hide\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_hide]\" value=\"\" data-adv-group=\"sliding\" /> 
                                                <label for=\"bf-number-to-hide\">";
            // line 301
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_sliding_min", [], "any", false, false, false, 301);
            echo "</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 307
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_height_limit", [], "any", false, false, false, 307);
            echo "</span>
                                        </td>
                                        <td class=\"bf-intersect center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-limit-height\" type=\"hidden\" name=\"bf[behaviour][limit_height][enabled]\" value=\"0\" data-disable-adv=\"limit-height\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"> 
                                            <input id=\"bf-limit-height\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_height][height]\" value=\"\" data-adv-group=\"limit-height\" /> 
                                            <label for=\"bf-limit-height\">";
            // line 317
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "layout_max_height_limit", [], "any", false, false, false, 317);
            echo "</label>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        ";
            // line 325
            echo "                        <div id=\"bf-data-submission\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 326
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_header", [], "any", false, false, false, 326);
            echo "</div>
                            <p class=\"bf-info\">";
            // line 327
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_descr", [], "any", false, false, false, 327);
            echo "</p>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">Enabled</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-auto\">";
            // line 337
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_type_auto", [], "any", false, false, false, 337);
            echo "</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-auto\" type=\"radio\" value=\"auto\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-delay\">";
            // line 346
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_delay", [], "any", false, false, false, 346);
            echo "</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-delay\" type=\"radio\" value=\"delay\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td>
                                        <input id=\"bf-submit-delay-time\" type=\"text\" name=\"bf[submission][submit_delay_time]\" value=\"\" size=\"4\" maxlength=\"4\" data-adv-group=\"submit-type\" data-for-val=\"delay\" />
                                        <label for=\"bf-submit-delay-time\">";
            // line 353
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_time_in_sec", [], "any", false, false, false, 353);
            echo "</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\"> 
                                            <label for=\"bf-submit-btn\">";
            // line 358
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_type_button", [], "any", false, false, false, 358);
            echo "</label></span>
                                    </td>
                                    <td class=\"center\"> 
                                        <input id=\"bf-submit-btn\" type=\"radio\" value=\"button\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td style=\"padding-top: 5px;padding-bottom: 5px;\">
                                        <input id=\"bf-submit-btn-fixed\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"fix\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                        <label for=\"bf-submit-btn-fixed\">";
            // line 365
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_button_fixed", [], "any", false, false, false, 365);
            echo "</label>
                                        <div class=\"bf-suboption\">
                                            <input id=\"bf-submit-btn-float\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"float\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                            <label for=\"bf-submit-btn-float\">";
            // line 368
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_button_float", [], "any", false, false, false, 368);
            echo "</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr class=\"bf-local-settings\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">
                                            <label for=\"bf-submit-default\">";
            // line 375
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_type_default", [], "any", false, false, false, 375);
            echo "</label>
                                        </span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-default\" type=\"radio\" value=\"default\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" class=\"bf-default\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
            // line 385
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "submission_hide_panel", [], "any", false, false, false, 385);
            echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-layout\" type=\"hidden\" name=\"bf[submission][hide_panel]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        </div>
                        ";
        }
        // line 399
        echo "                        ";
        // line 400
        echo "                        ";
        if ((($context["moduleId"] ?? null) != "basic")) {
            // line 401
            echo "                        <div id=\"bf-categories\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 402
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_header", [], "any", false, false, false, 402);
            echo "</div>
                            <p class=\"bf-info\">";
            // line 403
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_info", [], "any", false, false, false, 403);
            echo "</p>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-green-info\">";
            // line 405
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_list_of_enabled", [], "any", false, false, false, 405);
            echo "</p>
                                <div class=\"bf-gray-panel\">
                                    ";
            // line 407
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_filter_hint", [], "any", false, false, false, 407);
            echo "
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-enabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-enabled-categories\">";
            // line 411
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_select_all", [], "any", false, false, false, 411);
            echo "</a> /
                                    <a data-unselect-all=\"#bf-enabled-categories\">";
            // line 412
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_unselect_all", [], "any", false, false, false, 412);
            echo "</a>
                                </div>
                                <div id=\"bf-enabled-categories\" class=\"bf-multi-select\">
                                    ";
            // line 415
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 416
                echo "                                        ";
                if (( !twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "categories", [], "array", true, true, false, 416) ||  !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "categories", [], "array", false, true, false, 416), (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["category"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["category_id"] ?? null) : null), [], "array", true, true, false, 416))) {
                    // line 417
                    echo "                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][";
                    // line 418
                    echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["category"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["category_id"] ?? null) : null);
                    echo "]\" value=\"1\" />
                                            ";
                    // line 419
                    echo (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["category"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["name"] ?? null) : null);
                    echo "
                                        </div>
                                        ";
                }
                // line 422
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 423
            echo "                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                                ";
            // line 426
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_move_selected", [], "any", false, false, false, 426);
            echo "
                                <br><button class=\"bf-move-right\"></button><br><button class=\"bf-move-left\"></button><br>
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-red-info\">";
            // line 430
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_list_of_disabled", [], "any", false, false, false, 430);
            echo "</p>
                                <div class=\"bf-gray-panel\">
                                    ";
            // line 432
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_filter_hint", [], "any", false, false, false, 432);
            echo "
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-disabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-disabled-categories\">";
            // line 436
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_select_all", [], "any", false, false, false, 436);
            echo "</a> /
                                    <a data-unselect-all=\"#bf-disabled-categories\">";
            // line 437
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories_unselect_all", [], "any", false, false, false, 437);
            echo "</a>
                                </div>
                                <div id=\"bf-disabled-categories\" class=\"bf-multi-select\">
                                    ";
            // line 440
            if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "categories", [], "array", true, true, false, 440)) {
                // line 441
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = ($context["settings"] ?? null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["categories"] ?? null) : null));
                foreach ($context['_seq'] as $context["catId"] => $context["b"]) {
                    // line 442
                    echo "                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][";
                    // line 443
                    echo (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = ($context["categories"] ?? null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54[$context["catId"]] ?? null) : null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["category_id"] ?? null) : null);
                    echo "]\" value=\"1\" />
                                            ";
                    // line 444
                    echo (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = ($context["categories"] ?? null)) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327[$context["catId"]] ?? null) : null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["name"] ?? null) : null);
                    echo "
                                        </div>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['catId'], $context['b'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 447
                echo "                                    ";
            }
            // line 448
            echo "                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        ";
        }
        // line 453
        echo "                        ";
        if ((($context["moduleId"] ?? null) === "basic")) {
            // line 454
            echo "                        ";
            // line 455
            echo "                        <div id=\"bf-attributes\" class=\"tab-content with-border\" data-group=\"settings\" data-select-all-group=\"attributes\">
                            <div class=\"bf-th-header-static\">";
            // line 456
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "attributes_header", [], "any", false, false, false, 456);
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center\">";
            // line 461
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 461);
            echo "</th>
                                        <th class=\"bf-w165\">";
            // line 462
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 462);
            echo "</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 466
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "attributes_group_setting", [], "any", false, false, false, 466);
            echo "</span></td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[attributes_default][enable_all]\" value=\"0\" data-disable-adv=\"group-attr-control\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td>
                                            <select name=\"bf[attributes_default][control]\" data-adv-group=\"group-attr-control\">
                                                <option value=\"checkbox\">";
            // line 475
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 475);
            echo "</option>
                                                <option value=\"radio\">";
            // line 476
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 476);
            echo "</option>
                                                <option value=\"select\">";
            // line 477
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 477);
            echo "</option>
                                                <option value=\"slider\">";
            // line 478
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 478);
            echo "</option>
                                                <option value=\"slider_lbl\">";
            // line 479
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 479);
            echo "</option>
                                                <option value=\"slider_lbl_inp\">";
            // line 480
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 480);
            echo "</option>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 487
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "attributes_individual_set", [], "any", false, false, false, 487);
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 491
            echo sprintf(twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "autocomplete_hint", [], "any", false, false, false, 491), twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_attribute", [], "any", false, false, false, 491));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-autocomplete\" data-lookup=\"attributes\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-attr-setting-template\" data-target-tbl=\"#custom-attr-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 494
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_attribute", [], "any", false, false, false, 494);
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 498
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "attributes_custom_set_descr", [], "any", false, false, false, 498);
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"attributes\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                ";
            // line 507
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 507);
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 509
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "set_all_default", [], "any", false, false, false, 509);
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"0\">";
            // line 511
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "disable_all", [], "any", false, false, false, 511);
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"1\">";
            // line 513
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enable_all", [], "any", false, false, false, 513);
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 516
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 516);
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-attr-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 527
            echo "                        <div id=\"bf-options\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 528
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_header", [], "any", false, false, false, 528);
            echo "</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                <tr>
                                    <th></th>
                                    <th class=\"center bf-w165\">";
            // line 532
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 532);
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 533
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 533);
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 534
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode", [], "any", false, false, false, 534);
            echo "</th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 537
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_group_setting", [], "any", false, false, false, 537);
            echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[options_default][enable_all]\" value=\"0\" data-disable-adv=\"group-opt-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[options_default][control]\" data-adv-group=\"group-opt-control\" class=\"bf-w165\">
                                            <option value=\"checkbox\">";
            // line 546
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 546);
            echo "</option>
                                            <option value=\"radio\">";
            // line 547
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 547);
            echo "</option>
                                            <option value=\"select\">";
            // line 548
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 548);
            echo "</option>
                                            <option value=\"slider\">";
            // line 549
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 549);
            echo "</option>
                                            <option value=\"slider_lbl\">";
            // line 550
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 550);
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 551
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 551);
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 552
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "grid_of_images", [], "any", false, false, false, 552);
            echo "</option>
                                        </select>
                                    </td>
                                    <td class=\"center\">
                                        <select name=\"bf[options_default][mode]\" class=\"bf-opt-mode bf-w135 d-opt-control\" data-adv-group=\"group-opt-control\" data-bf-role=\"mode\">
                                            <option value=\"label\">";
            // line 557
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_label", [], "any", false, false, false, 557);
            echo "</option>
                                            <option value=\"img_label\">";
            // line 558
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_image_and_label", [], "any", false, false, false, 558);
            echo "</option>
                                            <option value=\"img\">";
            // line 559
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_image", [], "any", false, false, false, 559);
            echo "</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 564
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_individual_set", [], "any", false, false, false, 564);
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 568
            echo sprintf(twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "autocomplete_hint", [], "any", false, false, false, 568), twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_option", [], "any", false, false, false, 568));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"options\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-opt-setting-template\" data-target-tbl=\"#custom-opt-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 571
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_option", [], "any", false, false, false, 571);
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 575
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_custom_set_descr", [], "any", false, false, false, 575);
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"options\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">";
            // line 583
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 583);
            echo "</th>
                                                ";
            // line 584
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 584);
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"options\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 586
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "set_all_default", [], "any", false, false, false, 586);
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"0\">";
            // line 588
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "disable_all", [], "any", false, false, false, 588);
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"1\">";
            // line 590
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enable_all", [], "any", false, false, false, 590);
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 593
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 593);
            echo "</th>
                                            <th class=\"bf-w165\">";
            // line 594
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode", [], "any", false, false, false, 594);
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-opt-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 605
            echo "                        <div id=\"bf-filters\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 606
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filters_header", [], "any", false, false, false, 606);
            echo "</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"filters\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">";
            // line 610
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 610);
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 611
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 611);
            echo "</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 615
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filters_group_setting", [], "any", false, false, false, 615);
            echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[filters_default][enable_all]\" value=\"0\" data-disable-adv=\"group-filter-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[filters_default][control]\" data-adv-group=\"group-filter-control\">
                                            <option value=\"checkbox\">";
            // line 624
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 624);
            echo "</option>
                                            <option value=\"radio\">";
            // line 625
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 625);
            echo "</option>
                                            <option value=\"select\">";
            // line 626
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 626);
            echo "</option>
                                            <option value=\"slider\">";
            // line 627
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 627);
            echo "</option>
                                            <option value=\"slider_lbl\">";
            // line 628
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 628);
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 629
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 629);
            echo "</option>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 635
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filters_individual_set", [], "any", false, false, false, 635);
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 639
            echo sprintf(twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "autocomplete_hint", [], "any", false, false, false, 639), twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_filter", [], "any", false, false, false, 639));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"filters\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-filter-setting-template\" data-target-tbl=\"#custom-filter-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 642
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "btn_select_filter", [], "any", false, false, false, 642);
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 646
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "filters_custom_set_descr", [], "any", false, false, false, 646);
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"filters\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                ";
            // line 655
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enabled", [], "any", false, false, false, 655);
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"filters\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 657
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "set_all_default", [], "any", false, false, false, 657);
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"0\">";
            // line 659
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "disable_all", [], "any", false, false, false, 659);
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"1\">";
            // line 661
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enable_all", [], "any", false, false, false, 661);
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 664
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "control", [], "any", false, false, false, 664);
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-filter-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 675
            echo "                        <div id=\"bf-responsive\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 676
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_header", [], "any", false, false, false, 676);
            echo "</div>
                            <table class=\"bf-adm-table bf-adv-group-cont\">
                                <tr>
                                    <th></th>
                                    <th class=\"bf-w170\"></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 682
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 682);
            echo "</span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 686
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_mode_enable", [], "any", false, false, false, 686);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive\" type=\"hidden\" name=\"bf[style][responsive][enabled]\" value=\"0\" data-disable-adv=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 698
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_collapse_sections", [], "any", false, false, false, 698);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive-collapse\" type=\"hidden\" name=\"bf[style][responsive][collapsed]\" value=\"0\" data-adv-group=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 710
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_max_width", [], "any", false, false, false, 710);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 719
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_max_screen_width", [], "any", false, false, false, 719);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_screen_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 728
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_position", [], "any", false, false, false, 728);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-position-left\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"left\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-left\">";
            // line 731
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "left", [], "any", false, false, false, 731);
            echo "</label>
                                        <input id=\"bf-responsive-position-right\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"right\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-right\">";
            // line 733
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "right", [], "any", false, false, false, 733);
            echo "</label>
                                    </td>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 741
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "responsive_offset", [], "any", false, false, false, 741);
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-offset\" type=\"text\" name=\"bf[style][responsive][offset]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        ";
            // line 752
            echo "                         <div id=\"bf-style\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 753
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_block_header", [], "any", false, false, false, 753);
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><span class=\"bf-local-settings\">";
            // line 759
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 759);
            echo "</span></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 763
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_background", [], "any", false, false, false, 763);
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_background][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_background\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 771
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_text", [], "any", false, false, false, 771);
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_text][val]\" /> 
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_text\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    ";
            // line 778
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 779
                echo "                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                                ";
                // line 782
                echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_title", [], "any", false, false, false, 782);
                echo " (
                                                <img src=\"";
                // line 783
                echo (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = $context["language"]) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["image_path"] ?? null) : null);
                echo "\" /> 
                                                ";
                // line 784
                echo (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["language"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["name"] ?? null) : null);
                echo ")
                                            </span>
                                        </td>
                                        <td colspan=\"2\">
                                            <input type=\"text\" name=\"bf[behaviour][filter_name][";
                // line 788
                echo (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = $context["language"]) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["language_id"] ?? null) : null);
                echo "]\" value=\"\" class=\"bf-w195\" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 793
            echo "                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 795
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_product_quantity", [], "any", false, false, false, 795);
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 801
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 801);
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 805
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_background", [], "any", false, false, false, 805);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_background][val]\"/> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 813
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_text", [], "any", false, false, false, 813);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_text][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                                </div>
                                <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 822
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_price_slider", [], "any", false, false, false, 822);
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 828
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 828);
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 832
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_background", [], "any", false, false, false, 832);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 840
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_active_area_background", [], "any", false, false, false, 840);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-area-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_area_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_area_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_area_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 848
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_border", [], "any", false, false, false, 848);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_border\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 856
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_slider_handle_background", [], "any", false, false, false, 856);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 864
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_slider_handle_border", [], "any", false, false, false, 864);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_border\", this)}'/></td>
                                    <td></td>
                                </tr>

                            </table>
                            </div>
                             <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 874
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_group_block_header", [], "any", false, false, false, 874);
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 880
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 880);
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 884
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_background", [], "any", false, false, false, 884);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 892
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_text", [], "any", false, false, false, 892);
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_text][val]\"  /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 901
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_responsive_popup_view", [], "any", false, false, false, 901);
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 905
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_show_btn_color", [], "any", false, false, false, 905);
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_show_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_show_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_show_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 913
            echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "theme_reset_btn_color", [], "any", false, false, false, 913);
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_reset_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_reset_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_reset_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        ";
        }
        // line 924
        echo "                        ";
        // line 925
        echo "                        <div id=\"bf-attr-values\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 926
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "ordering_header", [], "any", false, false, false, 926);
        echo "</div>
                            <div class=\"bf-alert\" style=\"display: block;\">";
        // line 927
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "ordering_note", [], "any", false, false, false, 927);
        echo "</div>
                            <p class=\"bf-info\">";
        // line 928
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "ordering_descr", [], "any", false, false, false, 928);
        echo "</p>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    ";
        // line 931
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "ordering_filter_hint", [], "any", false, false, false, 931);
        echo "
                                    <input type=\"text\" class=\"bf-attr-filter bf-full-width\" data-target=\"#bf-attr-list\" />
                                </div>
                                <div id=\"bf-attr-list\" class=\"bf-multi-select\">
                                    ";
        // line 935
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attributes"] ?? null));
        foreach ($context['_seq'] as $context["attrId"] => $context["attribute"]) {
            // line 936
            echo "                                    <div class=\"bf-row\" data-attr-id=\"";
            echo $context["attrId"];
            echo "\">
                                        <b>";
            // line 937
            echo (($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["attribute"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["group"] ?? null) : null);
            echo "</b> / ";
            echo (($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = $context["attribute"]) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["name"] ?? null) : null);
            echo "
                                    </div>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrId'], $context['attribute'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 940
        echo "                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    <div class=\"buttons\">
                                        <div>";
        // line 947
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "ordering_language", [], "any", false, false, false, 947);
        echo ":</div>
                                        <select id=\"bf-attr-val-language\" class=\"bf-w165\">
                                            ";
        // line 949
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["language"]) {
            // line 950
            echo "                                            <option value=\"";
            echo (($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = $context["language"]) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["language_id"] ?? null) : null);
            echo "\">";
            echo (($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = $context["language"]) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["name"] ?? null) : null);
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 952
        echo "                                        </select>
                                        <div class=\"bf-pull-right\">
                                            <button class=\"bf-auto-sort\" data-type=\"number\">0..9</button>
                                            <button class=\"bf-auto-sort\" data-type=\"string\">A..Z</button>
                                            <a class=\"bf-button bf-save-btn\" title=\"";
        // line 956
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "button_save_n_close", [], "any", false, false, false, 956);
        echo "\"><span class=\"icon\"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div id=\"bf-attr-val-list\" class=\"bf-multi-select\">
                                    
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        ";
        // line 967
        echo "                        <div id=\"bf-global-settings\" class=\"tab-content with-border bf-global-settings\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 968
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_header", [], "any", false, false, false, 968);
        echo "</div>
                            <p class=\"bf-info\">";
        // line 969
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_settings_descr", [], "any", false, false, false, 969);
        echo "</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
        // line 972
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_hide_empty_stock", [], "any", false, false, false, 972);
        echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-out-of-stock\" type=\"hidden\" name=\"bf[global][hide_out_of_stock]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class=\"bf-global-settings\">
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 983
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_postponed_count", [], "any", false, false, false, 983);
        echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-postponed-count\" type=\"hidden\" name=\"bf[global][postponed_count]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
        // line 994
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_enable_multiple_attributes", [], "any", false, false, false, 994);
        echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-multiple-attributes\" type=\"hidden\" name=\"bf[global][multiple_attributes]\" value=\"0\" data-disable-adv=\"attr-separator\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <label for=\"bf-attr-separator\">";
        // line 1002
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_separator", [], "any", false, false, false, 1002);
        echo "</label>
                                        <input id=\"bf-attr-separator\" type=\"text\" name=\"bf[global][attribute_separator]\" value=\"\" size=\"4\" data-adv-group=\"attr-separator\" />
                                        ";
        // line 1004
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_multiple_attr_separator", [], "any", false, false, false, 1004);
        echo "
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1009
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_in_stock_status_id", [], "any", false, false, false, 1009);
        echo "</span>
                                    </td>
                                    <td class=\"\" colspan=\"2\">
                                        <select name=\"bf[global][instock_status_id]\" class=\"bf-w165\">
                                            ";
        // line 1013
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["stockStatuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 1014
            echo "                                                    <option value=\"";
            echo (($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = $context["status"]) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["stock_status_id"] ?? null) : null);
            echo "\">";
            echo (($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a = $context["status"]) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["name"] ?? null) : null);
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1016
        echo "                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1021
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "global_subcats_fix", [], "any", false, false, false, 1021);
        echo "</span>
                                    </td>
                                    <td class=\" center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-subcategories\" type=\"hidden\" name=\"bf[global][subcategories_fix]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1033
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "entry_cron_secret_key", [], "any", false, false, false, 1033);
        echo "
                                            <span class=\"bf-link-highlight\" style=\"padding-top:5px;\">";
        // line 1034
        echo ($context["catalogUrl"] ?? null);
        echo "index.php?route=extension/module/brainyfilter/cron&key=<b>cron secret key</b></span>
                                        </span>
                                    </td>
                                    <td>
                                        <input id=\"bf-cron-key\" class=\"bf-w165\" type=\"text\" name=\"bf[global][cron_secret_key]\" value=\"\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan=\"3\">
                                    </td>
                                </tr>
                            </table>                    
                        </div>
                        ";
        // line 1049
        echo "                        <div id=\"bf-help\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header\" id=\"bf-faq-n-troubleshooting\"><span class=\"icon bf-arrow\"></span>";
        // line 1050
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "help_faq_n_trouleshooting", [], "any", false, false, false, 1050);
        echo "</div>
                            <div style=\"display:none;\">
                                
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
        // line 1054
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "help_about", [], "any", false, false, false, 1054);
        echo "</div>
                            <div id=\"bf-about\">
                                <div class=\"bf-about-text\">
                                    ";
        // line 1057
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "help_about_content", [], "any", false, false, false, 1057);
        echo "
                                    <hr />
                                    <p>";
        // line 1059
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "bf_signature", [], "any", false, false, false, 1059);
        echo "</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class=\"bf-signature\">";
        // line 1069
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "bf_signature", [], "any", false, false, false, 1069);
        echo "</div>
</div>

<table style=\"display:none;\">
    <tbody id=\"custom-attr-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][attribute][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[attributes][{i}][enabled]\" value=\"0\" data-disable-adv=\"attr-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[attributes][{i}][control]\" data-adv-group=\"attr-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">";
        // line 1084
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 1084);
        echo "</option>
                    <option value=\"radio\">";
        // line 1085
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 1085);
        echo "</option>
                    <option value=\"select\">";
        // line 1086
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 1086);
        echo "</option>
                    <option value=\"slider\">";
        // line 1087
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 1087);
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1088
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 1088);
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1089
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 1089);
        echo "</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-opt-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][options][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[options][{i}][enabled]\" value=\"0\" data-disable-adv=\"opt-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][control]\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"control\" class=\"bf-w135\">
                    <option value=\"checkbox\">";
        // line 1108
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 1108);
        echo "</option>
                    <option value=\"radio\">";
        // line 1109
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 1109);
        echo "</option>
                    <option value=\"select\">";
        // line 1110
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 1110);
        echo "</option>
                    <option value=\"slider\">";
        // line 1111
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 1111);
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1112
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 1112);
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1113
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 1113);
        echo "</option>
                    <option value=\"grid\">";
        // line 1114
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "grid_of_images", [], "any", false, false, false, 1114);
        echo "</option>
                </select>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][mode]\" class=\"bf-opt-mode bf-w135\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"mode\">
                    <option value=\"label\">";
        // line 1119
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_label", [], "any", false, false, false, 1119);
        echo "</option>
                    <option value=\"img_label\">";
        // line 1120
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_image_and_label", [], "any", false, false, false, 1120);
        echo "</option>
                    <option value=\"img\">";
        // line 1121
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "options_view_mode_image", [], "any", false, false, false, 1121);
        echo "</option>
                </select>
            </td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-filter-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][filter][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[filters][{i}][enabled]\" value=\"0\" data-disable-adv=\"filter-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[filters][{i}][control]\" data-adv-group=\"filter-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">";
        // line 1140
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "checkbox", [], "any", false, false, false, 1140);
        echo "</option>
                    <option value=\"radio\">";
        // line 1141
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "radio", [], "any", false, false, false, 1141);
        echo "</option>
                    <option value=\"select\">";
        // line 1142
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "selectbox", [], "any", false, false, false, 1142);
        echo "</option>
                    <option value=\"slider\">";
        // line 1143
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider", [], "any", false, false, false, 1143);
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1144
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_only", [], "any", false, false, false, 1144);
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1145
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "slider_labels_and_inputs", [], "any", false, false, false, 1145);
        echo "</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>

";
        // line 1153
        echo "
<div id=\"bf-category-list-tpl\" class=\"bf-category-list\" style=\"display: none;\">
    <div class=\"bf-label\">
        ";
        // line 1156
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "categories", [], "any", false, false, false, 1156);
        echo "
        (<a onclick=\"jQuery(this).closest('.bf-category-list').find('input').removeAttr('checked')\">";
        // line 1157
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "disable_all", [], "any", false, false, false, 1157);
        echo "</a>
        <span>/</span>
        <a onclick=\"jQuery(this).closest('.bf-category-list').find('input').attr('checked', 'checked')\">";
        // line 1159
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enable_all", [], "any", false, false, false, 1159);
        echo "</a>)
    </div>
    <div class=\"bf-cat-list-cont\">
        <ul data-select-all-group=\"categories\">
            ";
        // line 1163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 1164
            echo "            <li>
                <label>
                    <input type=\"checkbox\" name=\"bf[categories][";
            // line 1166
            echo (($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = $context["cat"]) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["category_id"] ?? null) : null);
            echo "]\" value=\"1\" />
                    ";
            // line 1167
            echo (($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = $context["cat"]) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["name"] ?? null) : null);
            echo "
                </label>
            </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1171
        echo "        </ul>
    </div>
</div>

";
        // line 1176
        echo "<script>
BF.lang = {
        'default' : '";
        // line 1178
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 1178);
        echo "',
        'error_layout_not_set' : '";
        // line 1179
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "message_error_layout_not_set", [], "any", false, false, false, 1179);
        echo "',
        'error_cant_remove_default' : '";
        // line 1180
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_remove_default_layout", [], "any", false, false, false, 1180);
        echo "',
        'default_layout' : '";
        // line 1181
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_default_layout", [], "any", false, false, false, 1181);
        echo "',
        'confirm_remove_layout' : '";
        // line 1182
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_remove_confirmation", [], "any", false, false, false, 1182);
        echo "',
        'confirm_unsaved_changes' : '";
        // line 1183
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "message_unsaved_changes", [], "any", false, false, false, 1183);
        echo "',
        'updating' : '";
        // line 1184
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "updating", [], "any", false, false, false, 1184);
        echo "',
        'empty_table' : '";
        // line 1185
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "message_empty_table", [], "any", false, false, false, 1185);
        echo "',
        'content_top' : '";
        // line 1186
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_content_top", [], "any", false, false, false, 1186);
        echo "',
        'column_left' : '";
        // line 1187
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_column_left", [], "any", false, false, false, 1187);
        echo "',
        'column_right' : '";
        // line 1188
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_column_right", [], "any", false, false, false, 1188);
        echo "',
        'content_bottom' : '";
        // line 1189
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "instance_content_bottom", [], "any", false, false, false, 1189);
        echo "'
    };
BF.moduleId = '";
        // line 1191
        echo ((($context["isNewInstance"] ?? null)) ? ("new") : (($context["moduleId"] ?? null)));
        echo "';
BF.settings = ";
        // line 1192
        echo json_encode(($context["settings"] ?? null));
        echo ";
BF.attributes = ";
        // line 1193
        echo json_encode(($context["attributes"] ?? null));
        echo ";
BF.options = ";
        // line 1194
        echo json_encode(($context["options"] ?? null));
        echo ";
BF.filters = ";
        // line 1195
        echo json_encode(($context["filters"] ?? null));
        echo ";
BF.refreshActionUrl = '";
        // line 1196
        echo twig_replace_filter(($context["refreshAction"] ?? null), ["&amp;" => "&"]);
        echo "';
BF.modRefreshActionUrl = '";
        // line 1197
        echo twig_replace_filter(($context["modRefreshAction"] ?? null), ["&amp;" => "&"]);
        echo "';
BF.attrValActionUrl = '";
        // line 1198
        echo twig_replace_filter(($context["attributeValuesAction"] ?? null), ["&amp;" => "&"]);
        echo "';
BF.isFirstLaunch = ";
        // line 1199
        echo ($context["isFirstLaunch"] ?? null);
        echo ";
BF.categoryLayouts = ";
        // line 1200
        echo json_encode(($context["category_layouts"] ?? null));
        echo ";
jQuery(document).ready(BF.init());
</script>

<style>
    .bf-def:before {
        content: '";
        // line 1206
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "default", [], "any", false, false, false, 1206);
        echo "';
    }
    .bf-no:before {
        content: '";
        // line 1209
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "no", [], "any", false, false, false, 1209);
        echo "';
    }
    .bf-yes:before {
        content: '";
        // line 1212
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "yes", [], "any", false, false, false, 1212);
        echo "';
    }
    .bf-disable-enable .bf-no:before {
        content: '";
        // line 1215
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "disable_all", [], "any", false, false, false, 1215);
        echo "';
    }
    .bf-disable-enable .bf-yes:before {
        content: '";
        // line 1218
        echo twig_get_attribute($this->env, $this->source, ($context["lang"] ?? null), "enable_all", [], "any", false, false, false, 1218);
        echo "';
    }
</style>
";
        // line 1221
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/brainyfilter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2382 => 1221,  2376 => 1218,  2370 => 1215,  2364 => 1212,  2358 => 1209,  2352 => 1206,  2343 => 1200,  2339 => 1199,  2335 => 1198,  2331 => 1197,  2327 => 1196,  2323 => 1195,  2319 => 1194,  2315 => 1193,  2311 => 1192,  2307 => 1191,  2302 => 1189,  2298 => 1188,  2294 => 1187,  2290 => 1186,  2286 => 1185,  2282 => 1184,  2278 => 1183,  2274 => 1182,  2270 => 1181,  2266 => 1180,  2262 => 1179,  2258 => 1178,  2254 => 1176,  2248 => 1171,  2238 => 1167,  2234 => 1166,  2230 => 1164,  2226 => 1163,  2219 => 1159,  2214 => 1157,  2210 => 1156,  2205 => 1153,  2195 => 1145,  2191 => 1144,  2187 => 1143,  2183 => 1142,  2179 => 1141,  2175 => 1140,  2153 => 1121,  2149 => 1120,  2145 => 1119,  2137 => 1114,  2133 => 1113,  2129 => 1112,  2125 => 1111,  2121 => 1110,  2117 => 1109,  2113 => 1108,  2091 => 1089,  2087 => 1088,  2083 => 1087,  2079 => 1086,  2075 => 1085,  2071 => 1084,  2053 => 1069,  2040 => 1059,  2035 => 1057,  2029 => 1054,  2022 => 1050,  2019 => 1049,  2002 => 1034,  1998 => 1033,  1983 => 1021,  1976 => 1016,  1965 => 1014,  1961 => 1013,  1954 => 1009,  1946 => 1004,  1941 => 1002,  1930 => 994,  1916 => 983,  1902 => 972,  1896 => 969,  1892 => 968,  1889 => 967,  1876 => 956,  1870 => 952,  1859 => 950,  1855 => 949,  1850 => 947,  1841 => 940,  1830 => 937,  1825 => 936,  1821 => 935,  1814 => 931,  1808 => 928,  1804 => 927,  1800 => 926,  1797 => 925,  1795 => 924,  1781 => 913,  1770 => 905,  1763 => 901,  1751 => 892,  1740 => 884,  1733 => 880,  1724 => 874,  1711 => 864,  1700 => 856,  1689 => 848,  1678 => 840,  1667 => 832,  1660 => 828,  1651 => 822,  1639 => 813,  1628 => 805,  1621 => 801,  1612 => 795,  1608 => 793,  1597 => 788,  1590 => 784,  1586 => 783,  1582 => 782,  1577 => 779,  1573 => 778,  1563 => 771,  1552 => 763,  1545 => 759,  1536 => 753,  1533 => 752,  1520 => 741,  1509 => 733,  1504 => 731,  1498 => 728,  1486 => 719,  1474 => 710,  1459 => 698,  1444 => 686,  1437 => 682,  1428 => 676,  1425 => 675,  1412 => 664,  1406 => 661,  1401 => 659,  1396 => 657,  1391 => 655,  1379 => 646,  1372 => 642,  1366 => 639,  1359 => 635,  1350 => 629,  1346 => 628,  1342 => 627,  1338 => 626,  1334 => 625,  1330 => 624,  1318 => 615,  1311 => 611,  1307 => 610,  1300 => 606,  1297 => 605,  1284 => 594,  1280 => 593,  1274 => 590,  1269 => 588,  1264 => 586,  1259 => 584,  1255 => 583,  1244 => 575,  1237 => 571,  1231 => 568,  1224 => 564,  1216 => 559,  1212 => 558,  1208 => 557,  1200 => 552,  1196 => 551,  1192 => 550,  1188 => 549,  1184 => 548,  1180 => 547,  1176 => 546,  1164 => 537,  1158 => 534,  1154 => 533,  1150 => 532,  1143 => 528,  1140 => 527,  1127 => 516,  1121 => 513,  1116 => 511,  1111 => 509,  1106 => 507,  1094 => 498,  1087 => 494,  1081 => 491,  1074 => 487,  1064 => 480,  1060 => 479,  1056 => 478,  1052 => 477,  1048 => 476,  1044 => 475,  1032 => 466,  1025 => 462,  1021 => 461,  1013 => 456,  1010 => 455,  1008 => 454,  1005 => 453,  998 => 448,  995 => 447,  986 => 444,  982 => 443,  979 => 442,  974 => 441,  972 => 440,  966 => 437,  962 => 436,  955 => 432,  950 => 430,  943 => 426,  938 => 423,  932 => 422,  926 => 419,  922 => 418,  919 => 417,  916 => 416,  912 => 415,  906 => 412,  902 => 411,  895 => 407,  890 => 405,  885 => 403,  881 => 402,  878 => 401,  875 => 400,  873 => 399,  856 => 385,  843 => 375,  833 => 368,  827 => 365,  817 => 358,  809 => 353,  799 => 346,  787 => 337,  774 => 327,  770 => 326,  767 => 325,  757 => 317,  744 => 307,  735 => 301,  729 => 298,  716 => 288,  699 => 274,  684 => 262,  666 => 247,  655 => 239,  647 => 234,  644 => 233,  639 => 229,  631 => 226,  627 => 224,  616 => 222,  612 => 221,  605 => 220,  603 => 219,  593 => 216,  581 => 211,  574 => 207,  568 => 205,  564 => 204,  556 => 199,  552 => 198,  544 => 193,  540 => 192,  537 => 191,  535 => 190,  533 => 189,  525 => 184,  519 => 181,  511 => 176,  505 => 173,  498 => 169,  494 => 168,  490 => 167,  487 => 166,  480 => 161,  470 => 158,  461 => 157,  452 => 155,  444 => 154,  436 => 153,  428 => 152,  420 => 151,  412 => 150,  404 => 149,  396 => 148,  387 => 147,  385 => 146,  376 => 145,  366 => 143,  362 => 142,  359 => 141,  357 => 140,  354 => 139,  352 => 138,  342 => 133,  329 => 123,  322 => 119,  315 => 115,  311 => 114,  307 => 113,  303 => 112,  298 => 110,  293 => 107,  282 => 105,  278 => 104,  274 => 103,  269 => 101,  265 => 99,  263 => 98,  260 => 97,  254 => 93,  252 => 92,  247 => 90,  243 => 88,  237 => 86,  235 => 85,  230 => 84,  228 => 83,  225 => 82,  220 => 78,  209 => 69,  201 => 68,  198 => 67,  188 => 63,  180 => 62,  177 => 61,  173 => 60,  167 => 57,  158 => 53,  151 => 49,  146 => 47,  139 => 45,  135 => 43,  131 => 41,  127 => 40,  118 => 35,  108 => 29,  104 => 28,  100 => 27,  96 => 25,  85 => 23,  81 => 22,  76 => 20,  71 => 17,  68 => 16,  59 => 14,  54 => 13,  51 => 12,  45 => 10,  43 => 9,  37 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("{#
 * Brainy Filter Pro 5.1.3 OC3, September 18, 2017 / brainyfilter.com 
 * Copyright 2015-2017 Giant Leap Lab / www.giantleaplab.com 
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store. 
 * Support: http://support.giantleaplab.com 
#}
{{ header }}{{ column_left }}
<div id=\"content\">
    {% if success %}
        <div class=\"alert alert-success\">{{ success }}</div>
    {% endif %}
    {% if error_warning|length %}
        {% for err in error_warning %}
            <div class=\"warning\">{{ err }}</div>
        {% endfor %}
    {% endif %}
    <div class=\"box\">
        <div class=\"heading page-header\">
            <div class=\"container-fluid\">
                <h1>{{ heading_title }}</h1>
                <ul class=\"breadcrumb\">
                    {% for breadcrumb in breadcrumbs %}
                    <li><a href=\"{{ breadcrumb['href'] }}\">{{ breadcrumb['text'] }}</a></li>
                    {% endfor %}
                </ul>
                <div class=\"pull-right\">
                    <a onclick=\"jQuery('[name=action]').val('apply');BF.submitForm();\" class=\"btn btn-success\" data-toggle=\"tooltip\" title=\"{{ lang.button_save }}\"><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"BF.submitForm();\" class=\"btn btn-primary\" data-toggle=\"tooltip\" title=\"{{ lang.button_save_n_close }}\"><span class=\"icon\"></span><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"location = '{{ cancel }}';\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"{{ lang.button_cancel }}\"><i class=\"fa fa-reply\"></i></a>
                </div>
            </div>
        </div>
    </div>
    {# settings block #}
    <form action=\"{{ action }}\" method=\"post\" enctype=\"application/x-www-form-urlencoded\" id=\"form\">
        <input type=\"hidden\" name=\"action\" value=\"save\" />
        <input type=\"hidden\" name=\"bf\" value=\"\" />
    </form>
    <form action=\"\" id=\"bf-form\" class=\"container-fluid\">
        <input type=\"hidden\" name=\"bf[module_id]\" value=\"{{ isNewInstance ? 'new' : moduleId }}\" />
        <input type=\"hidden\" name=\"bf[current_adm_tab]\" value=\"{{ settings['current_adm_tab'] }}\" />
        {# main menu #}
        <div id=\"bf-adm-main-menu\">
            <ul class=\"clearfix\">
                <li class=\"{% if moduleId is same as ('basic') %} selected {% endif %}\">
                    <div>
                        <a href=\"{{ instanceUrl ~ 'basic' }}\">
                            <span class=\"icon basic\"></span>
                            {{ lang.top_menu_basic_settings }}
                        </a>
                    </div>
                </li>
                <li class=\"{% if moduleId and moduleId != 'basic' %} selected {% endif %}\">
                    <div class=\"dropdown\">
                        <a id=\"dLabel\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            <span class=\"icon layouts\"></span>
                            {{ lang.top_menu_module_instances }}
                        </a>
                        <ul class=\"dropdown-menu\" aria-labelledby=\"dLabel\">
                            {% for module in modules %}
                            <li>
                                <a href=\"{{ instanceUrl ~ module['module_id'] }}\" {% if moduleId == module['module_id'] %} class=\"bf-selected\" {% endif %}>
                                    {{ module['name'] }}
                                </a>
                            </li>
                            {% endfor %}
                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"{{ instanceUrl ~ 'new' }}\" {% if moduleId == 'new' %} class=\"bf-selected\" {% endif %}>
                                    <i class=\"fa fa-plus\"></i> {{ lang.top_menu_add_new_instance }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <div class=\"clearfix\"></div>
        </div>
        {# main menu block end #}
        
        <div id=\"bf-adm-main-container\">
            
            {# basic settings container #}
            <div id=\"bf-adm-basic-settings\" class=\"tab-content\" data-group=\"main\" style=\"display:block\">
                {% if moduleId is same as ('basic') %}
                <p class=\"bf-info\">{{ lang.instance_basic_settings_info }}</p>
                {% elseif moduleId is same as ('new') %}
                <p class=\"bf-info\">{{ lang.instance_new_layout_notice }}</p>
                {% endif %}
                <div class=\"bf-panel\">
                    <div id=\"bf-create-instance-alert\" class=\"bf-alert\">
                        {{ lang.message_new_instance }}
                    </div>
                    {% if moduleId != 'basic' %}
                    <div class=\"bf-panel-row clearfix\">
                        <div class=\"bf-notice\"></div>
                    </div>
                    {% endif %}
                    <div class=\"tab-content-inner\">
                        {% if moduleId != 'basic' %}
                        <div class=\"bf-panel-row bf-local-settings clearfix\">
                            <div class=\"left\">
                                <label for=\"bf-layout-id\">{{ lang.instance_layout }}</label>
                                <select name=\"bf[layout_id]\" id=\"bf-layout-id\" class=\"bf-layout-select bf-w195\">
                                    <option value=\"0\" selected=\"selected\">{{ lang.select }}</option>
                                    {% for  id, layout in layouts %}
                                        <option value=\"{{ id }}\">{{ layout }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-position\">{{ lang.instance_position }}</label>
                                <select name=\"bf[layout_position]\" id=\"bf-layout-position\" class=\"bf-layout-position bf-w195\">
                                    <option value=\"content_top\">{{ lang.instance_content_top }}</option>
                                    <option value=\"content_bottom\">{{ lang.instance_content_bottom }}</option>
                                    <option value=\"column_left\">{{ lang.instance_column_left }}</option>
                                    <option value=\"column_right\">{{ lang.instance_column_right }}</option>
                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-sort-order\">{{ lang.sort_order }}</label>
                                <input type=\"text\" name=\"bf[layout_sort_order]\" id=\"bf-layout-sort-order\" class=\"bf-layout-sort bf-w65\" />
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label center\">{{ lang.enabled }}</span>
                                <div class=\"bf-layout-enable yesno\">
                                    <span class=\"bf-switcher\">
                                        <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[layout_enabled]\" value=\"0\" />
                                        <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                    </span>
                                </div>
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label\">&nbsp;</span>
                                <a href=\"{{ removeInstanceAction }}\" class=\"bf-remove-layout\" onclick=\"if (!window.confirm(BF.lang.confirm_remove_layout)) return false;\">{{ lang.instance_remove }}</a>
                            </div>

                        </div>
                        {% endif %}
                        {# Basic section tabs #}
                        <ul class=\"tabs vertical clearfix\">
                            {% if moduleId != 'basic' %}
                            <li class=\"tab cat-tab
                                {% if settings['layout_id'] is not defined or settings['layout_id'] not in [category_layouts[0]] %} hidden {% endif %}
                                {% if settings['current_adm_tab'] is same as ('categories') %} selected {% endif %}\" data-tab-name=\"categories\" data-target=\"#bf-categories\">{{ lang.tab_categories }}</li>
                            {% endif %}
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('embedding') %} selected {% endif %}\" data-tab-name=\"embedding\" data-target=\"#bf-filter-embedding\">{{ lang.tab_embedding }}</li>
                            {% if moduleId is same as ('basic') %}
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('blocks') %} selected {% endif %}\" data-tab-name=\"blocks\" data-target=\"#bf-filter-blocks-display\">{{ lang.tab_attributes_display }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('layout') %} selected {% endif %}\" data-tab-name=\"layout\" data-target=\"#bf-filter-layout-view\">{{ lang.tab_filter_layout_view }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('submission') %} selected {% endif %}\" data-tab-name=\"submission\" data-target=\"#bf-data-submission\">{{ lang.tab_data_submission }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('attributes') %} selected {% endif %}\" data-tab-name=\"attributes\" data-target=\"#bf-attributes\">{{ lang.tab_attributes }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('options') %} selected {% endif %}\" data-tab-name=\"options\" data-target=\"#bf-options\">{{ lang.tab_options }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('filters') %} selected {% endif %}\" data-tab-name=\"filters\" data-target=\"#bf-filters\">{{ lang.tab_filters }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('responsive') %} selected {% endif %}\" data-tab-name=\"responsive\" data-target=\"#bf-responsive\">{{ lang.tab_responsive_view }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('style') %} selected {% endif %}\" data-tab-name=\"style\" data-target=\"#bf-style\">{{ lang.tab_style }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('global') %} selected {% endif %}\" data-tab-name=\"global\" data-target=\"#bf-global-settings\">{{ lang.tab_global_settings }}</li>
                            {% endif %}
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('attr_values') %} selected {% endif %}\" data-tab-name=\"attr_values\" data-target=\"#bf-attr-values\">{{ lang.tab_attr_values }}</li>
                            <li class=\"tab {% if settings['current_adm_tab'] is same as ('help') %} selected {% endif %}\" data-tab-name=\"help\" data-target=\"#bf-help\">{{ lang.tab_help }}</li>
                            <li id=\"refresh-btn-wrapper\">
                                <button onclick=\"BF.refreshDB();return false;\" class=\"bf-button\" id=\"bf-refresh-db\">
                                    <span class=\"icon bf-update\"></span><span class=\"lbl\">{{ lang.update_cache }}</span>
                                </button>
                            </li>
                        </ul>
                        {# Containers for Filter Embedding #}
                        <div id=\"bf-filter-embedding\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.embedding_header }}</div>
                            <div class=\"bf-alert\" style=\"display: block;\">{{ lang.embedding_warning }}</div>
                            <p class=\"bf-info\">{{ lang.embedding_description }}</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-container-selector\">{{ lang.embedding_container_selector }}</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][containerSelector]\" value=\"\" id=\"bf-container-selector\" placeholder=\"{{ basicSettings['behaviour']['containerSelector'] }}\" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-paginator-selector\">{{ lang.embedding_paginator_selector }}</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][paginatorSelector]\" value=\"\" id=\"bf-paginator-selector\" placeholder=\"{{ basicSettings['behaviour']['paginatorSelector'] }}\" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        {% if moduleId is same as ('basic') %}
                        {# Filter Blocks Display #}
                        <div id=\"bf-filter-blocks-display\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.filter_blocks_header }}</div>
                            <p class=\"bf-info\">{{ lang.filter_blocks_descr }}</p>
                            <table class=\"bf-adm-table\" id=\"bf-filter-sections\">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class=\"center\">{{ lang.enabled }}</th>
                                    <th class=\"bf-collapse-td\">{{ lang.collapse }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {% for filter in filterBlocks %}
                                <tr class=\"bf-sort\" data-section=\"{{ filter['name'] }}\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">{{ filter['label'] }}</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-{{ filter['name'] }}-filter\" type=\"hidden\" name=\"bf[behaviour][sections][{{ filter['name'] }}][enabled]\" value=\"0\" data-disable-adv=\"section-{{ filter['name'] }}\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td class=\"center bf-collapse-td\">
                                        <input id=\"bf-{{ filter['name'] }}-collapse\" type=\"checkbox\" name=\"bf[behaviour][sections][{{ filter['name'] }}][collapsed]\" value=\"1\" data-adv-group=\"section-{{ filter['name'] }}\" />
                                    </td>
                                    <td>
                                        {% if filter['control'] is defined and possible_controls[filter['name']] is defined %}
                                        <select name=\"bf[behaviour][sections][{{ filter['name'] }}][control]\" data-adv-group=\"section-{{ filter['name'] }}\">
                                            {% for val, lbl in possible_controls[filter['name']] %}
                                            <option value=\"{{ val }}\">{{ lbl }}</option>
                                            {% endfor %}
                                        </select>
                                        {% endif %}
                                    </td>
                                </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                        {# Filter Layout View #}
                        <div id=\"bf-filter-layout-view\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.layout_header }}</div>
                            <div>
                                <table class=\"bf-adm-table\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center bf-w165\">{{ lang.enabled }}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                            {{ lang.layout_show_attr_groups }}
                                            </span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-attr-group\" type=\"hidden\" name=\"bf[behaviour][attribute_groups]\" value=\"0\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-adv-group-cont\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            {{ lang.layout_product_count }}</span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-product-count\" type=\"hidden\" name=\"bf[behaviour][product_count]\" value=\"0\" data-disable-adv=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            {{ lang.layout_hide_empty_attr }}</span>
                                        </td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-hide-empty\" type=\"hidden\" name=\"bf[behaviour][hide_empty]\" value=\"0\" data-adv-group=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-intersect-cont\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            {{ lang.layout_sliding }}</span>
                                        </td>
                                        <td class=\"bf-intersect center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-sliding\" type=\"hidden\" name=\"bf[behaviour][limit_items][enabled]\" value=\"0\" data-disable-adv=\"sliding\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\" style=\"padding-top: 5px;padding-bottom: 5px;\"> 
                                            <input id=\"bf-number-to-show\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_show]\" value=\"\" data-adv-group=\"sliding\" />
                                            <label for=\"bf-number-to-show\">{{ lang.layout_sliding_num_to_show }}</label>
                                            <div class=\"bf-suboption\">
                                                <input id=\"bf-number-to-hide\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_hide]\" value=\"\" data-adv-group=\"sliding\" /> 
                                                <label for=\"bf-number-to-hide\">{{ lang.layout_sliding_min }}</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            {{ lang.layout_height_limit }}</span>
                                        </td>
                                        <td class=\"bf-intersect center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-limit-height\" type=\"hidden\" name=\"bf[behaviour][limit_height][enabled]\" value=\"0\" data-disable-adv=\"limit-height\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"> 
                                            <input id=\"bf-limit-height\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_height][height]\" value=\"\" data-adv-group=\"limit-height\" /> 
                                            <label for=\"bf-limit-height\">{{ lang.layout_max_height_limit }}</label>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        {# Data Submission Tab #}
                        <div id=\"bf-data-submission\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.submission_header }}</div>
                            <p class=\"bf-info\">{{ lang.submission_descr }}</p>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">Enabled</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-auto\">{{ lang.submission_type_auto }}</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-auto\" type=\"radio\" value=\"auto\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-delay\">{{ lang.submission_delay }}</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-delay\" type=\"radio\" value=\"delay\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td>
                                        <input id=\"bf-submit-delay-time\" type=\"text\" name=\"bf[submission][submit_delay_time]\" value=\"\" size=\"4\" maxlength=\"4\" data-adv-group=\"submit-type\" data-for-val=\"delay\" />
                                        <label for=\"bf-submit-delay-time\">{{ lang.submission_time_in_sec }}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\"> 
                                            <label for=\"bf-submit-btn\">{{ lang.submission_type_button }}</label></span>
                                    </td>
                                    <td class=\"center\"> 
                                        <input id=\"bf-submit-btn\" type=\"radio\" value=\"button\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td style=\"padding-top: 5px;padding-bottom: 5px;\">
                                        <input id=\"bf-submit-btn-fixed\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"fix\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                        <label for=\"bf-submit-btn-fixed\">{{ lang.submission_button_fixed }}</label>
                                        <div class=\"bf-suboption\">
                                            <input id=\"bf-submit-btn-float\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"float\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                            <label for=\"bf-submit-btn-float\">{{ lang.submission_button_float }}</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr class=\"bf-local-settings\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">
                                            <label for=\"bf-submit-default\">{{ lang.submission_type_default }}</label>
                                        </span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-default\" type=\"radio\" value=\"default\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" class=\"bf-default\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        {{ lang.submission_hide_panel }}</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-layout\" type=\"hidden\" name=\"bf[submission][hide_panel]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        </div>
                        {% endif %}
                        {# Categories #}
                        {% if moduleId != 'basic' %}
                        <div id=\"bf-categories\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.categories_header }}</div>
                            <p class=\"bf-info\">{{ lang.categories_info }}</p>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-green-info\">{{ lang.categories_list_of_enabled }}</p>
                                <div class=\"bf-gray-panel\">
                                    {{ lang.categories_filter_hint }}
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-enabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-enabled-categories\">{{ lang.categories_select_all }}</a> /
                                    <a data-unselect-all=\"#bf-enabled-categories\">{{ lang.categories_unselect_all }}</a>
                                </div>
                                <div id=\"bf-enabled-categories\" class=\"bf-multi-select\">
                                    {% for category in categories %}
                                        {% if not settings['categories'] is defined or settings['categories'][category['category_id']] is not defined %}
                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][{{ category['category_id'] }}]\" value=\"1\" />
                                            {{ category['name'] }}
                                        </div>
                                        {% endif %}
                                    {% endfor %}
                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                                {{ lang.categories_move_selected }}
                                <br><button class=\"bf-move-right\"></button><br><button class=\"bf-move-left\"></button><br>
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-red-info\">{{ lang.categories_list_of_disabled }}</p>
                                <div class=\"bf-gray-panel\">
                                    {{ lang.categories_filter_hint }}
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-disabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-disabled-categories\">{{ lang.categories_select_all }}</a> /
                                    <a data-unselect-all=\"#bf-disabled-categories\">{{ lang.categories_unselect_all }}</a>
                                </div>
                                <div id=\"bf-disabled-categories\" class=\"bf-multi-select\">
                                    {% if settings['categories'] is defined %}
                                        {% for catId, b in settings['categories'] %}
                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][{{ categories[catId]['category_id'] }}]\" value=\"1\" />
                                            {{ categories[catId]['name'] }}
                                        </div>
                                        {% endfor %}
                                    {% endif %}
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        {% endif %}
                        {% if moduleId is same as ('basic') %}
                        {# Attributes Tab #}
                        <div id=\"bf-attributes\" class=\"tab-content with-border\" data-group=\"settings\" data-select-all-group=\"attributes\">
                            <div class=\"bf-th-header-static\">{{ lang.attributes_header }}</div>
                            <div>
                                <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center\">{{ lang.enabled }}</th>
                                        <th class=\"bf-w165\">{{ lang.control }}</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.attributes_group_setting }}</span></td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[attributes_default][enable_all]\" value=\"0\" data-disable-adv=\"group-attr-control\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td>
                                            <select name=\"bf[attributes_default][control]\" data-adv-group=\"group-attr-control\">
                                                <option value=\"checkbox\">{{ lang.checkbox }}</option>
                                                <option value=\"radio\">{{ lang.radio }}</option>
                                                <option value=\"select\">{{ lang.selectbox }}</option>
                                                <option value=\"slider\">{{ lang.slider }}</option>
                                                <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                                                <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.attributes_individual_set }}</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        {{ lang.autocomplete_hint|format(lang.btn_select_attribute) }}
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-autocomplete\" data-lookup=\"attributes\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-attr-setting-template\" data-target-tbl=\"#custom-attr-settings\">
                                            <i class=\"fa fa-plus\"></i> {{ lang.btn_select_attribute }}
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">{{ lang.attributes_custom_set_descr }}</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"attributes\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                {{ lang.enabled }}
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"2\" class=\"bf-local-settings\">{{ lang.set_all_default }}</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"0\">{{ lang.disable_all }}</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"1\">{{ lang.enable_all }}</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">{{ lang.control }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-attr-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {# Options Tab #}
                        <div id=\"bf-options\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.options_header }}</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                <tr>
                                    <th></th>
                                    <th class=\"center bf-w165\">{{ lang.enabled }}</th>
                                    <th class=\"bf-w165\">{{ lang.control }}</th>
                                    <th class=\"bf-w165\">{{ lang.options_view_mode }}</th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.options_group_setting }}</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[options_default][enable_all]\" value=\"0\" data-disable-adv=\"group-opt-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[options_default][control]\" data-adv-group=\"group-opt-control\" class=\"bf-w165\">
                                            <option value=\"checkbox\">{{ lang.checkbox }}</option>
                                            <option value=\"radio\">{{ lang.radio }}</option>
                                            <option value=\"select\">{{ lang.selectbox }}</option>
                                            <option value=\"slider\">{{ lang.slider }}</option>
                                            <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                                            <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                                            <option value=\"slider_lbl_inp\">{{ lang.grid_of_images }}</option>
                                        </select>
                                    </td>
                                    <td class=\"center\">
                                        <select name=\"bf[options_default][mode]\" class=\"bf-opt-mode bf-w135 d-opt-control\" data-adv-group=\"group-opt-control\" data-bf-role=\"mode\">
                                            <option value=\"label\">{{ lang.options_view_mode_label }}</option>
                                            <option value=\"img_label\">{{ lang.options_view_mode_image_and_label }}</option>
                                            <option value=\"img\">{{ lang.options_view_mode_image }}</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.options_individual_set }}</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        {{ lang.autocomplete_hint|format(lang.btn_select_option) }}
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"options\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-opt-setting-template\" data-target-tbl=\"#custom-opt-settings\">
                                            <i class=\"fa fa-plus\"></i> {{ lang.btn_select_option }}
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">{{ lang.options_custom_set_descr }}</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"options\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">{{ lang.enabled }}</th>
                                                {{ lang.enabled }}
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"options\" data-select-all-val=\"2\" class=\"bf-local-settings\">{{ lang.set_all_default }}</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"0\">{{ lang.disable_all }}</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"1\">{{ lang.enable_all }}</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">{{ lang.control }}</th>
                                            <th class=\"bf-w165\">{{ lang.options_view_mode }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-opt-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {# Filters Tab #}
                        <div id=\"bf-filters\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.filters_header }}</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"filters\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">{{ lang.enabled }}</th>
                                    <th class=\"bf-w165\">{{ lang.control }}</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.filters_group_setting }}</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[filters_default][enable_all]\" value=\"0\" data-disable-adv=\"group-filter-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[filters_default][control]\" data-adv-group=\"group-filter-control\">
                                            <option value=\"checkbox\">{{ lang.checkbox }}</option>
                                            <option value=\"radio\">{{ lang.radio }}</option>
                                            <option value=\"select\">{{ lang.selectbox }}</option>
                                            <option value=\"slider\">{{ lang.slider }}</option>
                                            <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                                            <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.filters_individual_set }}</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        {{ lang.autocomplete_hint|format(lang.btn_select_filter) }}
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"filters\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-filter-setting-template\" data-target-tbl=\"#custom-filter-settings\">
                                            <i class=\"fa fa-plus\"></i> {{ lang.btn_select_filter }}
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">{{ lang.filters_custom_set_descr }}</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"filters\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                {{ lang.enabled }}
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"filters\" data-select-all-val=\"2\" class=\"bf-local-settings\">{{ lang.set_all_default }}</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"0\">{{ lang.disable_all }}</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"1\">{{ lang.enable_all }}</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">{{ lang.control }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-filter-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {# Responsive View #}
                        <div id=\"bf-responsive\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.responsive_header }}</div>
                            <table class=\"bf-adm-table bf-adv-group-cont\">
                                <tr>
                                    <th></th>
                                    <th class=\"bf-w170\"></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">{{ lang.default }}</span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_mode_enable }}</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive\" type=\"hidden\" name=\"bf[style][responsive][enabled]\" value=\"0\" data-disable-adv=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_collapse_sections }}</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive-collapse\" type=\"hidden\" name=\"bf[style][responsive][collapsed]\" value=\"0\" data-adv-group=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_max_width }}</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_max_screen_width }}</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_screen_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_position }}</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-position-left\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"left\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-left\">{{ lang.left }}</label>
                                        <input id=\"bf-responsive-position-right\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"right\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-right\">{{ lang.right }}</label>
                                    </td>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.responsive_offset }}</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-offset\" type=\"text\" name=\"bf[style][responsive][offset]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        {# Theme Tab #}
                         <div id=\"bf-style\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.theme_block_header }}</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><span class=\"bf-local-settings\">{{ lang.default }}</span></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_background }}</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_background][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_background\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_text }}</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_text][val]\" /> 
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_text\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    {% for language in languages %}
                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                                {{ lang.theme_title }} (
                                                <img src=\"{{ language['image_path'] }}\" /> 
                                                {{ language['name'] }})
                                            </span>
                                        </td>
                                        <td colspan=\"2\">
                                            <input type=\"text\" name=\"bf[behaviour][filter_name][{{ language['language_id'] }}]\" value=\"\" class=\"bf-w195\" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    {% endfor %}
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.theme_product_quantity }}</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">{{ lang.default }}</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_background }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_background][val]\"/> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_text }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_text][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                                </div>
                                <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.theme_price_slider }}</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">{{ lang.default }}</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_background }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_active_area_background }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-area-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_area_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_area_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_area_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_border }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_border\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_slider_handle_background }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_slider_handle_border }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_border\", this)}'/></td>
                                    <td></td>
                                </tr>

                            </table>
                            </div>
                             <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.theme_group_block_header }}</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">{{ lang.default }}</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_background }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_text }}</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_text][val]\"  /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.theme_responsive_popup_view }}</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_show_btn_color }}</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_show_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_show_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_show_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.theme_reset_btn_color }}</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_reset_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_reset_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_reset_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        {% endif %}
                        {# Attribute Values Ordering #}
                        <div id=\"bf-attr-values\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.ordering_header }}</div>
                            <div class=\"bf-alert\" style=\"display: block;\">{{ lang.ordering_note }}</div>
                            <p class=\"bf-info\">{{ lang.ordering_descr }}</p>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    {{ lang.ordering_filter_hint }}
                                    <input type=\"text\" class=\"bf-attr-filter bf-full-width\" data-target=\"#bf-attr-list\" />
                                </div>
                                <div id=\"bf-attr-list\" class=\"bf-multi-select\">
                                    {% for attrId, attribute in attributes %}
                                    <div class=\"bf-row\" data-attr-id=\"{{ attrId }}\">
                                        <b>{{ attribute['group'] }}</b> / {{ attribute['name'] }}
                                    </div>
                                    {% endfor %}
                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    <div class=\"buttons\">
                                        <div>{{ lang.ordering_language }}:</div>
                                        <select id=\"bf-attr-val-language\" class=\"bf-w165\">
                                            {% for i, language in languages %}
                                            <option value=\"{{ language['language_id'] }}\">{{ language['name'] }}</option>
                                            {% endfor %}
                                        </select>
                                        <div class=\"bf-pull-right\">
                                            <button class=\"bf-auto-sort\" data-type=\"number\">0..9</button>
                                            <button class=\"bf-auto-sort\" data-type=\"string\">A..Z</button>
                                            <a class=\"bf-button bf-save-btn\" title=\"{{ lang.button_save_n_close }}\"><span class=\"icon\"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div id=\"bf-attr-val-list\" class=\"bf-multi-select\">
                                    
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        {# Global Settings #}
                        <div id=\"bf-global-settings\" class=\"tab-content with-border bf-global-settings\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">{{ lang.global_header }}</div>
                            <p class=\"bf-info\">{{ lang.global_settings_descr }}</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.global_hide_empty_stock }}</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-out-of-stock\" type=\"hidden\" name=\"bf[global][hide_out_of_stock]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class=\"bf-global-settings\">
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        {{ lang.global_postponed_count }}</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-postponed-count\" type=\"hidden\" name=\"bf[global][postponed_count]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">{{ lang.global_enable_multiple_attributes }}</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-multiple-attributes\" type=\"hidden\" name=\"bf[global][multiple_attributes]\" value=\"0\" data-disable-adv=\"attr-separator\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <label for=\"bf-attr-separator\">{{ lang.global_separator }}</label>
                                        <input id=\"bf-attr-separator\" type=\"text\" name=\"bf[global][attribute_separator]\" value=\"\" size=\"4\" data-adv-group=\"attr-separator\" />
                                        {{ lang.global_multiple_attr_separator }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        {{ lang.global_in_stock_status_id }}</span>
                                    </td>
                                    <td class=\"\" colspan=\"2\">
                                        <select name=\"bf[global][instock_status_id]\" class=\"bf-w165\">
                                            {% for status in stockStatuses %}
                                                    <option value=\"{{ status['stock_status_id'] }}\">{{ status['name'] }}</option>
                                            {% endfor %}
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        {{ lang.global_subcats_fix }}</span>
                                    </td>
                                    <td class=\" center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-subcategories\" type=\"hidden\" name=\"bf[global][subcategories_fix]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        {{ lang.entry_cron_secret_key }}
                                            <span class=\"bf-link-highlight\" style=\"padding-top:5px;\">{{ catalogUrl }}index.php?route=extension/module/brainyfilter/cron&key=<b>cron secret key</b></span>
                                        </span>
                                    </td>
                                    <td>
                                        <input id=\"bf-cron-key\" class=\"bf-w165\" type=\"text\" name=\"bf[global][cron_secret_key]\" value=\"\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan=\"3\">
                                    </td>
                                </tr>
                            </table>                    
                        </div>
                        {# FAQ container #}
                        <div id=\"bf-help\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header\" id=\"bf-faq-n-troubleshooting\"><span class=\"icon bf-arrow\"></span>{{ lang.help_faq_n_trouleshooting }}</div>
                            <div style=\"display:none;\">
                                
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>{{ lang.help_about }}</div>
                            <div id=\"bf-about\">
                                <div class=\"bf-about-text\">
                                    {{ lang.help_about_content }}
                                    <hr />
                                    <p>{{ lang.bf_signature }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class=\"bf-signature\">{{ lang.bf_signature }}</div>
</div>

<table style=\"display:none;\">
    <tbody id=\"custom-attr-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][attribute][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[attributes][{i}][enabled]\" value=\"0\" data-disable-adv=\"attr-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[attributes][{i}][control]\" data-adv-group=\"attr-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">{{ lang.checkbox }}</option>
                    <option value=\"radio\">{{ lang.radio }}</option>
                    <option value=\"select\">{{ lang.selectbox }}</option>
                    <option value=\"slider\">{{ lang.slider }}</option>
                    <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                    <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-opt-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][options][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[options][{i}][enabled]\" value=\"0\" data-disable-adv=\"opt-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][control]\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"control\" class=\"bf-w135\">
                    <option value=\"checkbox\">{{ lang.checkbox }}</option>
                    <option value=\"radio\">{{ lang.radio }}</option>
                    <option value=\"select\">{{ lang.selectbox }}</option>
                    <option value=\"slider\">{{ lang.slider }}</option>
                    <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                    <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                    <option value=\"grid\">{{ lang.grid_of_images }}</option>
                </select>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][mode]\" class=\"bf-opt-mode bf-w135\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"mode\">
                    <option value=\"label\">{{ lang.options_view_mode_label }}</option>
                    <option value=\"img_label\">{{ lang.options_view_mode_image_and_label }}</option>
                    <option value=\"img\">{{ lang.options_view_mode_image }}</option>
                </select>
            </td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-filter-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][filter][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[filters][{i}][enabled]\" value=\"0\" data-disable-adv=\"filter-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[filters][{i}][control]\" data-adv-group=\"filter-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">{{ lang.checkbox }}</option>
                    <option value=\"radio\">{{ lang.radio }}</option>
                    <option value=\"select\">{{ lang.selectbox }}</option>
                    <option value=\"slider\">{{ lang.slider }}</option>
                    <option value=\"slider_lbl\">{{ lang.slider_labels_only }}</option>
                    <option value=\"slider_lbl_inp\">{{ lang.slider_labels_and_inputs }}</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>

{# Category list template #}

<div id=\"bf-category-list-tpl\" class=\"bf-category-list\" style=\"display: none;\">
    <div class=\"bf-label\">
        {{ lang.categories }}
        (<a onclick=\"jQuery(this).closest('.bf-category-list').find('input').removeAttr('checked')\">{{ lang.disable_all }}</a>
        <span>/</span>
        <a onclick=\"jQuery(this).closest('.bf-category-list').find('input').attr('checked', 'checked')\">{{ lang.enable_all }}</a>)
    </div>
    <div class=\"bf-cat-list-cont\">
        <ul data-select-all-group=\"categories\">
            {% for cat in categories %}
            <li>
                <label>
                    <input type=\"checkbox\" name=\"bf[categories][{{ cat['category_id'] }}]\" value=\"1\" />
                    {{ cat['name'] }}
                </label>
            </li>
            {% endfor %}
        </ul>
    </div>
</div>

{# End of Category list template #}
<script>
BF.lang = {
        'default' : '{{ lang.default }}',
        'error_layout_not_set' : '{{ lang.message_error_layout_not_set }}',
        'error_cant_remove_default' : '{{ lang.instance_remove_default_layout }}',
        'default_layout' : '{{ lang.instance_default_layout }}',
        'confirm_remove_layout' : '{{ lang.instance_remove_confirmation }}',
        'confirm_unsaved_changes' : '{{ lang.message_unsaved_changes }}',
        'updating' : '{{ lang.updating }}',
        'empty_table' : '{{ lang.message_empty_table }}',
        'content_top' : '{{ lang.instance_content_top }}',
        'column_left' : '{{ lang.instance_column_left }}',
        'column_right' : '{{ lang.instance_column_right }}',
        'content_bottom' : '{{ lang.instance_content_bottom }}'
    };
BF.moduleId = '{{ isNewInstance ? 'new' : moduleId }}';
BF.settings = {{ settings|json_encode() }};
BF.attributes = {{ attributes|json_encode() }};
BF.options = {{ options|json_encode() }};
BF.filters = {{ filters|json_encode() }};
BF.refreshActionUrl = '{{ refreshAction|replace({'&amp;': '&' }) }}';
BF.modRefreshActionUrl = '{{ modRefreshAction|replace({'&amp;': '&' }) }}';
BF.attrValActionUrl = '{{ attributeValuesAction|replace({'&amp;': '&' }) }}';
BF.isFirstLaunch = {{ isFirstLaunch }};
BF.categoryLayouts = {{ category_layouts|json_encode() }};
jQuery(document).ready(BF.init());
</script>

<style>
    .bf-def:before {
        content: '{{ lang.default }}';
    }
    .bf-no:before {
        content: '{{ lang.no }}';
    }
    .bf-yes:before {
        content: '{{ lang.yes }}';
    }
    .bf-disable-enable .bf-no:before {
        content: '{{ lang.disable_all }}';
    }
    .bf-disable-enable .bf-yes:before {
        content: '{{ lang.enable_all }}';
    }
</style>
{{ footer }}", "extension/module/brainyfilter.twig", "");
    }
}
