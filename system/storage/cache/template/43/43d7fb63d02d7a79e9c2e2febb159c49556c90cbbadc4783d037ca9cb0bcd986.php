<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/menu.twig */
class __TwigTemplate_966ac2178798cabab4ad5f8b47415df6ba42d4fdd96bd92517b3efe0c8e70a2e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "
  <nav id=\"menu\" class=\"navbar\">
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
            // line 4
            echo ($context["text_category"] ?? null);
            echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
      <div class=\"container\">
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
        <div class=\"menu-wrap\">
            <div class=\"cat-wrap menu-item-new\"><span class=\"catalog-name\">Каталог</span>
              <ul class=\"nav navbar-nav category-all\">
                ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 13
                echo "                ";
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 13)) {
                    // line 14
                    echo "                <li class=\"dropdown\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 14);
                    echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 14);
                    echo "</a>
                  <div class=\"dropdown-menu\">
                    <div class=\"dropdown-inner\"> ";
                    // line 16
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 16), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 16)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 16), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 17
                        echo "                      <ul class=\"list-unstyled\">
                        ";
                        // line 18
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 19
                            echo "                        <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 19);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 19);
                            echo "</a></li>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 21
                        echo "                      </ul>
                      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 22
                    echo "</div>
                    <a href=\"";
                    // line 23
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 23);
                    echo "\" class=\"see-all\">";
                    echo ($context["text_all"] ?? null);
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 23);
                    echo "</a> </div>
                </li>
                ";
                } else {
                    // line 26
                    echo "                <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 26);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 26);
                    echo "</a></li>
                ";
                }
                // line 28
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "              </ul>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/about_us\">О компании</a>
                    <div class=\"dropdown-other\">
                        <ul class=\"ul-other\">
                            <li class=\"\"><a href=\"/rekvizity\" class=\"\">Реквизиты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">Как купить?
                    <div class=\"dropdown-other\">
                        <ul class=\"ul-other\">
                            <li><a href=\"/delivery\">Доставка и оплата</a></li>
                            <li><a href=\"/vozvrat-i-obmen-garantiya\">Возврат и обмен. Гарантия</a></li>
                            <li><a href=\"/politika-konfidencialnosti\">Политика конфиденциальности</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/akcii\">Акции</a>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/kontakty\">Контакты</a>
                </div>
            </div>
        </div>
    </div>
      </div>
  </nav>

";
        }
        // line 67
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 67,  125 => 29,  119 => 28,  111 => 26,  101 => 23,  98 => 22,  91 => 21,  80 => 19,  76 => 18,  73 => 17,  69 => 16,  61 => 14,  58 => 13,  54 => 12,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}

  <nav id=\"menu\" class=\"navbar\">
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">{{ text_category }}</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
      <div class=\"container\">
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
        <div class=\"menu-wrap\">
            <div class=\"cat-wrap menu-item-new\"><span class=\"catalog-name\">Каталог</span>
              <ul class=\"nav navbar-nav category-all\">
                {% for category in categories %}
                {% if category.children %}
                <li class=\"dropdown\"><a href=\"{{ category.href }}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\">
                    <div class=\"dropdown-inner\"> {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                      <ul class=\"list-unstyled\">
                        {% for child in children %}
                        <li><a href=\"{{ child.href }}\">{{ child.name }}</a></li>
                        {% endfor %}
                      </ul>
                      {% endfor %}</div>
                    <a href=\"{{ category.href }}\" class=\"see-all\">{{ text_all }} {{ category.name }}</a> </div>
                </li>
                {% else %}
                <li><a href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
              </ul>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/about_us\">О компании</a>
                    <div class=\"dropdown-other\">
                        <ul class=\"ul-other\">
                            <li class=\"\"><a href=\"/rekvizity\" class=\"\">Реквизиты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">Как купить?
                    <div class=\"dropdown-other\">
                        <ul class=\"ul-other\">
                            <li><a href=\"/delivery\">Доставка и оплата</a></li>
                            <li><a href=\"/vozvrat-i-obmen-garantiya\">Возврат и обмен. Гарантия</a></li>
                            <li><a href=\"/politika-konfidencialnosti\">Политика конфиденциальности</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/akcii\">Акции</a>
                </div>
            </div>
            <div class=\"menu-item-new\">
                <div class=\"catalog-name other-items\">
                    <a href=\"/kontakty\">Контакты</a>
                </div>
            </div>
        </div>
    </div>
      </div>
  </nav>

{% endif %} ", "default/template/common/menu.twig", "");
    }
}
