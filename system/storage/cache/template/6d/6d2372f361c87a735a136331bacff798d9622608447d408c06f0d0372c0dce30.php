<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/products_insert.twig */
class __TwigTemplate_9db60078eae3e9c96ba8f2d7cb807df29dae3856afee9c87b87e8c1fe8ca6494 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 22
        echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"";
        // line 28
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">";
        // line 32
        echo ($context["text_import"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"";
        // line 36
        echo ($context["text_tips_import"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>

                    <div class=\"form-group\">

                        ";
        // line 50
        echo "
                    </div>

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 55
        echo ($context["entry_status"] ?? null);
        echo "</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                ";
        // line 59
        if (($context["module_xml_module_status"] ?? null)) {
            // line 60
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\">";
            // line 61
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        } else {
            // line 63
            echo "                                    <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 64
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        }
        // line 66
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-test\"
                      style=\"border: 1px solid red;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test
                </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token=";
        // line 95
        echo ($context["user_token"] ?? null);
        echo "',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

";
        // line 132
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/products_insert.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 132,  195 => 95,  164 => 66,  159 => 64,  154 => 63,  149 => 61,  144 => 60,  142 => 59,  135 => 55,  128 => 50,  117 => 36,  110 => 32,  103 => 28,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}{{ column_left }}
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"{{ button_save }}\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"{{ cancel }}\" data-toggle=\"tooltip\" title=\"{{ button_cancel }}\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>{{ heading_title }}</h1>
            <ul class=\"breadcrumb\">
                {% for breadcrumb in breadcrumbs %}
                    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
                {% endfor %}
            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        {% if error_warning %}
            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> {{ error_warning }}
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        {% endif %}
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>{{ text_edit }}</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">{{ text_import }}</label>
                        <div class=\"col-sm-10\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"{{ text_tips_import }}\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>

                    <div class=\"form-group\">

                        {# <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">{{ text_export }}</label>
                        <div class=\"col-sm-10\">
                          <div class=\"input-group\" style=\"padding-top: 10px;\">
                            <a href=\"{{ export_xml }}\" target=\"_blank\">{{ export_xml }}</a>
                          </div>
                        </div> #}

                    </div>

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">{{ entry_status }}</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                {% if module_xml_module_status %}
                                    <option value=\"1\" selected=\"selected\">{{ text_enabled }}</option>
                                    <option value=\"0\">{{ text_disabled }}</option>
                                {% else %}
                                    <option value=\"1\">{{ text_enabled }}</option>
                                    <option value=\"0\" selected=\"selected\">{{ text_disabled }}</option>
                                {% endif %}
                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-test\"
                      style=\"border: 1px solid red;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test
                </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token={{ user_token }}',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

{{ footer }}
", "extension/module/products_insert.twig", "");
    }
}
