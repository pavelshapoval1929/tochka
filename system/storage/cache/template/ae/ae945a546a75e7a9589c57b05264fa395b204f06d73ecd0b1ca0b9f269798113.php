<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/update_prices.twig */
class __TwigTemplate_e5ba906528fdfdc82ef6b18023070a1a42e85e69e0494a4a925b485ffd9ab9d9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 22
        echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"";
        // line 28
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\" style=\"display: none\">";
        // line 32
        echo ($context["text_import"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\" style=\"display: none\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"";
        // line 35
        echo ($context["xml_import_link"] ?? null);
        echo "\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"";
        // line 36
        echo ($context["text_tips_import"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>


                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 45
        echo ($context["entry_status"] ?? null);
        echo "</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                ";
        // line 49
        if (($context["module_xml_module_status"] ?? null)) {
            // line 50
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\">";
            // line 51
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        } else {
            // line 53
            echo "                                    <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 54
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        }
        // line 56
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">

                    </div>


                    <hr>

                    <div id=\"loader\"
                         style=\"display: none;
                 position: fixed;
                 top: 50%;
                 left: 50%;
                 background: url(/admin/view/image/loading_csv.gif);
                 width: 64px;
                 height: 64px;\">
                    </div>
                </form>

                <form class=\"set_prices\" action=\"";
        // line 77
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                    <p>Форма увеличения цены по производителю</p>
                    <label for=\"manufacturer\" style=\"display: block;\">Производитель</label>
                    <select name=\"manufacturer\" id=\"manufacturer\" style=\"height: 35px; padding: 5px;\">Производители
                        ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["all_manufacturers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
            // line 82
            echo "                            <option value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["manufacturer"], "manufacturer_id", [], "any", false, false, false, 82);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["manufacturer"], "name", [], "any", false, false, false, 82);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "                    </select>
                    <br>
                    <label for=\"percent\" style=\"display: block; margin-top: 20px\">Процент</label>
                    <input type=\"number\" class=\"percent\" placeholder=\"введите число\" name=\"percent\" style=\"display: block; margin: 10px 0; height: 35px; padding: 5px;\">
                    <input type=\"button\" class=\"set_prices_submit\" value=\"Увеличить цену\" style=\"height: 30px; display: block; margin-top: 10px;\">
                </form>

                <hr style=\"border-top: 1px solid #000\">

                <form class=\"set_prices\" action=\"";
        // line 93
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                    <p>Форма умненьшения цены по производителю</p>
                    <label for=\"manufacturer\" style=\"display: block;\">Производитель</label>
                    <select name=\"manufacturer\" id=\"manufacturer\" style=\"height: 35px; padding: 5px;\">Производители
                        ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["all_manufacturers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
            // line 98
            echo "                            <option value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["manufacturer"], "manufacturer_id", [], "any", false, false, false, 98);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["manufacturer"], "name", [], "any", false, false, false, 98);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "                    </select>
                    <br>
                    <label for=\"percent\" style=\"display: block; margin-top: 20px\">Процент</label>
                    <input type=\"number\" class=\"percent\" placeholder=\"введите число\" name=\"percent\" style=\"display: block; margin: 10px 0; height: 35px; padding: 5px;\">
                    <input type=\"button\" class=\"low_prices_submit\" value=\"Уменьшить цену\" style=\"height: 30px; display: block; margin-top: 10px;\">
                </form>
                <style>
                    .set_prices_submit,
                    .low_prices_submit{
                        color: #fff;
                        background: #006ecd;
                    }
                    .set_prices_submit:hover,
                    .low_prices_submit:hover{
                        color: #fff;
                        background: #2a8ee4;
                    }
                </style>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token=";
        // line 134
        echo ($context["user_token"] ?? null);
        echo "',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

<script type=\"text/javascript\">
    \$(document).on('click', '.start', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_prodat&user_token=";
        // line 178
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-pricat', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_pricat&user_token=";
        // line 206
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer&user_token=";
        // line 234
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer-id', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer_id&user_token=";
        // line 262
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });


    \$(document).on('click', '.start-update-time', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_update_time&user_token=";
        // line 291
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.update-properties', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_properties&user_token=";
        // line 322
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$(document).on('click', '.update-descriptions', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_descriptions&user_token=";
        // line 363
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
    \$('.set_filters_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_filters&user_token=";
        // line 399
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$('.set_prices_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();

        //var form_data = \$(this).parent('form').find('#manufacturer').val();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/update_prices/update_prices_up&user_token=";
        // line 438
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(response){
                alert(response);
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });


    \$('.low_prices_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();

        //var form_data = \$(this).parent('form').find('#manufacturer').val();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/update_prices/update_prices_down&user_token=";
        // line 478
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(response){
                alert(response);
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
</script>

";
        // line 511
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/update_prices.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  685 => 511,  649 => 478,  606 => 438,  564 => 399,  525 => 363,  481 => 322,  447 => 291,  415 => 262,  384 => 234,  353 => 206,  322 => 178,  275 => 134,  239 => 100,  228 => 98,  224 => 97,  217 => 93,  206 => 84,  195 => 82,  191 => 81,  184 => 77,  161 => 56,  156 => 54,  151 => 53,  146 => 51,  141 => 50,  139 => 49,  132 => 45,  120 => 36,  116 => 35,  110 => 32,  103 => 28,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}{{ column_left }}
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"{{ button_save }}\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"{{ cancel }}\" data-toggle=\"tooltip\" title=\"{{ button_cancel }}\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>{{ heading_title }}</h1>
            <ul class=\"breadcrumb\">
                {% for breadcrumb in breadcrumbs %}
                    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
                {% endfor %}
            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        {% if error_warning %}
            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> {{ error_warning }}
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        {% endif %}
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>{{ text_edit }}</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\" style=\"display: none\">{{ text_import }}</label>
                        <div class=\"col-sm-10\" style=\"display: none\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"{{ xml_import_link }}\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"{{ text_tips_import }}\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>


                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">{{ entry_status }}</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                {% if module_xml_module_status %}
                                    <option value=\"1\" selected=\"selected\">{{ text_enabled }}</option>
                                    <option value=\"0\">{{ text_disabled }}</option>
                                {% else %}
                                    <option value=\"1\">{{ text_enabled }}</option>
                                    <option value=\"0\" selected=\"selected\">{{ text_disabled }}</option>
                                {% endif %}
                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">

                    </div>


                    <hr>

                    <div id=\"loader\"
                         style=\"display: none;
                 position: fixed;
                 top: 50%;
                 left: 50%;
                 background: url(/admin/view/image/loading_csv.gif);
                 width: 64px;
                 height: 64px;\">
                    </div>
                </form>

                <form class=\"set_prices\" action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\">
                    <p>Форма увеличения цены по производителю</p>
                    <label for=\"manufacturer\" style=\"display: block;\">Производитель</label>
                    <select name=\"manufacturer\" id=\"manufacturer\" style=\"height: 35px; padding: 5px;\">Производители
                        {% for manufacturer in all_manufacturers %}
                            <option value=\"{{ manufacturer.manufacturer_id }}\">{{ manufacturer.name }}</option>
                        {% endfor %}
                    </select>
                    <br>
                    <label for=\"percent\" style=\"display: block; margin-top: 20px\">Процент</label>
                    <input type=\"number\" class=\"percent\" placeholder=\"введите число\" name=\"percent\" style=\"display: block; margin: 10px 0; height: 35px; padding: 5px;\">
                    <input type=\"button\" class=\"set_prices_submit\" value=\"Увеличить цену\" style=\"height: 30px; display: block; margin-top: 10px;\">
                </form>

                <hr style=\"border-top: 1px solid #000\">

                <form class=\"set_prices\" action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\">
                    <p>Форма умненьшения цены по производителю</p>
                    <label for=\"manufacturer\" style=\"display: block;\">Производитель</label>
                    <select name=\"manufacturer\" id=\"manufacturer\" style=\"height: 35px; padding: 5px;\">Производители
                        {% for manufacturer in all_manufacturers %}
                            <option value=\"{{ manufacturer.manufacturer_id }}\">{{ manufacturer.name }}</option>
                        {% endfor %}
                    </select>
                    <br>
                    <label for=\"percent\" style=\"display: block; margin-top: 20px\">Процент</label>
                    <input type=\"number\" class=\"percent\" placeholder=\"введите число\" name=\"percent\" style=\"display: block; margin: 10px 0; height: 35px; padding: 5px;\">
                    <input type=\"button\" class=\"low_prices_submit\" value=\"Уменьшить цену\" style=\"height: 30px; display: block; margin-top: 10px;\">
                </form>
                <style>
                    .set_prices_submit,
                    .low_prices_submit{
                        color: #fff;
                        background: #006ecd;
                    }
                    .set_prices_submit:hover,
                    .low_prices_submit:hover{
                        color: #fff;
                        background: #2a8ee4;
                    }
                </style>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token={{ user_token }}',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

<script type=\"text/javascript\">
    \$(document).on('click', '.start', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_prodat&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-pricat', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_pricat&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer-id', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer_id&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });


    \$(document).on('click', '.start-update-time', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_update_time&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.update-properties', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_properties&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$(document).on('click', '.update-descriptions', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_descriptions&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
    \$('.set_filters_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_filters&user_token={{ user_token }}',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$('.set_prices_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();

        //var form_data = \$(this).parent('form').find('#manufacturer').val();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/update_prices/update_prices_up&user_token={{ user_token }}',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(response){
                alert(response);
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });


    \$('.low_prices_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();

        //var form_data = \$(this).parent('form').find('#manufacturer').val();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/update_prices/update_prices_down&user_token={{ user_token }}',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(response){
                alert(response);
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
</script>

{{ footer }}
", "extension/module/update_prices.twig", "");
    }
}
