<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/header.twig */
class __TwigTemplate_7399ddfbc785a0cf6801611ce95f38847cffb26e7902f17c01309d6c289b2002 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo ($context["title"] ?? null);
        echo "</title>
<base href=\"";
        // line 13
        echo ($context["base"] ?? null);
        echo "\" />
";
        // line 14
        if (($context["alternate"] ?? null)) {
            echo ($context["alternate"] ?? null);
        }
        echo " \t
\t\t\t

";
        // line 17
        if (($context["canonical_link"] ?? null)) {
            echo (("<link href=\"" . ($context["canonical_link"] ?? null)) . "\" rel=\"canonical\" />");
        }
        echo " 
";
        // line 18
        if (($context["robots"] ?? null)) {
            echo ($context["robots"] ?? null);
        }
        echo " 
\t\t\t
";
        // line 20
        if (($context["description"] ?? null)) {
            // line 21
            echo "<meta name=\"description\" content=\"";
            echo ($context["description"] ?? null);
            echo "\" />
";
        }
        // line 23
        if (($context["keywords"] ?? null)) {
            // line 24
            echo "<meta name=\"keywords\" content=\"";
            echo ($context["keywords"] ?? null);
            echo "\" />
";
        }
        // line 26
        echo "<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"catalog/view/theme/default/stylesheet/stylesheet.css\" rel=\"stylesheet\">
<link href=\"catalog/view/theme/default/stylesheet/theme.css\" rel=\"stylesheet\">
";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 34
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 34);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 34);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 34);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 37
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/jquery.inputmask.min.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/ajax.js\" type=\"text/javascript\"></script>
";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 43
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "href", [], "any", false, false, false, 43);
            echo "\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "rel", [], "any", false, false, false, 43);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 46
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
\t\t\t<link rel=\"stylesheet\" href=\"catalog/view/javascript/jquery.cluetip.css\" type=\"text/css\" />
\t\t\t<script src=\"catalog/view/javascript/jquery.cluetip.js\" type=\"text/javascript\"></script>
\t\t\t
\t\t\t<script type=\"text/javascript\">
\t\t\t\t\$(document).ready(function() {
\t\t\t\t\$('a.title').cluetip({splitTitle: '|'});
\t\t\t\t  \$('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});
\t\t\t\t  \$('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});
\t\t\t\t  \$('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});
\t\t\t\t  \$('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});
\t\t\t\t  \$('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  
\t\t\t\t});
\t\t\t</script>
\t\t\t

\t\t\t\t";
        // line 64
        if (($context["socialseo"] ?? null)) {
            echo ($context["socialseo"] ?? null);
        }
        echo " 
\t\t\t\t
\t\t\t\t";
        // line 66
        if (twig_get_attribute($this->env, $this->source, ($context["richsnippets"] ?? null), "store", [], "any", false, false, false, 66)) {
            echo " 
<script type=\"application/ld+json\">
\t\t\t\t{ \"@context\" : \"http://schema.org\",
\t\t\t\t  \"@type\" : \"Organization\",
\t\t\t\t  \"name\" : \"";
            // line 70
            echo ($context["name"] ?? null);
            echo "\",
\t\t\t\t  \"url\" : \"";
            // line 71
            echo ($context["home"] ?? null);
            echo "\",
\t\t\t\t  \"logo\" : \"";
            // line 72
            echo ($context["logo"] ?? null);
            echo "\",
\t\t\t\t  \"contactPoint\" : [
\t\t\t\t\t{ \"@type\" : \"ContactPoint\",
\t\t\t\t\t  \"telephone\" : \"";
            // line 75
            echo ($context["telephone"] ?? null);
            echo "\",
\t\t\t\t\t  \"contactType\" : \"customer service\"
\t\t\t\t\t} ] }
\t\t\t\t</script>
\t\t\t\t";
        }
        // line 79
        echo " \t\t\t\t
\t\t\t
</head>
<body>
<div class=\"wrapper-body\">
";
        // line 85
        echo "<div class=\"layer\"></div>
<div class=\"form-wrap\">
    <p class=\"form-capt\">Заказать звонок</p>

    <p class=\"form-txt\">Оставьте пожалуйста свои контактные данные. Наши менеджеры свяжутся с вами для уточнения деталей заказа</p>
    <form class=\"modform\" action=\"\" method=\"POST\">
        <label for=\"form-name\">Ваше имя*</label>
        <input class=\"form-input-txt\" value=\"\" name=\"form-name\" placeholder=\"Как к вам обращаться\">
        <label for=\"form-tel\">Телефон*</label>
        <input class=\"form-input-txt form-phone\" value=\"\" name=\"form-tel\" placeholder=\"+7(000) 000 00 00\">
        <input type=\"submit\" class=\"form-input-submit\" value=\"Отправить\">
        <!--вставка фронта-->
        <div class=\"g-recaptcha\" data-sitekey=\"6LdPDxYbAAAAAFEq5lpQuvDXZ_90KuNW6GAgcm6R\"></div>
        <div class=\"text-danger\" id=\"recaptchaError\"></div>
        <p class=\"form-politika\">Нажатием кнопки \"Отправить\" я даю свое согласие на обработку <a href=\"#\">персональных данных</a></p>
    </form>


    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
    <!--капча-->
    <script src=\"https://www.google.com/recaptcha/api.js\"></script>
</div>
<div class=\"form-alert1\">
    <p class=\"form-capt\">Спасибо, форма отправлена!</p>
    <p class=\"form-txt\">Наши менеджеры свяжутся с вами в ближайшее время.</p>
    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
</div>
<div class=\"form-alert2\">
    <p class=\"form-capt\">Ошибка!</p>
    <p class=\"form-txt\">При заполнении формы произошла ошибка. Проверьте правильность заполнения полей формы.</p>
    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
</div>

";
        // line 131
        echo "<nav id=\"top\">
  <div class=\"container\">";
        // line 132
        echo ($context["currency"] ?? null);
        echo "
    ";
        // line 133
        echo ($context["language"] ?? null);
        echo "
    <div id=\"top-links\" class=\"nav pull-right\">
      <ul class=\"list-inline\">
        <li><a href=\"";
        // line 136
        echo ($context["contact"] ?? null);
        echo "\"><i class=\"fa fa-phone\"></i></a> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["telephone"] ?? null);
        echo "</span></li>
        <li class=\"dropdown\"><a href=\"";
        // line 137
        echo ($context["account"] ?? null);
        echo "\" title=\"";
        echo ($context["text_account"] ?? null);
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_account"] ?? null);
        echo "</span> <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu dropdown-menu-right\">
            ";
        // line 139
        if (($context["logged"] ?? null)) {
            // line 140
            echo "            <li><a href=\"";
            echo ($context["account"] ?? null);
            echo "\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
            <li><a href=\"";
            // line 141
            echo ($context["order"] ?? null);
            echo "\">";
            echo ($context["text_order"] ?? null);
            echo "</a></li>
            <li><a href=\"";
            // line 142
            echo ($context["transaction"] ?? null);
            echo "\">";
            echo ($context["text_transaction"] ?? null);
            echo "</a></li>
            <li><a href=\"";
            // line 143
            echo ($context["download"] ?? null);
            echo "\">";
            echo ($context["text_download"] ?? null);
            echo "</a></li>
            <li><a href=\"";
            // line 144
            echo ($context["logout"] ?? null);
            echo "\">";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
            ";
        } else {
            // line 146
            echo "            <li><a href=\"";
            echo ($context["register"] ?? null);
            echo "\">";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
            <li><a href=\"";
            // line 147
            echo ($context["login"] ?? null);
            echo "\">";
            echo ($context["text_login"] ?? null);
            echo "</a></li>
            ";
        }
        // line 149
        echo "          </ul>
        </li>
        <li><a href=\"";
        // line 151
        echo ($context["wishlist"] ?? null);
        echo "\" id=\"wishlist-total\" title=\"";
        echo ($context["text_wishlist"] ?? null);
        echo "\"><i class=\"fa fa-heart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_wishlist"] ?? null);
        echo "</span></a></li>
        <li><a href=\"";
        // line 152
        echo ($context["shopping_cart"] ?? null);
        echo "\" title=\"";
        echo ($context["text_shopping_cart"] ?? null);
        echo "\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_shopping_cart"] ?? null);
        echo "</span></a></li>
        <li><a href=\"";
        // line 153
        echo ($context["checkout"] ?? null);
        echo "\" title=\"";
        echo ($context["text_checkout"] ?? null);
        echo "\"><i class=\"fa fa-share\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_checkout"] ?? null);
        echo "</span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-sm-4\">
        <div id=\"logo\">
            ";
        // line 163
        if (($context["logo"] ?? null)) {
            // line 164
            echo "            <a href=\"";
            echo ($context["home"] ?? null);
            echo "\">
                <img src=\"";
            // line 165
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" class=\"img-responsive\" />
                </a>
            ";
        } else {
            // line 168
            echo "                <h1>
                    <a href=\"";
            // line 169
            echo ($context["home"] ?? null);
            echo "\">";
            echo ($context["name"] ?? null);
            echo "</a>
                </h1>
            ";
        }
        // line 172
        echo "        </div>
          <div class=\"site-name\">
              ";
        // line 174
        echo ($context["name"] ?? null);
        echo "
          </div>
      </div>
      <div class=\"col-md-6 col-sm-5\">
          <div class=\"header-contacts-wrap\">
              <div class=\"header-contacts-1\">
                  Продажа лучшей электротехнической продукции
              </div>
              <div class=\"header-contacts-2\">
                  <div class=\"header-time time-1\">Время работы</div>
                  <div class=\"header-time time-2\">Пн-Пт 9:00 - 18:00</div>
                  <div class=\"header-time time-3\">Сб-Вс Выходной</div>
              </div>
              <div class=\"header-contacts-3\">
                  <div class=\"place-1\">г. Омск</div>
                  <div class=\"place-2\">ул. Ватутина, 11в , 2 этаж, 9 офис</div>
              </div>
          </div>
      </div>
      <div class=\"col-md-2 col-sm-3\">
          <div class=\"header-phone\"><a href=\"tel:";
        // line 194
        echo ($context["telephone"] ?? null);
        echo "\">";
        echo ($context["telephone"] ?? null);
        echo "</a></div>
          <div class=\"main-header__mid-phone-callback button-click\">Перезвоните мне</div>
          <div class=\"clearfix\"></div>
          <div class=\"cart-wrap\">";
        // line 197
        echo ($context["cart"] ?? null);
        echo "</div>
      </div>

    </div>
  </div>
</header>
";
        // line 203
        echo ($context["menu"] ?? null);
        echo "

<div class=\"container\">
    ";
        // line 214
        echo "    <div class=\"col-sm-12\">";
        echo ($context["search"] ?? null);
        echo "</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 214,  474 => 203,  465 => 197,  457 => 194,  434 => 174,  430 => 172,  422 => 169,  419 => 168,  409 => 165,  404 => 164,  402 => 163,  385 => 153,  377 => 152,  369 => 151,  365 => 149,  358 => 147,  351 => 146,  344 => 144,  338 => 143,  332 => 142,  326 => 141,  319 => 140,  317 => 139,  308 => 137,  302 => 136,  296 => 133,  292 => 132,  289 => 131,  242 => 85,  235 => 79,  227 => 75,  221 => 72,  217 => 71,  213 => 70,  206 => 66,  199 => 64,  181 => 48,  173 => 46,  169 => 45,  158 => 43,  154 => 42,  149 => 39,  140 => 37,  136 => 36,  123 => 34,  119 => 33,  110 => 26,  104 => 24,  102 => 23,  96 => 21,  94 => 20,  87 => 18,  81 => 17,  73 => 14,  69 => 13,  65 => 12,  54 => 6,  47 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"{{ direction }}\" lang=\"{{ lang }}\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"{{ direction }}\" lang=\"{{ lang }}\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"{{ direction }}\" lang=\"{{ lang }}\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>{{ title }}</title>
<base href=\"{{ base }}\" />
{% if alternate %}{{ alternate }}{% endif %} \t
\t\t\t

{% if canonical_link %}{{ '<link href=\"'~canonical_link~'\" rel=\"canonical\" />' }}{% endif %} 
{% if robots %}{{ robots }}{% endif %} 
\t\t\t
{% if description %}
<meta name=\"description\" content=\"{{ description }}\" />
{% endif %}
{% if keywords %}
<meta name=\"keywords\" content=\"{{ keywords }}\" />
{% endif %}
<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"catalog/view/theme/default/stylesheet/stylesheet.css\" rel=\"stylesheet\">
<link href=\"catalog/view/theme/default/stylesheet/theme.css\" rel=\"stylesheet\">
{% for style in styles %}
<link href=\"{{ style.href }}\" type=\"text/css\" rel=\"{{ style.rel }}\" media=\"{{ style.media }}\" />
{% endfor %}
{% for script in scripts %}
<script src=\"{{ script }}\" type=\"text/javascript\"></script>
{% endfor %}
<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/jquery.inputmask.min.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/javascript/ajax.js\" type=\"text/javascript\"></script>
{% for link in links %}
<link href=\"{{ link.href }}\" rel=\"{{ link.rel }}\" />
{% endfor %}
{% for analytic in analytics %}
{{ analytic }}
{% endfor %}

\t\t\t<link rel=\"stylesheet\" href=\"catalog/view/javascript/jquery.cluetip.css\" type=\"text/css\" />
\t\t\t<script src=\"catalog/view/javascript/jquery.cluetip.js\" type=\"text/javascript\"></script>
\t\t\t
\t\t\t<script type=\"text/javascript\">
\t\t\t\t\$(document).ready(function() {
\t\t\t\t\$('a.title').cluetip({splitTitle: '|'});
\t\t\t\t  \$('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});
\t\t\t\t  \$('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});
\t\t\t\t  \$('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});
\t\t\t\t  \$('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});
\t\t\t\t  \$('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  
\t\t\t\t});
\t\t\t</script>
\t\t\t

\t\t\t\t{% if socialseo %}{{ socialseo }}{% endif %} 
\t\t\t\t
\t\t\t\t{% if richsnippets.store %} 
<script type=\"application/ld+json\">
\t\t\t\t{ \"@context\" : \"http://schema.org\",
\t\t\t\t  \"@type\" : \"Organization\",
\t\t\t\t  \"name\" : \"{{ name }}\",
\t\t\t\t  \"url\" : \"{{ home }}\",
\t\t\t\t  \"logo\" : \"{{ logo }}\",
\t\t\t\t  \"contactPoint\" : [
\t\t\t\t\t{ \"@type\" : \"ContactPoint\",
\t\t\t\t\t  \"telephone\" : \"{{ telephone }}\",
\t\t\t\t\t  \"contactType\" : \"customer service\"
\t\t\t\t\t} ] }
\t\t\t\t</script>
\t\t\t\t{% endif %} \t\t\t\t
\t\t\t
</head>
<body>
<div class=\"wrapper-body\">
{# ** form ** #}
<div class=\"layer\"></div>
<div class=\"form-wrap\">
    <p class=\"form-capt\">Заказать звонок</p>

    <p class=\"form-txt\">Оставьте пожалуйста свои контактные данные. Наши менеджеры свяжутся с вами для уточнения деталей заказа</p>
    <form class=\"modform\" action=\"\" method=\"POST\">
        <label for=\"form-name\">Ваше имя*</label>
        <input class=\"form-input-txt\" value=\"\" name=\"form-name\" placeholder=\"Как к вам обращаться\">
        <label for=\"form-tel\">Телефон*</label>
        <input class=\"form-input-txt form-phone\" value=\"\" name=\"form-tel\" placeholder=\"+7(000) 000 00 00\">
        <input type=\"submit\" class=\"form-input-submit\" value=\"Отправить\">
        <!--вставка фронта-->
        <div class=\"g-recaptcha\" data-sitekey=\"6LdPDxYbAAAAAFEq5lpQuvDXZ_90KuNW6GAgcm6R\"></div>
        <div class=\"text-danger\" id=\"recaptchaError\"></div>
        <p class=\"form-politika\">Нажатием кнопки \"Отправить\" я даю свое согласие на обработку <a href=\"#\">персональных данных</a></p>
    </form>


    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
    <!--капча-->
    <script src=\"https://www.google.com/recaptcha/api.js\"></script>
</div>
<div class=\"form-alert1\">
    <p class=\"form-capt\">Спасибо, форма отправлена!</p>
    <p class=\"form-txt\">Наши менеджеры свяжутся с вами в ближайшее время.</p>
    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
</div>
<div class=\"form-alert2\">
    <p class=\"form-capt\">Ошибка!</p>
    <p class=\"form-txt\">При заполнении формы произошла ошибка. Проверьте правильность заполнения полей формы.</p>
    <svg version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\" width=\"16px\" height=\"16px\" class=\"\"><g><g>
                <g>
                    <path d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z\" data-original=\"#000000\" class=\"active-path\" data-old_color=\"#000000\" fill=\"#666666\"/>
                </g>
            </g></g> </svg>
</div>

{# ** form end ** #}
<nav id=\"top\">
  <div class=\"container\">{{ currency }}
    {{ language }}
    <div id=\"top-links\" class=\"nav pull-right\">
      <ul class=\"list-inline\">
        <li><a href=\"{{ contact }}\"><i class=\"fa fa-phone\"></i></a> <span class=\"hidden-xs hidden-sm hidden-md\">{{ telephone }}</span></li>
        <li class=\"dropdown\"><a href=\"{{ account }}\" title=\"{{ text_account }}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ text_account }}</span> <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu dropdown-menu-right\">
            {% if logged %}
            <li><a href=\"{{ account }}\">{{ text_account }}</a></li>
            <li><a href=\"{{ order }}\">{{ text_order }}</a></li>
            <li><a href=\"{{ transaction }}\">{{ text_transaction }}</a></li>
            <li><a href=\"{{ download }}\">{{ text_download }}</a></li>
            <li><a href=\"{{ logout }}\">{{ text_logout }}</a></li>
            {% else %}
            <li><a href=\"{{ register }}\">{{ text_register }}</a></li>
            <li><a href=\"{{ login }}\">{{ text_login }}</a></li>
            {% endif %}
          </ul>
        </li>
        <li><a href=\"{{ wishlist }}\" id=\"wishlist-total\" title=\"{{ text_wishlist }}\"><i class=\"fa fa-heart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ text_wishlist }}</span></a></li>
        <li><a href=\"{{ shopping_cart }}\" title=\"{{ text_shopping_cart }}\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ text_shopping_cart }}</span></a></li>
        <li><a href=\"{{ checkout }}\" title=\"{{ text_checkout }}\"><i class=\"fa fa-share\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ text_checkout }}</span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-sm-4\">
        <div id=\"logo\">
            {% if logo %}
            <a href=\"{{ home }}\">
                <img src=\"{{ logo }}\" title=\"{{ name }}\" alt=\"{{ name }}\" class=\"img-responsive\" />
                </a>
            {% else %}
                <h1>
                    <a href=\"{{ home }}\">{{ name }}</a>
                </h1>
            {% endif %}
        </div>
          <div class=\"site-name\">
              {{ name }}
          </div>
      </div>
      <div class=\"col-md-6 col-sm-5\">
          <div class=\"header-contacts-wrap\">
              <div class=\"header-contacts-1\">
                  Продажа лучшей электротехнической продукции
              </div>
              <div class=\"header-contacts-2\">
                  <div class=\"header-time time-1\">Время работы</div>
                  <div class=\"header-time time-2\">Пн-Пт 9:00 - 18:00</div>
                  <div class=\"header-time time-3\">Сб-Вс Выходной</div>
              </div>
              <div class=\"header-contacts-3\">
                  <div class=\"place-1\">г. Омск</div>
                  <div class=\"place-2\">ул. Ватутина, 11в , 2 этаж, 9 офис</div>
              </div>
          </div>
      </div>
      <div class=\"col-md-2 col-sm-3\">
          <div class=\"header-phone\"><a href=\"tel:{{ (telephone) }}\">{{ (telephone) }}</a></div>
          <div class=\"main-header__mid-phone-callback button-click\">Перезвоните мне</div>
          <div class=\"clearfix\"></div>
          <div class=\"cart-wrap\">{{ cart }}</div>
      </div>

    </div>
  </div>
</header>
{{ menu }}

<div class=\"container\">
    {#<pre>
    {{ dump(title) }}
        </pre>
    <ol>
        {% for key, value in _context  %}
            <li>{{ key }}</li>
        {% endfor %}
    </ol>#}
    <div class=\"col-sm-12\">{{ search }}</div>

</div>
", "default/template/common/header.twig", "");
    }
}
