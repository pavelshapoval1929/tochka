<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/payment/prodamus.twig */
class __TwigTemplate_d35881d8fe2b0cdcb16c5abb5020f56dac4fe057b93d6c899ca3ded4857e3032 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-prodamus\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
        <li><a href=\"";
            // line 11
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
            echo "\">";
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ( !twig_test_empty(($context["error_warning"] ?? null))) {
            echo " 
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 18
            echo ($context["error_warning"] ?? null);
            echo " 
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 21
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-prodamus\" class=\"form-horizontal\">
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-title\"><span data-toggle=\"tooltip\" title=\"";
        // line 29
        echo ($context["help_title"] ?? null);
        echo "\">";
        echo ($context["entry_title"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_title\" value=\"";
        // line 31
        echo ($context["payment_prodamus_title"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_title"] ?? null);
        echo "\" id=\"entry-title\" class=\"form-control\"/>
              ";
        // line 32
        if ( !twig_test_empty(($context["error_title"] ?? null))) {
            echo " 
              <div class=\"text-danger\">";
            // line 33
            echo ($context["error_title"] ?? null);
            echo "</div>
              ";
        }
        // line 34
        echo " 
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-site_name\"><span data-toggle=\"tooltip\" title=\"";
        // line 38
        echo ($context["help_site_name"] ?? null);
        echo "\">";
        echo ($context["entry_site_name"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_site_name\" value=\"";
        // line 40
        echo ($context["payment_prodamus_site_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_site_name"] ?? null);
        echo "\" id=\"entry-site_name\" class=\"form-control\"/>
              ";
        // line 41
        if ( !twig_test_empty(($context["error_site_name"] ?? null))) {
            echo " 
              <div class=\"text-danger\">";
            // line 42
            echo ($context["error_site_name"] ?? null);
            echo "</div>
              ";
        }
        // line 43
        echo " 
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-secret_key\"><span data-toggle=\"tooltip\" title=\"";
        // line 47
        echo ($context["help_secret_key"] ?? null);
        echo "\">";
        echo ($context["entry_secret_key"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_secret_key\" value=\"";
        // line 49
        echo ($context["payment_prodamus_secret_key"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_secret_key"] ?? null);
        echo "\" id=\"entry-secret_key\" class=\"form-control\"/>
              ";
        // line 50
        if ( !twig_test_empty(($context["error_secret_key"] ?? null))) {
            echo " 
              <div class=\"text-danger\">";
            // line 51
            echo ($context["error_secret_key"] ?? null);
            echo "</div>
              ";
        }
        // line 52
        echo " 
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-total\"><span data-toggle=\"tooltip\" title=\"";
        // line 56
        echo ($context["help_total"] ?? null);
        echo "\">";
        echo ($context["entry_total"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_total\" value=\"";
        // line 58
        echo ($context["payment_prodamus_total"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_total"] ?? null);
        echo "\" id=\"input-total\" class=\"form-control\" />
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 62
        echo ($context["help_success_status"] ?? null);
        echo "\">";
        echo ($context["entry_success_status"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_success_status_id\" id=\"input-order-status\" class=\"form-control\">
                ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            echo " 
                ";
            // line 66
            if (((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["order_status"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["order_status_id"] ?? null) : null) == ($context["payment_prodamus_success_status_id"] ?? null))) {
                echo " 
                <option value=\"";
                // line 67
                echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["order_status"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["order_status_id"] ?? null) : null);
                echo "\" selected=\"selected\">";
                echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["order_status"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["name"] ?? null) : null);
                echo "</option>
                ";
            } else {
                // line 68
                echo " 
                <option value=\"";
                // line 69
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["order_status"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["order_status_id"] ?? null) : null);
                echo "\">";
                echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["order_status"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["name"] ?? null) : null);
                echo "</option>
                ";
            }
            // line 70
            echo " 
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo " 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status-confirm\">
              <span data-toggle=\"tooltip\" title=\"";
        // line 77
        echo ($context["help_confirm_status"] ?? null);
        echo "\">";
        echo ($context["entry_confirm_status"] ?? null);
        echo "</span>
            </label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_confirm_status_id\" id=\"input-order-status-confirm\" class=\"form-control\">
                ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses_confirm"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            echo " 
                  ";
            // line 82
            if (((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["order_status"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["order_status_id"] ?? null) : null) == ($context["payment_prodamus_confirm_status_id"] ?? null))) {
                echo " 
                    <option value=\"";
                // line 83
                echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["order_status"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["order_status_id"] ?? null) : null);
                echo "\" selected=\"selected\">";
                echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["order_status"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["name"] ?? null) : null);
                echo "</option>
                  ";
            } else {
                // line 84
                echo " 
                    <option value=\"";
                // line 85
                echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["order_status"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["order_status_id"] ?? null) : null);
                echo "\">";
                echo (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["order_status"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["name"] ?? null) : null);
                echo "</option>
                  ";
            }
            // line 86
            echo " 
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo " 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">";
        // line 92
        echo ($context["entry_geo_zone"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                <option value=\"0\">";
        // line 95
        echo ($context["text_all_zones"] ?? null);
        echo "</option>
                ";
        // line 96
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["geo_zones"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["geo_zone"]) {
            echo " 
                ";
            // line 97
            if (((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["geo_zone"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["geo_zone_id"] ?? null) : null) == ($context["payment_prodamus_geo_zone_id"] ?? null))) {
                echo " 
                <option value=\"";
                // line 98
                echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["geo_zone"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["geo_zone_id"] ?? null) : null);
                echo "\" selected=\"selected\">";
                echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["geo_zone"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["name"] ?? null) : null);
                echo "</option>
                ";
            } else {
                // line 99
                echo " 
                <option value=\"";
                // line 100
                echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["geo_zone"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["geo_zone_id"] ?? null) : null);
                echo "\">";
                echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["geo_zone"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["name"] ?? null) : null);
                echo "</option>
                ";
            }
            // line 101
            echo " 
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geo_zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo " 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 107
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 110
        if (($context["payment_prodamus_status"] ?? null)) {
            echo " 
                <option value=\"1\" selected=\"selected\">";
            // line 111
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 112
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 113
            echo " 
                <option value=\"1\">";
            // line 114
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 115
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 116
        echo " 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 121
        echo ($context["entry_sort_order"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_sort_order\" value=\"";
        // line 123
        echo ($context["payment_prodamus_sort_order"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_sort_order"] ?? null);
        echo "\" id=\"input-sort-order\" class=\"form-control\" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 131
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/payment/prodamus.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 131,  405 => 123,  400 => 121,  393 => 116,  388 => 115,  384 => 114,  381 => 113,  376 => 112,  372 => 111,  368 => 110,  362 => 107,  355 => 102,  348 => 101,  341 => 100,  338 => 99,  331 => 98,  327 => 97,  321 => 96,  317 => 95,  311 => 92,  304 => 87,  297 => 86,  290 => 85,  287 => 84,  280 => 83,  276 => 82,  270 => 81,  261 => 77,  253 => 71,  246 => 70,  239 => 69,  236 => 68,  229 => 67,  225 => 66,  219 => 65,  211 => 62,  202 => 58,  195 => 56,  189 => 52,  184 => 51,  180 => 50,  174 => 49,  167 => 47,  161 => 43,  156 => 42,  152 => 41,  146 => 40,  139 => 38,  133 => 34,  128 => 33,  124 => 32,  118 => 31,  111 => 29,  106 => 27,  100 => 24,  95 => 21,  88 => 18,  84 => 17,  77 => 12,  67 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}{{ column_left }} 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-prodamus\" data-toggle=\"tooltip\" title=\"{{ button_save }}\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"{{ cancel }}\" data-toggle=\"tooltip\" title=\"{{ button_cancel }}\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>{{ heading_title }}</h1>
      <ul class=\"breadcrumb\">
        {% for breadcrumb in breadcrumbs %} 
        <li><a href=\"{{ breadcrumb['href'] }}\">{{ breadcrumb['text'] }}</a></li>
        {% endfor %} 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    {% if (error_warning is not empty) %} 
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> {{ error_warning }} 
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    {% endif %} 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> {{ text_edit }}</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-prodamus\" class=\"form-horizontal\">
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-title\"><span data-toggle=\"tooltip\" title=\"{{ help_title }}\">{{ entry_title }}</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_title\" value=\"{{ payment_prodamus_title }}\" placeholder=\"{{ entry_title }}\" id=\"entry-title\" class=\"form-control\"/>
              {% if (error_title is not empty) %} 
              <div class=\"text-danger\">{{ error_title }}</div>
              {% endif %} 
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-site_name\"><span data-toggle=\"tooltip\" title=\"{{ help_site_name }}\">{{ entry_site_name }}</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_site_name\" value=\"{{ payment_prodamus_site_name }}\" placeholder=\"{{ entry_site_name }}\" id=\"entry-site_name\" class=\"form-control\"/>
              {% if (error_site_name is not empty) %} 
              <div class=\"text-danger\">{{ error_site_name }}</div>
              {% endif %} 
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"entry-secret_key\"><span data-toggle=\"tooltip\" title=\"{{ help_secret_key }}\">{{ entry_secret_key }}</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_secret_key\" value=\"{{ payment_prodamus_secret_key }}\" placeholder=\"{{ entry_secret_key }}\" id=\"entry-secret_key\" class=\"form-control\"/>
              {% if (error_secret_key is not empty) %} 
              <div class=\"text-danger\">{{ error_secret_key }}</div>
              {% endif %} 
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-total\"><span data-toggle=\"tooltip\" title=\"{{ help_total }}\">{{ entry_total }}</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_total\" value=\"{{ payment_prodamus_total }}\" placeholder=\"{{ entry_total }}\" id=\"input-total\" class=\"form-control\" />
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status\"><span data-toggle=\"tooltip\" title=\"{{ help_success_status }}\">{{ entry_success_status }}</span></label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_success_status_id\" id=\"input-order-status\" class=\"form-control\">
                {% for order_status in order_statuses %} 
                {% if (order_status['order_status_id'] == payment_prodamus_success_status_id) %} 
                <option value=\"{{ order_status['order_status_id'] }}\" selected=\"selected\">{{ order_status['name'] }}</option>
                {% else %} 
                <option value=\"{{ order_status['order_status_id'] }}\">{{ order_status['name'] }}</option>
                {% endif %} 
                {% endfor %} 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status-confirm\">
              <span data-toggle=\"tooltip\" title=\"{{ help_confirm_status }}\">{{ entry_confirm_status }}</span>
            </label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_confirm_status_id\" id=\"input-order-status-confirm\" class=\"form-control\">
                {% for order_status in order_statuses_confirm %} 
                  {% if (order_status['order_status_id'] ==payment_prodamus_confirm_status_id) %} 
                    <option value=\"{{ order_status['order_status_id'] }}\" selected=\"selected\">{{ order_status['name'] }}</option>
                  {% else %} 
                    <option value=\"{{ order_status['order_status_id'] }}\">{{ order_status['name'] }}</option>
                  {% endif %} 
                {% endfor %} 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">{{ entry_geo_zone }}</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                <option value=\"0\">{{ text_all_zones }}</option>
                {% for geo_zone in geo_zones %} 
                {% if (geo_zone['geo_zone_id'] == payment_prodamus_geo_zone_id) %} 
                <option value=\"{{ geo_zone['geo_zone_id'] }}\" selected=\"selected\">{{ geo_zone['name'] }}</option>
                {% else %} 
                <option value=\"{{ geo_zone['geo_zone_id'] }}\">{{ geo_zone['name'] }}</option>
                {% endif %} 
                {% endfor %} 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">{{ entry_status }}</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_prodamus_status\" id=\"input-status\" class=\"form-control\">
                {% if (payment_prodamus_status) %} 
                <option value=\"1\" selected=\"selected\">{{ text_enabled }}</option>
                <option value=\"0\">{{ text_disabled }}</option>
                {% else %} 
                <option value=\"1\">{{ text_enabled }}</option>
                <option value=\"0\" selected=\"selected\">{{ text_disabled }}</option>
                {% endif %} 
              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">{{ entry_sort_order }}</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_prodamus_sort_order\" value=\"{{ payment_prodamus_sort_order }}\" placeholder=\"{{ entry_sort_order }}\" id=\"input-sort-order\" class=\"form-control\" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
{{ footer }}", "extension/payment/prodamus.twig", "");
    }
}
