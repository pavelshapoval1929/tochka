<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/product/category.twig */
class __TwigTemplate_534f4684859b3d171d445deb4da855c4ee433c81e4766f76d9732d248020d1f1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-category\" class=\"container 1223\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">";
        // line 8
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 9
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 11
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 12
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 13
            echo "    ";
        } else {
            // line 14
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 15
            echo "    ";
        }
        // line 16
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
      <h2>";
        // line 17
        echo ($context["heading_title"] ?? null);
        echo "</h2>
      ";
        // line 18
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            // line 19
            echo "      <div class=\"row\"> ";
            if (($context["thumb"] ?? null)) {
                // line 20
                echo "        <div class=\"col-sm-2\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ((($context["custom_alt"] ?? null)) ? (($context["custom_alt"] ?? null)) : (($context["heading_title"] ?? null)));
                echo "\" title=\"";
                echo ((($context["custom_imgtitle"] ?? null)) ? (($context["custom_imgtitle"] ?? null)) : (($context["heading_title"] ?? null)));
                echo "\" class=\"img-thumbnail\" /></div>
        ";
            }
            // line 22
            echo "        ";
            if (($context["description"] ?? null)) {
                // line 23
                echo "        <div class=\"col-sm-10\">";
                echo ($context["description"] ?? null);
                echo "</div>
        ";
            }
            // line 24
            echo "</div>
      <hr>
      ";
        }
        // line 27
        echo "      ";
        if (($context["categories"] ?? null)) {
            // line 28
            echo "      <h3>";
            echo ($context["text_refine"] ?? null);
            echo "</h3>
      ";
            // line 29
            if ((twig_length_filter($this->env, ($context["categories"] ?? null)) <= 5)) {
                // line 30
                echo "      <div class=\"row\">
        <div class=\"col-sm-6\">
          <ul>
            ";
                // line 33
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 34
                    echo "            <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 34);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 34);
                    echo "</a></li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo "          </ul>
        </div>
      </div>
      ";
            } else {
                // line 40
                echo "      <div class=\"row columns\">";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["categories"] ?? null), twig_round((twig_length_filter($this->env, ($context["categories"] ?? null)) / 4), 1, "ceil")));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 41
                    echo "        <div class=\"col-sm-12\"><!--parent cat-->
          <ul style=\"margin: 0\">
            ";
                    // line 43
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["category"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 44
                        echo "            <li><a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 44);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 44);
                        echo "</a></li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 46
                    echo "          </ul>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "</div>
      <br />
      ";
            }
            // line 51
            echo "      ";
        }
        // line 52
        echo "      ";
        if (($context["products"] ?? null)) {
            // line 53
            echo "      <div class=\"row\">
        <div class=\"col-md-2 col-sm-6 hidden-xs\">
          <div class=\"btn-group btn-group-sm\">
            <button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 56
            echo ($context["button_list"] ?? null);
            echo "\"><i class=\"fa fa-th-list\"></i></button>
            <button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 57
            echo ($context["button_grid"] ?? null);
            echo "\"><i class=\"fa fa-th\"></i></button>
          </div>
        </div>
        <div class=\"col-md-3 col-sm-6\">
          <div class=\"form-group\"><a href=\"";
            // line 61
            echo ($context["compare"] ?? null);
            echo "\" id=\"compare-total\" class=\"btn btn-link\">";
            echo ($context["text_compare"] ?? null);
            echo "</a></div>
        </div>
        <div class=\"col-md-4 col-xs-6\">
          <div class=\"form-group input-group input-group-sm\">
            <label class=\"input-group-addon\" for=\"input-sort\">";
            // line 65
            echo ($context["text_sort"] ?? null);
            echo "</label>
            <select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 71
                echo "              ";
                if ((twig_get_attribute($this->env, $this->source, $context["sorts"], "value", [], "any", false, false, false, 71) == sprintf("%s-%s", ($context["sort"] ?? null), ($context["order"] ?? null)))) {
                    // line 72
                    echo "              
              
              
              <option value=\"";
                    // line 75
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "href", [], "any", false, false, false, 75);
                    echo "\" selected=\"selected\">";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "text", [], "any", false, false, false, 75);
                    echo "</option>
              
              
              
              ";
                } else {
                    // line 80
                    echo "              
              
              
              <option value=\"";
                    // line 83
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "href", [], "any", false, false, false, 83);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "text", [], "any", false, false, false, 83);
                    echo "</option>
              
              
              
              ";
                }
                // line 88
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "            
            
            
            </select>
          </div>
        </div>
        <div class=\"col-md-3 col-xs-6\">
          <div class=\"form-group input-group input-group-sm\">
            <label class=\"input-group-addon\" for=\"input-limit\">";
            // line 97
            echo ($context["text_limit"] ?? null);
            echo "</label>
            <select id=\"input-limit\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              ";
            // line 102
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 103
                echo "              ";
                if ((twig_get_attribute($this->env, $this->source, $context["limits"], "value", [], "any", false, false, false, 103) == ($context["limit"] ?? null))) {
                    // line 104
                    echo "              
              
              
              <option value=\"";
                    // line 107
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "href", [], "any", false, false, false, 107);
                    echo "\" selected=\"selected\">";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "text", [], "any", false, false, false, 107);
                    echo "</option>
              
              
              
              ";
                } else {
                    // line 112
                    echo "              
              
              
              <option value=\"";
                    // line 115
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "href", [], "any", false, false, false, 115);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "text", [], "any", false, false, false, 115);
                    echo "</option>
              
              
              
              ";
                }
                // line 120
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo "            
            
            
            </select>
          </div>
        </div>
      </div>

      <div class=\"row\"> ";
            // line 129
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 130
                echo "        <div class=\"product-layout product-list col-xs-12 111\">
          <div class=\"product-thumb 1234\">
             ";
                // line 135
                echo "            ";
                // line 136
                echo "
            <div class=\"inner-info\">
                <div class=\"mega-wrap\">
                ";
                // line 139
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "image", [], "any", false, false, false, 139)) > 2)) {
                    // line 140
                    echo "                    <div class=\"image\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 140);
                    echo "\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "image", [], "any", false, false, false, 140);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 140);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 140);
                    echo "\" class=\"img-responsive\" /></a></div>
                ";
                } else {
                    // line 142
                    echo "                    <div class=\"image\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 142);
                    echo "\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 142);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 142);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 142);
                    echo "\" class=\"img-responsive\" /></a></div>
                ";
                }
                // line 144
                echo "              <div class=\"caption\">
                <div class=\"caption-wrap\">
                    <h4><a href=\"";
                // line 146
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 146);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 146);
                echo "</a></h4>
                </div>
                  ";
                // line 151
                echo "                  <div class=\"special-block\">
                  ";
                // line 152
                if (twig_get_attribute($this->env, $this->source, $context["product"], "manufacturer", [], "any", false, false, false, 152)) {
                    // line 153
                    echo "                      <div class=\"upc-wrap\">
                          <span class=\"upc-name\">Производитель:</span>
                          <span class=\"upc-sep\"></span>
                          <span class=\"upc-value\">";
                    // line 156
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "manufacturer", [], "any", false, false, false, 156);
                    echo "</span>
                      </div>
                  ";
                }
                // line 159
                echo "                  ";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "upc", [], "any", false, false, false, 159)) {
                    // line 160
                    echo "                      <div class=\"upc-wrap\">
                          <span class=\"upc-name\">Артикул:</span>
                          <span class=\"upc-sep\"></span>
                          <span class=\"upc-value\">";
                    // line 163
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "upc", [], "any", false, false, false, 163);
                    echo "</span>
                      </div>
                  ";
                }
                // line 166
                echo "                  ";
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 166) > 0)) {
                    // line 167
                    echo "                      <span style=\"color: #28a745\" class=\"stock\">В наличии&nbsp;</span><span style=\"color: #28a745\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 167);
                    echo " ед.</span>
                  ";
                } else {
                    // line 169
                    echo "                      <span style=\"color:#ff8940;\" class=\"stock\">Временно нет на складе</span>
                  ";
                }
                // line 171
                echo "                  </div>
                  <div class=\"price-block-wrap\">
                    ";
                // line 173
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 173)) {
                    // line 174
                    echo "                    <p class=\"price\">Цена: ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 174)) {
                        // line 175
                        echo "                      ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 175);
                        echo "
                      ";
                    } else {
                        // line 176
                        echo " <span class=\"price-new\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 176);
                        echo "</span> <span class=\"price-old\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 176);
                        echo "</span> ";
                    }
                    // line 177
                    echo "                      ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 177)) {
                        echo " <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 177);
                        echo "</span> ";
                    }
                    echo " </p>
                    ";
                }
                // line 179
                echo "                      <div class=\"cart-quantity\">
                          <i onclick=\"\$(this).next().val(\$(this).next().val()-1);\$(this).parents('form').submit();\" class=\"fa fa-minus\"></i>
                          <input type=\"text\" name=\"quantity\" size=\"2\" value=\"";
                // line 181
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 181);
                echo "\" id=\"quantity_";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 181);
                echo "\"/>
                          <i onclick=\"\$(this).prev().val(~~\$(this).prev().val()+1);\$(this).parents('form').submit();\" class=\"fa fa-plus\"></i>
                          <input type=\"hidden\" name=\"product_id\" size=\"2\" value=\"";
                // line 183
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 183);
                echo "\" />
                      </div>
                  </div>
                ";
                // line 186
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 186)) {
                    // line 187
                    echo "                <div class=\"rating\"> ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 188
                        echo "                  ";
                        if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 188) < $context["i"])) {
                            echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                        } else {
                            echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>";
                        }
                        // line 189
                        echo "                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo " </div>
                ";
                }
                // line 190
                echo " </div>
                </div>

              <div class=\"button-group\">
                  ";
                // line 197
                echo "                ";
                // line 198
                echo "                <button type=\"button\" onclick=\"cart.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 198);
                echo "', \$('#quantity_";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 198);
                echo "').val());\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                echo ($context["button_cart"] ?? null);
                echo "</span></button>
                <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 199
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 199);
                echo "');\"><i class=\"fa fa-heart\"></i></button>
                <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 200
                echo ($context["button_compare"] ?? null);
                echo "\" onclick=\"compare.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 200);
                echo "');\"><i class=\"fa fa-exchange\"></i></button>
              </div>
            </div>
          </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo " </div>
      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">";
            // line 207
            echo ($context["pagination"] ?? null);
            echo "</div>
        <div class=\"col-sm-6 text-right\">";
            // line 208
            echo ($context["results"] ?? null);
            echo "</div>
      </div>
      ";
        }
        // line 211
        echo "      ";
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            // line 212
            echo "      <p>";
            echo ($context["text_empty"] ?? null);
            echo "</p>
      <div class=\"buttons\">
        <div class=\"pull-right\"><a href=\"";
            // line 214
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
      </div>
      ";
        }
        // line 217
        echo "      ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 218
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 220
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  598 => 220,  593 => 218,  588 => 217,  580 => 214,  574 => 212,  571 => 211,  565 => 208,  561 => 207,  557 => 205,  543 => 200,  537 => 199,  528 => 198,  526 => 197,  520 => 190,  511 => 189,  504 => 188,  499 => 187,  497 => 186,  491 => 183,  484 => 181,  480 => 179,  468 => 177,  461 => 176,  455 => 175,  452 => 174,  450 => 173,  446 => 171,  442 => 169,  436 => 167,  433 => 166,  427 => 163,  422 => 160,  419 => 159,  413 => 156,  408 => 153,  406 => 152,  403 => 151,  396 => 146,  392 => 144,  380 => 142,  368 => 140,  366 => 139,  361 => 136,  359 => 135,  355 => 130,  351 => 129,  341 => 121,  335 => 120,  325 => 115,  320 => 112,  310 => 107,  305 => 104,  302 => 103,  298 => 102,  290 => 97,  280 => 89,  274 => 88,  264 => 83,  259 => 80,  249 => 75,  244 => 72,  241 => 71,  237 => 70,  229 => 65,  220 => 61,  213 => 57,  209 => 56,  204 => 53,  201 => 52,  198 => 51,  193 => 48,  185 => 46,  174 => 44,  170 => 43,  166 => 41,  161 => 40,  155 => 36,  144 => 34,  140 => 33,  135 => 30,  133 => 29,  128 => 28,  125 => 27,  120 => 24,  114 => 23,  111 => 22,  101 => 20,  98 => 19,  96 => 18,  92 => 17,  85 => 16,  82 => 15,  79 => 14,  76 => 13,  73 => 12,  70 => 11,  67 => 10,  65 => 9,  61 => 8,  58 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}
<div id=\"product-category\" class=\"container 1223\">
  <ul class=\"breadcrumb\">
    {% for breadcrumb in breadcrumbs %}
    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
    {% endfor %}
  </ul>
  <div class=\"row\">{{ column_left }}
    {% if column_left and column_right %}
    {% set class = 'col-sm-6' %}
    {% elseif column_left or column_right %}
    {% set class = 'col-sm-9' %}
    {% else %}
    {% set class = 'col-sm-12' %}
    {% endif %}
    <div id=\"content\" class=\"{{ class }}\">{{ content_top }}
      <h2>{{ heading_title }}</h2>
      {% if thumb or description %}
      <div class=\"row\"> {% if thumb %}
        <div class=\"col-sm-2\"><img src=\"{{ thumb }}\" alt=\"{{ custom_alt ? custom_alt : heading_title }}\" title=\"{{ custom_imgtitle ? custom_imgtitle : heading_title }}\" class=\"img-thumbnail\" /></div>
        {% endif %}
        {% if description %}
        <div class=\"col-sm-10\">{{ description }}</div>
        {% endif %}</div>
      <hr>
      {% endif %}
      {% if categories %}
      <h3>{{ text_refine }}</h3>
      {% if categories|length <= 5 %}
      <div class=\"row\">
        <div class=\"col-sm-6\">
          <ul>
            {% for category in categories %}
            <li><a href=\"{{ category.href }}\">{{ category.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
      </div>
      {% else %}
      <div class=\"row columns\">{% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}
        <div class=\"col-sm-12\"><!--parent cat-->
          <ul style=\"margin: 0\">
            {% for child in category %}
            <li><a href=\"{{ child.href }}\">{{ child.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
        {% endfor %}</div>
      <br />
      {% endif %}
      {% endif %}
      {% if products %}
      <div class=\"row\">
        <div class=\"col-md-2 col-sm-6 hidden-xs\">
          <div class=\"btn-group btn-group-sm\">
            <button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"{{ button_list }}\"><i class=\"fa fa-th-list\"></i></button>
            <button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"{{ button_grid }}\"><i class=\"fa fa-th\"></i></button>
          </div>
        </div>
        <div class=\"col-md-3 col-sm-6\">
          <div class=\"form-group\"><a href=\"{{ compare }}\" id=\"compare-total\" class=\"btn btn-link\">{{ text_compare }}</a></div>
        </div>
        <div class=\"col-md-4 col-xs-6\">
          <div class=\"form-group input-group input-group-sm\">
            <label class=\"input-group-addon\" for=\"input-sort\">{{ text_sort }}</label>
            <select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              {% for sorts in sorts %}
              {% if sorts.value == '%s-%s'|format(sort, order) %}
              
              
              
              <option value=\"{{ sorts.href }}\" selected=\"selected\">{{ sorts.text }}</option>
              
              
              
              {% else %}
              
              
              
              <option value=\"{{ sorts.href }}\">{{ sorts.text }}</option>
              
              
              
              {% endif %}
              {% endfor %}
            
            
            
            </select>
          </div>
        </div>
        <div class=\"col-md-3 col-xs-6\">
          <div class=\"form-group input-group input-group-sm\">
            <label class=\"input-group-addon\" for=\"input-limit\">{{ text_limit }}</label>
            <select id=\"input-limit\" class=\"form-control\" onchange=\"location = this.value;\">
              
              
              
              {% for limits in limits %}
              {% if limits.value == limit %}
              
              
              
              <option value=\"{{ limits.href }}\" selected=\"selected\">{{ limits.text }}</option>
              
              
              
              {% else %}
              
              
              
              <option value=\"{{ limits.href }}\">{{ limits.text }}</option>
              
              
              
              {% endif %}
              {% endfor %}
            
            
            
            </select>
          </div>
        </div>
      </div>

      <div class=\"row\"> {% for product in products %}
        <div class=\"product-layout product-list col-xs-12 111\">
          <div class=\"product-thumb 1234\">
             {#<pre>
            {{ dump(product.quantity) }}
              </pre>#}
            {#<div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>#}

            <div class=\"inner-info\">
                <div class=\"mega-wrap\">
                {% if product.image|length > 2 %}
                    <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.image }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
                {% else %}
                    <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
                {% endif %}
              <div class=\"caption\">
                <div class=\"caption-wrap\">
                    <h4><a href=\"{{ product.href }}\">{{ product.name }}</a></h4>
                </div>
                  {#{% if product.description != '..' %}
                <p>{{ product.description }}</p>
                  {% endif %}#}
                  <div class=\"special-block\">
                  {% if product.manufacturer %}
                      <div class=\"upc-wrap\">
                          <span class=\"upc-name\">Производитель:</span>
                          <span class=\"upc-sep\"></span>
                          <span class=\"upc-value\">{{ product.manufacturer }}</span>
                      </div>
                  {% endif %}
                  {% if product.upc %}
                      <div class=\"upc-wrap\">
                          <span class=\"upc-name\">Артикул:</span>
                          <span class=\"upc-sep\"></span>
                          <span class=\"upc-value\">{{ product.upc }}</span>
                      </div>
                  {% endif %}
                  {% if product.quantity > 0 %}
                      <span style=\"color: #28a745\" class=\"stock\">В наличии&nbsp;</span><span style=\"color: #28a745\">{{ product.quantity }} ед.</span>
                  {% else %}
                      <span style=\"color:#ff8940;\" class=\"stock\">Временно нет на складе</span>
                  {% endif %}
                  </div>
                  <div class=\"price-block-wrap\">
                    {% if product.price %}
                    <p class=\"price\">Цена: {% if not product.special %}
                      {{ product.price }}
                      {% else %} <span class=\"price-new\">{{ product.special }}</span> <span class=\"price-old\">{{ product.price }}</span> {% endif %}
                      {% if product.tax %} <span class=\"price-tax\">{{ text_tax }} {{ product.tax }}</span> {% endif %} </p>
                    {% endif %}
                      <div class=\"cart-quantity\">
                          <i onclick=\"\$(this).next().val(\$(this).next().val()-1);\$(this).parents('form').submit();\" class=\"fa fa-minus\"></i>
                          <input type=\"text\" name=\"quantity\" size=\"2\" value=\"{{ product.minimum }}\" id=\"quantity_{{ product.product_id }}\"/>
                          <i onclick=\"\$(this).prev().val(~~\$(this).prev().val()+1);\$(this).parents('form').submit();\" class=\"fa fa-plus\"></i>
                          <input type=\"hidden\" name=\"product_id\" size=\"2\" value=\"{{ product.product_id }}\" />
                      </div>
                  </div>
                {% if product.rating %}
                <div class=\"rating\"> {% for i in 1..5 %}
                  {% if product.rating < i %} <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> {% else %} <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>{% endif %}
                  {% endfor %} </div>
                {% endif %} </div>
                </div>

              <div class=\"button-group\">
                  {#<pre>
                  {{ dump(product) }}
                  </pre>#}
                {#<button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', '{{ product.minimum }}');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>#}
                <button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', \$('#quantity_{{ product.product_id }}').val());\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>
                <button type=\"button\" data-toggle=\"tooltip\" title=\"{{ button_wishlist }}\" onclick=\"wishlist.add('{{ product.product_id }}');\"><i class=\"fa fa-heart\"></i></button>
                <button type=\"button\" data-toggle=\"tooltip\" title=\"{{ button_compare }}\" onclick=\"compare.add('{{ product.product_id }}');\"><i class=\"fa fa-exchange\"></i></button>
              </div>
            </div>
          </div>
        </div>
        {% endfor %} </div>
      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">{{ pagination }}</div>
        <div class=\"col-sm-6 text-right\">{{ results }}</div>
      </div>
      {% endif %}
      {% if not categories and not products %}
      <p>{{ text_empty }}</p>
      <div class=\"buttons\">
        <div class=\"pull-right\"><a href=\"{{ continue }}\" class=\"btn btn-primary\">{{ button_continue }}</a></div>
      </div>
      {% endif %}
      {{ content_bottom }}</div>
    {{ column_right }}</div>
</div>
{{ footer }} 
", "default/template/product/category.twig", "");
    }
}
