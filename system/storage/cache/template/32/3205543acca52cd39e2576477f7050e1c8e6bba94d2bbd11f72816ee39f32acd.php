<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/update_prices.twig */
class __TwigTemplate_b4d1f278ae0f1a8dd0bd76f42fbfa86c655963933ae1adfec0fbf6ddbbaf78e1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
123
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 8
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            // line 19
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 23
        echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>";
        // line 25
        echo ($context["text_edit"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"";
        // line 29
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\" style=\"display: none\">";
        // line 33
        echo ($context["text_import"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\" style=\"display: none\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"";
        // line 36
        echo ($context["xml_import_link"] ?? null);
        echo "\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"";
        // line 37
        echo ($context["text_tips_import"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>

                    <div class=\"form-group\">

                        ";
        // line 51
        echo "
                    </div>

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 56
        echo ($context["entry_status"] ?? null);
        echo "</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                ";
        // line 60
        if (($context["module_xml_module_status"] ?? null)) {
            // line 61
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\">";
            // line 62
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        } else {
            // line 64
            echo "                                    <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 65
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        }
        // line 67
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start\"
                      style=\"border: 1px solid red;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test prodat
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-pricat\"
                      style=\"border: 1px solid blue;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test pricat
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-manufacturer\"
                      style=\"border: 1px solid #13ff1d;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test manufacturer
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-manufacturer-id\"
                      style=\"border: 1px solid #fdff45;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test manufacturer id
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-update-time\"
                      style=\"border: 1px solid #8612cc;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">start update time
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"update-properties\"
                      style=\"border: 1px solid #cc7c1a;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">update properties
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"update-descriptions\"
                      style=\"border: 1px solid #cc400c;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">update descriptions
                </span>
                    </div>
                    <hr>
                    <form class=\"set_filters\">
                        <label for=\"product_name\">Установить фильтра для товаров (по названию товара)</label>
                        <input type=\"text\" class=\"product_name\" name=\"product_name\" placeholder=\"Введите название фильтра\" style=\"width: 250px; height: 30px; display: block\">
                        <input type=\"button\" class=\"set_filters_submit\" value=\"отправить\" style=\"height: 30px; display: block; margin-top: 10px;\">
                    </form>

                    <hr>
                    <form class=\"set_attr\">
                        <label for=\"product_name\">Установить атрибуты для товаров (по названию товара)</label>
                        <input type=\"text\" class=\"attr_name\" name=\"attr_name\" placeholder=\"Введите название атрибута (Серия, Цвет и т.п.)\" style=\"width: 350px; height: 30px; display: block; margin-bottom: 10px\">
                        <input type=\"text\" class=\"product_name\" name=\"product_name\" placeholder=\"Введите название фильтра (Мира, Valena и т.п.)\" style=\"width: 350px; height: 30px; display: block\">
                        <input type=\"button\" class=\"set_attr_submit\" value=\"отправить\" style=\"height: 30px; display: block; margin-top: 10px;\">
                    </form>
                    <div id=\"loader\"
                         style=\"display: none;
                 position: fixed;
                 top: 50%;
                 left: 50%;
                 background: url(/admin/view/image/loading_csv.gif);
                 width: 64px;
                 height: 64px;\">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token=";
        // line 173
        echo ($context["user_token"] ?? null);
        echo "',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

<script type=\"text/javascript\">
    \$(document).on('click', '.start', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_prodat&user_token=";
        // line 217
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-pricat', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_pricat&user_token=";
        // line 245
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer&user_token=";
        // line 273
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer-id', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer_id&user_token=";
        // line 301
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });


    \$(document).on('click', '.start-update-time', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_update_time&user_token=";
        // line 330
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.update-properties', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_properties&user_token=";
        // line 361
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$(document).on('click', '.update-descriptions', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_descriptions&user_token=";
        // line 402
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
    \$('.set_filters_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_filters&user_token=";
        // line 438
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$('.set_attr_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_attributes&user_token=";
        // line 475
        echo ($context["user_token"] ?? null);
        echo "',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
</script>

";
        // line 508
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/update_prices.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  641 => 508,  605 => 475,  565 => 438,  526 => 402,  482 => 361,  448 => 330,  416 => 301,  385 => 273,  354 => 245,  323 => 217,  276 => 173,  168 => 67,  163 => 65,  158 => 64,  153 => 62,  148 => 61,  146 => 60,  139 => 56,  132 => 51,  121 => 37,  117 => 36,  111 => 33,  104 => 29,  97 => 25,  93 => 23,  85 => 19,  83 => 18,  77 => 14,  66 => 12,  62 => 11,  57 => 9,  51 => 8,  47 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}{{ column_left }}
123
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"{{ button_save }}\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"{{ cancel }}\" data-toggle=\"tooltip\" title=\"{{ button_cancel }}\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>{{ heading_title }}</h1>
            <ul class=\"breadcrumb\">
                {% for breadcrumb in breadcrumbs %}
                    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
                {% endfor %}
            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        {% if error_warning %}
            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> {{ error_warning }}
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        {% endif %}
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>{{ text_edit }}</h3>
            </div>
            <div class=\"panel-body\">

                <form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-data-feed\" style=\"display: none\">{{ text_import }}</label>
                        <div class=\"col-sm-10\" style=\"display: none\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"xml_link\" value=\"{{ xml_import_link }}\" placeholder=\"\" id=\"input-xml-link\" class=\"form-control\" />
                                <span class=\"input-group-btn\"><button type=\"button\" id=\"btn-import-xml\"  title=\"{{ text_tips_import }}\" class=\"btn btn-primary\"><i class=\"fa fa-download\"></i></button></span>
                            </div>
                        </div>

                    </div>

                    <div class=\"form-group\">

                        {# <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">{{ text_export }}</label>
                        <div class=\"col-sm-10\">
                          <div class=\"input-group\" style=\"padding-top: 10px;\">
                            <a href=\"{{ export_xml }}\" target=\"_blank\">{{ export_xml }}</a>
                          </div>
                        </div> #}

                    </div>

                    <div class=\"form-group\">

                        <label class=\"col-sm-2 control-label\" for=\"input-status\">{{ entry_status }}</label>

                        <div class=\"col-sm-10\">
                            <select name=\"module_xml_module_status\" id=\"input-status\" class=\"form-control\">
                                {% if module_xml_module_status %}
                                    <option value=\"1\" selected=\"selected\">{{ text_enabled }}</option>
                                    <option value=\"0\">{{ text_disabled }}</option>
                                {% else %}
                                    <option value=\"1\">{{ text_enabled }}</option>
                                    <option value=\"0\" selected=\"selected\">{{ text_disabled }}</option>
                                {% endif %}
                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start\"
                      style=\"border: 1px solid red;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test prodat
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-pricat\"
                      style=\"border: 1px solid blue;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test pricat
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-manufacturer\"
                      style=\"border: 1px solid #13ff1d;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test manufacturer
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-manufacturer-id\"
                      style=\"border: 1px solid #fdff45;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">test manufacturer id
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"start-update-time\"
                      style=\"border: 1px solid #8612cc;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">start update time
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"update-properties\"
                      style=\"border: 1px solid #cc7c1a;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">update properties
                </span>
                    </div>
                    <div class=\"form-group\">
                <span class=\"update-descriptions\"
                      style=\"border: 1px solid #cc400c;
                        padding: 20px;
                        display: inline-block;
                        cursor: pointer;
                        margin-left: 20px\">update descriptions
                </span>
                    </div>
                    <hr>
                    <form class=\"set_filters\">
                        <label for=\"product_name\">Установить фильтра для товаров (по названию товара)</label>
                        <input type=\"text\" class=\"product_name\" name=\"product_name\" placeholder=\"Введите название фильтра\" style=\"width: 250px; height: 30px; display: block\">
                        <input type=\"button\" class=\"set_filters_submit\" value=\"отправить\" style=\"height: 30px; display: block; margin-top: 10px;\">
                    </form>

                    <hr>
                    <form class=\"set_attr\">
                        <label for=\"product_name\">Установить атрибуты для товаров (по названию товара)</label>
                        <input type=\"text\" class=\"attr_name\" name=\"attr_name\" placeholder=\"Введите название атрибута (Серия, Цвет и т.п.)\" style=\"width: 350px; height: 30px; display: block; margin-bottom: 10px\">
                        <input type=\"text\" class=\"product_name\" name=\"product_name\" placeholder=\"Введите название фильтра (Мира, Valena и т.п.)\" style=\"width: 350px; height: 30px; display: block\">
                        <input type=\"button\" class=\"set_attr_submit\" value=\"отправить\" style=\"height: 30px; display: block; margin-top: 10px;\">
                    </form>
                    <div id=\"loader\"
                         style=\"display: none;
                 position: fixed;
                 top: 50%;
                 left: 50%;
                 background: url(/admin/view/image/loading_csv.gif);
                 width: 64px;
                 height: 64px;\">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

    \$('#btn-import-xml').on('click', function() {
        var url = '';
        \$(\"input[name='xml_link']\").each(function() {
            url = this.value;
        });
        console.log(url);

        if(url.length > 10) {
            \$.ajax({
                url: 'index.php?route=extension/module/xml_module/import_xml&user_token={{ user_token }}',
                type: 'post',
                data: 'xml_url=' + encodeURIComponent(url),
                dataType: 'json',
                beforeSend: function() {
                    \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + 'Loading...' + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                },
                complete: function() {
                    // \$('.alert-dismissible').remove();
                },
                success: function(json) {
                    \$('.alert-dismissible').remove();

                    // Check for errors
                    if (json['error']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }

                    if (json['success']) {
                        \$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
            });
        }else {
            alert(\"something unlike url!\");
        }


    });



</script>

<script type=\"text/javascript\">
    \$(document).on('click', '.start', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_prodat&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-pricat', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_pricat&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.start-manufacturer-id', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_manufacturer_id&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });


    \$(document).on('click', '.start-update-time', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;

        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/import_update_time&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
            })
            .always(function() {
                console.log(\"complete22\");
            });
    });

    \$(document).on('click', '.update-properties', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_properties&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$(document).on('click', '.update-descriptions', function (e) {
        e.preventDefault();
        \$load_prodat = true;
        \$load_pricat = true;
        //var start = 50;
        //var loop = 100;


        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_descriptions&user_token={{ user_token }}',
            type: 'POST',
            dataType: 'text',
            data: \$load_prodat,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
    \$('.set_filters_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_filters&user_token={{ user_token }}',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });

    \$('.set_attr_submit').on('click', function (e) {
        e.preventDefault();
        var form_data = \$(this).parent('form').serialize();
        //console.log(form_data);
        \$.ajax({
            url: 'index.php?route=extension/module/xml_module/update_attributes&user_token={{ user_token }}',
            type: 'POST',
            //dataType: 'text',
            data: form_data,
            //data: {start: start, loop: loop, \$load_prodat},
            //data: {cart_item_key: cart_item_key, data_product_id: data_product_id}
            beforeSend: function() {
                \$('#loader').show();
            },
            complete: function() {
                \$('#loader').hide();
            }

        })
            .done(function(response) {
                /*console.log(\"success\");*/
                console.log(response);
                //count.text(response);
            })
            .success(function(){
                alert('Данные успешно отправлены.');
            })
            .fail(function() {
                console.log(\"error11\");
                alert('ошибка.');
            })
            .always(function() {
                //console.log(\"complete22\");
                alert('Данные отправляются.');
            });
    });
</script>

{{ footer }}
", "extension/module/update_prices.twig", "");
    }
}
