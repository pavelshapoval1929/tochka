<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/brainyfilter.twig */
class __TwigTemplate_f4a5d5e6d5e377ff0cc7b774ba825640dabc913dcf7bff3c61ffd84d2bbb041a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        $context["isHorizontal"] = ((($context["layout_position"] ?? null) === "content_top") || (($context["layout_position"] ?? null) === "content_bottom"));
        // line 8
        $context["isResponsive"] = (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["settings"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["style"] ?? null) : null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["responsive"] ?? null) : null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["enabled"] ?? null) : null)) ? (true) : (false));
        // line 9
        $context["responsivePos"] = ((((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["settings"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["style"] ?? null) : null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["responsive"] ?? null) : null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["position"] ?? null) : null) === "right")) ? ("bf-right") : ("bf-left"));
        // line 10
        echo "
<style type=\"text/css\">
    .bf-responsive.bf-active.bf-layout-id-";
        // line 12
        echo ($context["layout_id"] ?? null);
        echo " .bf-check-position {
        top: ";
        // line 13
        echo twig_number_format_filter($this->env, (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["settings"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["style"] ?? null) : null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["responsive"] ?? null) : null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["offset"] ?? null) : null));
        echo "px;
    }
    .bf-responsive.bf-active.bf-layout-id-";
        // line 15
        echo ($context["layout_id"] ?? null);
        echo " .bf-btn-show, 
    .bf-responsive.bf-active.bf-layout-id-";
        // line 16
        echo ($context["layout_id"] ?? null);
        echo " .bf-btn-reset {
        top: ";
        // line 17
        echo twig_number_format_filter($this->env, (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["settings"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["style"] ?? null) : null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["responsive"] ?? null) : null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["offset"] ?? null) : null));
        echo "px;
    }
    .bf-layout-id-";
        // line 19
        echo ($context["layout_id"] ?? null);
        echo " .bf-btn-show {
        ";
        // line 20
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 20), "resp_show_btn_color", [], "array", false, true, false, 20), "val", [], "array", true, true, false, 20)) {
            // line 21
            echo "            background: ";
            echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["settings"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["style"] ?? null) : null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["resp_show_btn_color"] ?? null) : null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["val"] ?? null) : null);
            echo ";
        ";
        }
        // line 23
        echo "    }
    .bf-layout-id-";
        // line 24
        echo ($context["layout_id"] ?? null);
        echo " .bf-btn-reset {
        ";
        // line 25
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 25), "resp_reset_btn_color", [], "array", false, true, false, 25), "val", [], "array", true, true, false, 25)) {
            // line 26
            echo "            background: ";
            echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["settings"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["style"] ?? null) : null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["resp_reset_btn_color"] ?? null) : null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["val"] ?? null) : null);
            echo ";
        ";
        }
        // line 28
        echo "    }
    .bf-layout-id-";
        // line 29
        echo ($context["layout_id"] ?? null);
        echo " .bf-attr-header{
        ";
        // line 30
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 30), "block_header_background", [], "array", false, true, false, 30), "val", [], "array", true, true, false, 30)) ? ((("background: " . (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["settings"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["style"] ?? null) : null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["block_header_background"] ?? null) : null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["val"] ?? null) : null)) . ";")) : (""));
        echo "
        ";
        // line 31
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 31), "block_header_text", [], "array", false, true, false, 31), "val", [], "array", true, true, false, 31)) ? ((("color: " . (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["settings"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["style"] ?? null) : null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["block_header_text"] ?? null) : null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["val"] ?? null) : null)) . ";")) : (""));
        echo " 
    }
    .bf-layout-id-";
        // line 33
        echo ($context["layout_id"] ?? null);
        echo " .bf-count{
        ";
        // line 34
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 34), "product_quantity_background", [], "array", false, true, false, 34), "val", [], "array", true, true, false, 34)) ? ((("background: " . (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["settings"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["style"] ?? null) : null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["product_quantity_background"] ?? null) : null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["val"] ?? null) : null)) . ";")) : (""));
        echo " 
        ";
        // line 35
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 35), "product_quantity_text", [], "array", false, true, false, 35), "val", [], "array", true, true, false, 35)) ? ((("color: " . (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["settings"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["style"] ?? null) : null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["product_quantity_text"] ?? null) : null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["val"] ?? null) : null)) . ";")) : (""));
        echo " 
    }
   .bf-layout-id-";
        // line 37
        echo ($context["layout_id"] ?? null);
        echo " .ui-widget-header {
        ";
        // line 38
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 38), "price_slider_area_background", [], "array", false, true, false, 38), "val", [], "array", true, true, false, 38)) ? ((("background: " . (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = ($context["settings"] ?? null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["style"] ?? null) : null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["price_slider_area_background"] ?? null) : null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["val"] ?? null) : null)) . ";")) : (""));
        echo " 
    }
   .bf-layout-id-";
        // line 40
        echo ($context["layout_id"] ?? null);
        echo " .ui-widget-content {
        ";
        // line 41
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 41), "price_slider_background", [], "array", false, true, false, 41), "val", [], "array", true, true, false, 41)) ? ((("background: " . (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["settings"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["style"] ?? null) : null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["price_slider_background"] ?? null) : null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["val"] ?? null) : null)) . ";")) : (""));
        echo " 
        ";
        // line 42
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 42), "price_slider_border", [], "array", false, true, false, 42), "val", [], "array", true, true, false, 42)) ? ((("border:1px solid" . (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["settings"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["style"] ?? null) : null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["price_slider_border"] ?? null) : null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["val"] ?? null) : null)) . ";")) : (""));
        echo " 
    }
    .bf-layout-id-";
        // line 44
        echo ($context["layout_id"] ?? null);
        echo " .ui-state-default {
        ";
        // line 45
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 45), "price_slider_handle_background", [], "array", false, true, false, 45), "val", [], "array", true, true, false, 45)) ? ((("background:" . (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = ($context["settings"] ?? null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["style"] ?? null) : null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["price_slider_handle_background"] ?? null) : null)) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["val"] ?? null) : null)) . ";")) : (""));
        echo " 
        ";
        // line 46
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 46), "price_slider_handle_border", [], "array", false, true, false, 46), "val", [], "array", true, true, false, 46)) ? ((("border:1px solid" . (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["settings"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["style"] ?? null) : null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["price_slider_handle_border"] ?? null) : null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["val"] ?? null) : null)) . ";")) : (""));
        echo " 
   }
    .bf-layout-id-";
        // line 48
        echo ($context["layout_id"] ?? null);
        echo " .bf-attr-group-header{
        ";
        // line 49
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 49), "group_block_header_background", [], "array", false, true, false, 49), "val", [], "array", true, true, false, 49)) ? ((("background:" . (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = ($context["settings"] ?? null)) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["style"] ?? null) : null)) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["group_block_header_background"] ?? null) : null)) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["val"] ?? null) : null)) . ";")) : (""));
        echo " 
        ";
        // line 50
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "style", [], "array", false, true, false, 50), "group_block_header_text", [], "array", false, true, false, 50), "val", [], "array", true, true, false, 50)) ? ((("color:" . (($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = (($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = (($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = ($context["settings"] ?? null)) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["style"] ?? null) : null)) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["group_block_header_text"] ?? null) : null)) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["val"] ?? null) : null)) . ";")) : (""));
        echo " 
    }
    ";
        // line 52
        if ((($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = (($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = ($context["settings"] ?? null)) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["behaviour"] ?? null) : null)) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["hide_empty"] ?? null) : null)) {
            echo ">
        .bf-layout-id-";
            // line 53
            echo ($context["layout_id"] ?? null);
            echo " .bf-row.bf-disabled, 
        .bf-layout-id-";
            // line 54
            echo ($context["layout_id"] ?? null);
            echo " .bf-horizontal .bf-row.bf-disabled {
            display: none;
        }
    ";
        }
        // line 58
        echo "</style>
";
        // line 59
        if (twig_length_filter($this->env, ($context["filters"] ?? null))) {
            // line 60
            echo "<div class=\"bf-panel-wrapper";
            if (($context["isResponsive"] ?? null)) {
                echo " bf-responsive ";
            }
            echo " ";
            echo ($context["responsivePos"] ?? null);
            echo " bf-layout-id-";
            echo ($context["layout_id"] ?? null);
            echo "\">
    <div class=\"bf-btn-show\"></div>
    <a class=\"bf-btn-reset\" onclick=\"BrainyFilter.reset();\"></a>
    <div class=\"box bf-check-position ";
            // line 63
            if (($context["isHorizontal"] ?? null)) {
                echo " bf-horizontal ";
            }
            echo "\">
        <div class=\"box-heading\">";
            // line 64
            echo ($context["lang_block_title"] ?? null);
            echo " ";
            if (($context["isHorizontal"] ?? null)) {
                echo " <a class=\"bf-toggle-filter-arrow\"></a><input type=\"reset\" class=\"bf-buttonclear\" onclick=\"BrainyFilter.reset();\" value=\"";
                echo ($context["reset"] ?? null);
                echo "\" /> ";
            }
            echo "</div>
        <div class=\"brainyfilter-panel box-content ";
            // line 65
            if ((($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = (($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a = ($context["settings"] ?? null)) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["submission"] ?? null) : null)) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["hide_panel"] ?? null) : null)) {
                echo " bf-hide-panel ";
            }
            echo "\">
            <form class=\"bf-form 
                    ";
            // line 67
            if ((($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = (($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = ($context["settings"] ?? null)) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["behaviour"] ?? null) : null)) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["product_count"] ?? null) : null)) {
                echo " bf-with-counts ";
            }
            echo " 
                    ";
            // line 68
            if (($context["sliding"] ?? null)) {
                echo " bf-with-sliding ";
            }
            // line 69
            echo "                    ";
            if ((((($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec = (($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 = ($context["settings"] ?? null)) && is_array($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574) || $__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 instanceof ArrayAccess ? ($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574["submission"] ?? null) : null)) && is_array($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec) || $__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec instanceof ArrayAccess ? ($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec["submit_type"] ?? null) : null) === "button") && ((($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c = (($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 = ($context["settings"] ?? null)) && is_array($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0) || $__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 instanceof ArrayAccess ? ($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0["submission"] ?? null) : null)) && is_array($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c) || $__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c instanceof ArrayAccess ? ($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c["submit_button_type"] ?? null) : null) === "float"))) {
                echo " bf-with-float-btn ";
            }
            // line 70
            echo "                    ";
            if (($context["limit_height"] ?? null)) {
                echo " bf-with-height-limit ";
            }
            echo "\"
                    data-height-limit=\"";
            // line 71
            echo ($context["limit_height_opts"] ?? null);
            echo "\"
                    data-visible-items=\"";
            // line 72
            echo ($context["slidingOpts"] ?? null);
            echo "\"
                    data-hide-items=\"";
            // line 73
            echo ($context["slidingMin"] ?? null);
            echo "\"
                    data-submit-type=\"";
            // line 74
            echo (($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc = (($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd = ($context["settings"] ?? null)) && is_array($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd) || $__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd instanceof ArrayAccess ? ($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd["submission"] ?? null) : null)) && is_array($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc) || $__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc instanceof ArrayAccess ? ($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc["submit_type"] ?? null) : null);
            echo "\"
                    data-submit-delay=\"";
            // line 75
            echo twig_number_format_filter($this->env, (($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 = (($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 = ($context["settings"] ?? null)) && is_array($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007) || $__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 instanceof ArrayAccess ? ($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007["submission"] ?? null) : null)) && is_array($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81) || $__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 instanceof ArrayAccess ? ($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81["submit_delay_time"] ?? null) : null));
            echo "\"
                    data-submit-hide-panel =\"";
            // line 76
            echo twig_number_format_filter($this->env, (($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d = (($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba = ($context["settings"] ?? null)) && is_array($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba) || $__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba instanceof ArrayAccess ? ($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba["submission"] ?? null) : null)) && is_array($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d) || $__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d instanceof ArrayAccess ? ($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d["hide_panel"] ?? null) : null));
            echo "\"
                    data-resp-max-width=\"";
            // line 77
            echo twig_number_format_filter($this->env, (($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 = (($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 = (($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf = ($context["settings"] ?? null)) && is_array($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf) || $__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf instanceof ArrayAccess ? ($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf["style"] ?? null) : null)) && is_array($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639) || $__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 instanceof ArrayAccess ? ($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639["responsive"] ?? null) : null)) && is_array($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49) || $__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 instanceof ArrayAccess ? ($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49["max_width"] ?? null) : null));
            echo "\"
                    data-resp-collapse=\"";
            // line 78
            echo twig_number_format_filter($this->env, (($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 = (($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a = (($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 = ($context["settings"] ?? null)) && is_array($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4) || $__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 instanceof ArrayAccess ? ($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4["style"] ?? null) : null)) && is_array($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a) || $__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a instanceof ArrayAccess ? ($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a["responsive"] ?? null) : null)) && is_array($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921) || $__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 instanceof ArrayAccess ? ($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921["collapsed"] ?? null) : null));
            echo "\"
                    data-resp-max-scr-width =\"";
            // line 79
            echo twig_number_format_filter($this->env, (($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 = (($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 = (($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a = ($context["settings"] ?? null)) && is_array($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a) || $__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a instanceof ArrayAccess ? ($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a["style"] ?? null) : null)) && is_array($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51) || $__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 instanceof ArrayAccess ? ($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51["responsive"] ?? null) : null)) && is_array($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985) || $__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 instanceof ArrayAccess ? ($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985["max_screen_width"] ?? null) : null));
            echo "\"
                    method=\"get\" action=\"index.php\">
                ";
            // line 81
            if ((($context["currentRoute"] ?? null) === "product/search")) {
                // line 82
                echo "                    <input type=\"hidden\" name=\"route\" value=\"product/search\" />
                ";
            } else {
                // line 84
                echo "                    <input type=\"hidden\" name=\"route\" value=\"product/category\" />
                ";
            }
            // line 86
            echo "                ";
            if (($context["currentPath"] ?? null)) {
                // line 87
                echo "                    <input type=\"hidden\" name=\"path\" value=\"";
                echo ($context["currentPath"] ?? null);
                echo "\" />
                ";
            }
            // line 89
            echo "                ";
            if (($context["manufacturerId"] ?? null)) {
                // line 90
                echo "                    <input type=\"hidden\" name=\"manufacturer_id\" value=\"";
                echo ($context["manufacturerId"] ?? null);
                echo "\" />
                ";
            }
            // line 92
            echo "
                ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["filters"] ?? null));
            foreach ($context['_seq'] as $context["i"] => $context["section"]) {
                // line 94
                echo "                        
                    ";
                // line 95
                if (((($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 = $context["section"]) && is_array($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762) || $__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 instanceof ArrayAccess ? ($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762["type"] ?? null) : null) == "price")) {
                    // line 96
                    echo "                        ";
                    $context["sliderType"] = ((((($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 = $context["section"]) && is_array($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053) || $__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 instanceof ArrayAccess ? ($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053["control"] ?? null) : null) === "slider_lbl_inp")) ? (3) : (((((($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c = $context["section"]) && is_array($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c) || $__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c instanceof ArrayAccess ? ($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c["control"] ?? null) : null) === "slider_lbl")) ? (2) : (1))));
                    // line 97
                    echo "                        ";
                    $context["inputType"] = ((twig_in_filter(($context["sliderType"] ?? null), [0 => 1, 1 => 3])) ? ("text") : ("hidden"));
                    // line 98
                    echo "                        <div class=\"bf-attr-block bf-price-filter ";
                    if (((($context["isHorizontal"] ?? null) && twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), ($context["i"] + 1), [], "array", true, true, false, 98)) && ((($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c = (($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 = ($context["filters"] ?? null)) && is_array($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030) || $__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030 instanceof ArrayAccess ? ($__internal_c8e66b28fe4788d592082dfe3aeeaa036a7caf1017aa84d9002984c1f4fbc030[($context["i"] + 1)] ?? null) : null)) && is_array($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c) || $__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c instanceof ArrayAccess ? ($__internal_161aee9a7f672339d6d858e64e1de832e33321400f3f2381c16bf9c6d2fbcc9c["type"] ?? null) : null) === "search"))) {
                        echo "bf-left-half";
                    }
                    echo "\">
                        <div class=\"bf-attr-header ";
                    // line 99
                    if ((($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 = $context["section"]) && is_array($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8) || $__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8 instanceof ArrayAccess ? ($__internal_039139496843b11bef2e1873734e0f4e6f0334f99b26b9202f2107aca1929bb8["collapsed"] ?? null) : null)) {
                        echo " bf-collapse ";
                    }
                    echo " ";
                    if ( !$context["i"]) {
                        echo " bf-w-line ";
                    }
                    echo "\">
                            ";
                    // line 100
                    echo ($context["lang_price"] ?? null);
                    echo "<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            <div class=\"bf-price-container box-content bf-attr-filter\">
                                ";
                    // line 104
                    if (twig_in_filter(($context["sliderType"] ?? null), [0 => 1, 1 => 3])) {
                        // line 105
                        echo "                                <div class=\"bf-cur-symb\">
                                    <span class=\"bf-cur-symb-left\">";
                        // line 106
                        echo ($context["currency_symbol"] ?? null);
                        echo "</span>
                                    <input type=\"text\" class=\"bf-range-min\" name=\"bfp_price_min\" value=\"";
                        // line 107
                        echo ($context["lowerlimit"] ?? null);
                        echo "\" size=\"4\" />
                                    <span class=\"ndash\">&#8211;</span>
                                    <span class=\"bf-cur-symb-left\">";
                        // line 109
                        echo ($context["currency_symbol"] ?? null);
                        echo "</span>
                                    <input type=\"text\" class=\"bf-range-max\" name=\"bfp_price_max\" value=\"";
                        // line 110
                        echo ($context["upperlimit"] ?? null);
                        echo "\" size=\"4\" /> 
                                </div>
                                ";
                    } else {
                        // line 113
                        echo "                                <input type=\"hidden\" class=\"bf-range-min\" name=\"bfp_price_min\" value=\"";
                        echo ($context["lowerlimit"] ?? null);
                        echo "\" />
                                <input type=\"hidden\" class=\"bf-range-max\" name=\"bfp_price_max\" value=\"";
                        // line 114
                        echo ($context["upperlimit"] ?? null);
                        echo "\" /> 
                                ";
                    }
                    // line 116
                    echo "                                <div class=\"bf-price-slider-container ";
                    if (((($context["sliderType"] ?? null) === 2) || (($context["sliderType"] ?? null) === 3))) {
                        echo " bf-slider-with-labels ";
                    }
                    echo "\">
                                    <div class=\"bf-slider-range\" data-slider-type=\"";
                    // line 117
                    echo ($context["sliderType"] ?? null);
                    echo "\"></div>
                                </div>
                            </div>
                        </div>
                        </div>
                
                    ";
                } elseif (((($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 =                 // line 123
$context["section"]) && is_array($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86) || $__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86 instanceof ArrayAccess ? ($__internal_925e03cbc484a83726b2283dd3b53369cf62a514035d11f764f348b039e42e86["type"] ?? null) : null) == "search")) {
                    // line 124
                    echo "                
                        <div class=\"bf-attr-block bf-keywords-filter ";
                    // line 125
                    if (((($context["isHorizontal"] ?? null) && twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), ($context["i"] + 1), [], "array", true, true, false, 125)) && ((($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 = (($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac = ($context["filters"] ?? null)) && is_array($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac) || $__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac instanceof ArrayAccess ? ($__internal_7f8b6b79c000ace681a97eb4e372ecbf3824a243268aa8909f315967b09890ac[($context["i"] + 1)] ?? null) : null)) && is_array($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9) || $__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9 instanceof ArrayAccess ? ($__internal_1851fce5e10e004219a620bc4ec54e0dce7d95e3cc5df26b354b442a89edf2a9["type"] ?? null) : null) === "price"))) {
                        echo " bf-left-half ";
                    }
                    echo "\">
                        <div class=\"bf-attr-header";
                    // line 126
                    if ((($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 = $context["section"]) && is_array($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768) || $__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768 instanceof ArrayAccess ? ($__internal_f729ba442740d3f6c098998c72ec6936b2f5c5d7642933047145361560991768["collapsed"] ?? null) : null)) {
                        echo " bf-collapse ";
                    }
                    echo " ";
                    if ( !$context["i"]) {
                        echo " bf-w-line ";
                    }
                    echo "\">
                            ";
                    // line 127
                    echo ($context["lang_search"] ?? null);
                    echo "<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            <div class=\"bf-search-container bf-attr-filter\">
                                <div>
                                    <input type=\"text\" class=\"bf-search\" name=\"bfp_search\" value=\"";
                    // line 132
                    echo ($context["bfSearch"] ?? null);
                    echo "\" /> 
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    ";
                } elseif (((($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 =                 // line 138
$context["section"]) && is_array($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57) || $__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57 instanceof ArrayAccess ? ($__internal_9092e96c712a0a0873eb67a677c52108ea03891525ad69649382158e33697b57["type"] ?? null) : null) == "category")) {
                    // line 139
                    echo "                        
                        <div class=\"bf-attr-block\">
                        <div class=\"bf-attr-header";
                    // line 141
                    if ((($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 = $context["section"]) && is_array($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898) || $__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898 instanceof ArrayAccess ? ($__internal_fd5cc34776dcec03d7489322c0a0c72f1de5a01209896acc469d764bdcfe2898["collapsed"] ?? null) : null)) {
                        echo " bf-collapse ";
                    }
                    echo " ";
                    if ( !$context["i"]) {
                        echo " bf-w-line ";
                    }
                    echo "\">
                            ";
                    // line 142
                    echo ($context["lang_categories"] ?? null);
                    echo "<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            ";
                    // line 145
                    $context["groupUID"] = "c0";
                    // line 146
                    echo "
                            ";
                    // line 147
                    if (((($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 = $context["section"]) && is_array($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283) || $__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283 instanceof ArrayAccess ? ($__internal_e7cec1b021878d1bb02c1063e199e8cefa56cb589808a137e4cbc1921ac94283["control"] ?? null) : null) == "select")) {
                        // line 148
                        echo "                            <div class=\"bf-attr-filter bf-attr-";
                        echo ($context["groupUID"] ?? null);
                        echo " bf-row\">
                                <div class=\"bf-cell\">
                                    <select name=\"bfp_";
                        // line 150
                        echo ($context["groupUID"] ?? null);
                        echo "\">
                                        <option value=\"\" class=\"bf-default\">";
                        // line 151
                        echo ($context["default_value_select"] ?? null);
                        echo "</option>
                                        ";
                        // line 152
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a = $context["section"]) && is_array($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a) || $__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a instanceof ArrayAccess ? ($__internal_d531a19fddb41a9467c1a55a54b8a26432b407278d04ee272777b6e18b4ccd7a["values"] ?? null) : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                            // line 153
                            echo "                                            ";
                            $context["catId"] = (($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 = $context["cat"]) && is_array($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3) || $__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3 instanceof ArrayAccess ? ($__internal_1cd2a3f8cba41eac87892993230e5421a7dbd05c0ead14fc195d5433379f30f3["id"] ?? null) : null);
                            // line 154
                            echo "                                            ";
                            $context["isSelected"] = (twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 154) && twig_in_filter(($context["catId"] ?? null), (($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 = ($context["selected"] ?? null)) && is_array($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4) || $__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4 instanceof ArrayAccess ? ($__internal_83b8171902561b20ceb42baa6f852f2579c5aad78c12181da527b65620a553b4[($context["groupUID"] ?? null)] ?? null) : null)));
                            // line 155
                            echo "                                            <option value=\"";
                            echo ($context["catId"] ?? null);
                            echo "\" class=\"bf-attr-val\" ";
                            if (($context["isSelected"] ?? null)) {
                                echo " selected=\"true\" ";
                            }
                            echo ">
                                                ";
                            // line 156
                            $context["level"] = "";
                            // line 157
                            echo "                                                ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(range(0, (($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 = $context["cat"]) && is_array($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9) || $__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9 instanceof ArrayAccess ? ($__internal_daa44007e692567557eff5658cd46870513c97a8bc03c58428d8aaae92c0e8c9["level"] ?? null) : null)));
                            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                // line 158
                                echo "                                                    ";
                                if ($context["i"]) {
                                    // line 159
                                    echo "                                                        ";
                                    $context["level"] = (($context["level"] ?? null) . "-");
                                    // line 160
                                    echo "                                                    ";
                                }
                                // line 161
                                echo "                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 162
                            echo "                                                ";
                            echo ((($context["level"] ?? null) . " ") . (($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 = $context["cat"]) && is_array($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7) || $__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7 instanceof ArrayAccess ? ($__internal_e1b1a26e763ae747d1fc3d1b0b9c2b4626803f6553cb2f29a46e9b3f9b6a6aa7["name"] ?? null) : null));
                            echo "
                                            </option>
                                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 165
                        echo "                                    </select>
                                </div>
                            </div>
                            ";
                    } else {
                        // line 169
                        echo "                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 = $context["section"]) && is_array($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416) || $__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416 instanceof ArrayAccess ? ($__internal_dc5d8f1b0e8d8f121483833b3819db802deb2a1282c5450df5fbbdb4c4c3d416["values"] ?? null) : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                            // line 170
                            echo "                                    ";
                            $context["catId"] = (($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e = $context["cat"]) && is_array($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e) || $__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e instanceof ArrayAccess ? ($__internal_9b87a1e1323fa7607c7e95b07cf5d6a8a31261a0bbac03dc74c72110212f8f4e["id"] ?? null) : null);
                            // line 171
                            echo "                                    <div class=\"bf-attr-filter bf-attr-";
                            echo ($context["groupUID"] ?? null);
                            echo " bf-row
                                        ";
                            // line 172
                            if ((array_key_exists("totals", $context) && (($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f = (($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b = ($context["settings"] ?? null)) && is_array($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b) || $__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b instanceof ArrayAccess ? ($__internal_c937147b4224d1a42b33a5bd4d8cc7ca7f03deaf5756b9444870e8af2d4e771b["behaviour"] ?? null) : null)) && is_array($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f) || $__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f instanceof ArrayAccess ? ($__internal_473f956237dde602dca8d4c42519c23a7466c04927553a71be9b287c435e2e1f["hide_empty"] ?? null) : null))) {
                                // line 173
                                echo "                                            ";
                                $context["inStock"] = (($context["postponedCount"] ?? null) || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 173), ($context["catId"] ?? null), [], "array", true, true, false, 173) && (($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 = (($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c = ($context["totals"] ?? null)) && is_array($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c) || $__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c instanceof ArrayAccess ? ($__internal_5af03ff0cc8e1222402f36d418bce8507137ed70ad84b904d8c76ad12c3cdb1c[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75) || $__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75 instanceof ArrayAccess ? ($__internal_f312a27c239aee4ab13fb0728a2a81fd48b1756504f2c7f0a60f8e8114891a75[($context["catId"] ?? null)] ?? null) : null)));
                                // line 174
                                echo "                                            ";
                                $context["inSelected"] = (twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 174) && twig_in_filter(($context["catId"] ?? null), (($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 = ($context["selected"] ?? null)) && is_array($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1) || $__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1 instanceof ArrayAccess ? ($__internal_9af1f39a092ea44798cef53686837b7a0e65bc2d674686cabb26ec927398b4a1[($context["groupUID"] ?? null)] ?? null) : null)));
                                // line 175
                                echo "                                            ";
                                if (( !($context["inStock"] ?? null) &&  !($context["inSelected"] ?? null))) {
                                    // line 176
                                    echo "                                                bf-disabled
                                            ";
                                }
                                // line 178
                                echo "                                        ";
                            }
                            echo "\">
                                    <span class=\"bf-cell bf-c-1\">
                                        <input id=\"bf-attr-";
                            // line 180
                            echo ((((($context["groupUID"] ?? null) . "_") . ($context["catId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                            echo "\"
                                               data-filterid=\"bf-attr-";
                            // line 181
                            echo ((($context["groupUID"] ?? null) . "_") . ($context["catId"] ?? null));
                            echo "\"
                                               type=\"";
                            // line 182
                            echo (($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 = $context["section"]) && is_array($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24) || $__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24 instanceof ArrayAccess ? ($__internal_ac7e48faa0323c0109c068324f874a96d3f538986706d62646c4ff8bea813b24["control"] ?? null) : null);
                            echo "\" 
                                               name=\"bfp_";
                            // line 183
                            echo ($context["groupUID"] ?? null);
                            if (((($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 = $context["section"]) && is_array($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850) || $__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850 instanceof ArrayAccess ? ($__internal_b9697a1a026ba6f17a3b8f67645afbc14e9a7e8db717019bc29adbc98cc84850["control"] ?? null) : null) === "checkbox")) {
                                echo ("_" . ($context["catId"] ?? null));
                            }
                            echo "\"
                                               value=\"";
                            // line 184
                            echo ($context["catId"] ?? null);
                            echo "\" 
                                               ";
                            // line 185
                            if ((twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 185) && twig_in_filter(($context["catId"] ?? null), (($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 = ($context["selected"] ?? null)) && is_array($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34) || $__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34 instanceof ArrayAccess ? ($__internal_1d930af3621b2130f4718a24e570af2acfbccfb3425f8b4bdd93052a4b2e1e34[($context["groupUID"] ?? null)] ?? null) : null)))) {
                                echo " checked=\"true\" ";
                            }
                            echo " />
                                    </span>
                                    <span class=\"bf-cell bf-c-2 bf-cascade-";
                            // line 187
                            echo (($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df = $context["cat"]) && is_array($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df) || $__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df instanceof ArrayAccess ? ($__internal_cd308af9d66532a4787f365d74d2c10bc61439099a68241bdbc89bc9680a29df["level"] ?? null) : null);
                            echo "\">
                                        <span class=\"bf-hidden bf-attr-val\">";
                            // line 188
                            echo ($context["catId"] ?? null);
                            echo "</span>
                                        <label for=\"bf-attr-";
                            // line 189
                            echo ((((($context["groupUID"] ?? null) . "_") . ($context["catId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                            echo "\">
                                            ";
                            // line 190
                            echo (($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 = $context["cat"]) && is_array($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4) || $__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4 instanceof ArrayAccess ? ($__internal_5c7a1d4bbedb4e71d3728c47d25651b741a575e7549d719db9e389ac31f9e0e4["name"] ?? null) : null);
                            echo "
                                        </label>
                                    </span>
                                            <span class=\"bf-cell bf-c-3\">
                                                ";
                            // line 194
                            if (array_key_exists("totals", $context)) {
                                // line 195
                                echo "                                                    ";
                                if (( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 195), ($context["catId"] ?? null), [], "array", true, true, false, 195) &&  !twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 195))) {
                                    // line 196
                                    echo "                                                        ";
                                    echo "";
                                    echo "
                                                    ";
                                } else {
                                    // line 198
                                    echo "                                                        ";
                                    $context["total"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 198), ($context["catId"] ?? null), [], "array", true, true, false, 198)) ? ((($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 = (($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b = ($context["totals"] ?? null)) && is_array($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b) || $__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b instanceof ArrayAccess ? ($__internal_57db64d4ce3248effca58b9fa40f0a0305fbc7853831e1cd8a6a1a6d4c41e03b[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36) || $__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36 instanceof ArrayAccess ? ($__internal_d315cac7207a8fae6d0ee59f144a64ec832037139e03f92fd0b4f245fe3b7b36[($context["catId"] ?? null)] ?? null) : null)) : (0));
                                    // line 199
                                    echo "                                                        ";
                                    $context["addPlusSign"] = twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 199);
                                    // line 200
                                    echo "                                                        <span class=\"bf-count ";
                                    echo (( !($context["total"] ?? null)) ? ("bf-empty") : (""));
                                    echo "\">";
                                    echo ((($context["addPlusSign"] ?? null)) ? ("+") : (""));
                                    echo ($context["total"] ?? null);
                                    echo "</span>
                                                    ";
                                }
                                // line 202
                                echo "                                                ";
                            }
                            // line 203
                            echo "                                            </span>
                                </div>
                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 206
                        echo "                            ";
                    }
                    // line 207
                    echo "                        </div>
                        </div>
                
                    ";
                } else {
                    // line 211
                    echo "                        
                        ";
                    // line 212
                    $context["curGroupId"] = null;
                    // line 213
                    echo "                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e = $context["section"]) && is_array($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e) || $__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e instanceof ArrayAccess ? ($__internal_d128b295b5eb655509cce26cda95e1ee2e40d0773f4663d4c1290ef76c63f53e["array"] ?? null) : null));
                    foreach ($context['_seq'] as $context["groupId"] => $context["group"]) {
                        // line 214
                        echo "                            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["group"], "group_id", [], "array", true, true, false, 214) && (($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 = (($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606 = ($context["settings"] ?? null)) && is_array($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606) || $__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606 instanceof ArrayAccess ? ($__internal_eac0fb02beae87e52d8817de26caac024b72dbca3fe084a7fb60ce6297e74606["behaviour"] ?? null) : null)) && is_array($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7) || $__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7 instanceof ArrayAccess ? ($__internal_c13aaf965ee968fbdf4e25d265e9ad3332196be42b56e71118384af8d580afc7["attribute_groups"] ?? null) : null))) {
                            // line 215
                            echo "                                ";
                            if ((($context["curGroupId"] ?? null) != (($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd = $context["group"]) && is_array($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd) || $__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd instanceof ArrayAccess ? ($__internal_f449bd2e1c43123f4aea5ebb1dcb3149049e6b08332d88c5cbea9cbf72d7d7fd["group_id"] ?? null) : null))) {
                                // line 216
                                echo "                                    <div class=\"bf-attr-group-header\">";
                                echo (($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e = $context["group"]) && is_array($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e) || $__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e instanceof ArrayAccess ? ($__internal_ac6028158aec8e9114a7b50d00df46f3a0352559c4384cdefd768fa8d1f5095e["group"] ?? null) : null);
                                echo "</div>
                                    ";
                                // line 217
                                $context["curGroupId"] = (($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1 = $context["group"]) && is_array($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1) || $__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1 instanceof ArrayAccess ? ($__internal_7c32a0b33fb8ca8d971d62abc97ef27c0b0f4cefceb603cb91f0956165f4a2e1["group_id"] ?? null) : null);
                                // line 218
                                echo "                                ";
                            }
                            // line 219
                            echo "                            ";
                        }
                        // line 220
                        echo "                            ";
                        $context["collapsedGroup"] = false;
                        // line 221
                        echo "                            ";
                        if (((($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb = $context["group"]) && is_array($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb) || $__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb instanceof ArrayAccess ? ($__internal_68d3b371ec0c4bb1581025ed4c1d76a431a042b7b439120f66468cb409de0cdb["attr_id"] ?? null) : null) && (($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf = (($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b = (($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980 = (($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345 = ($context["settings"] ?? null)) && is_array($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345) || $__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345 instanceof ArrayAccess ? ($__internal_58f05cb7b103fdb27c83e116d9b750a441975afa718f181d426ba20756cae345["behaviour"] ?? null) : null)) && is_array($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980) || $__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980 instanceof ArrayAccess ? ($__internal_7c817ef80fec483e83fdd5a0d75d7936b34e91df63a1e5f99c810f6ddfb73980["sections"] ?? null) : null)) && is_array($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b) || $__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b instanceof ArrayAccess ? ($__internal_1fa86e54c040f0d1b500ff8a8536fb704ead4a955f38e9ee0c72d436e09d2d6b["attribute"] ?? null) : null)) && is_array($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf) || $__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf instanceof ArrayAccess ? ($__internal_12df7a6a0a260f0401b6892f7ce4fef2ea0fea7f4abf3aaab9ef6f1113a738cf[(($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3 = $context["group"]) && is_array($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3) || $__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3 instanceof ArrayAccess ? ($__internal_2c848f3022a3402e3a4e27a30257fa7d076f394b2c17fd1315626995668cc7a3["attr_id"] ?? null) : null)] ?? null) : null))) {
                            // line 222
                            echo "                                ";
                            $context["collapsedGroup"] = (($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0 = (($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938 = (($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3 = (($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa = (($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb = ($context["settings"] ?? null)) && is_array($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb) || $__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb instanceof ArrayAccess ? ($__internal_04e2723480818cf7e4ae08c1e7380310abe34ee48600ebabbfbaca3a62b4f1fb["behaviour"] ?? null) : null)) && is_array($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa) || $__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa instanceof ArrayAccess ? ($__internal_740db85f46dbd95cea320267399fd88e8007c386d126eec44ce5a5594fea0daa["sections"] ?? null) : null)) && is_array($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3) || $__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3 instanceof ArrayAccess ? ($__internal_079975fe41d37645946c3a823d9bb78a9ae0e38816557a03403725361f35feb3["attribute"] ?? null) : null)) && is_array($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938) || $__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938 instanceof ArrayAccess ? ($__internal_c1cd480b2bae110b528bbc3f808e69c4b6a9aeedf00a361275f8ddb342dfe938[(($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c = $context["group"]) && is_array($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c) || $__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c instanceof ArrayAccess ? ($__internal_9dfe9126eb6cb3d8182bbdebdcbf291354ce41935a4d52134757b624790fe26c["attr_id"] ?? null) : null)] ?? null) : null)) && is_array($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0) || $__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0 instanceof ArrayAccess ? ($__internal_b8c7cfa2093058440418fed5e0a741d0931d374a0b972ab2bdfe5d1a043c45d0["collapsed"] ?? null) : null);
                            // line 223
                            echo "                            ";
                        } elseif (((($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a = $context["group"]) && is_array($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a) || $__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a instanceof ArrayAccess ? ($__internal_7886d104df990d4d01343e15743b569d1995f6a6a8de3ead740a6091880b629a["option_id"] ?? null) : null) && (($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6 = (($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b = (($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526 = (($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f = ($context["settings"] ?? null)) && is_array($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f) || $__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f instanceof ArrayAccess ? ($__internal_69673c0dda0724bda92ca0f89665181eb299815d5bf0d9166a7fa457f623049f["behaviour"] ?? null) : null)) && is_array($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526) || $__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526 instanceof ArrayAccess ? ($__internal_eed548cde44c216c917d86f1b41aeead16364f508b904d138a9861b48cf18526["sections"] ?? null) : null)) && is_array($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b) || $__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b instanceof ArrayAccess ? ($__internal_d9d33b22591102d2e461af9c204c3c40751fa247b0275a5e9ac02a242b6b099b["options"] ?? null) : null)) && is_array($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6) || $__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6 instanceof ArrayAccess ? ($__internal_bf8548f45bc193921ffb4426690048a7605b21cb5873c3e67934670fc157bcb6[(($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c = $context["group"]) && is_array($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c) || $__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c instanceof ArrayAccess ? ($__internal_98e7c66b1d8077f0adf5874b6a626a0256df01315d35d9e34fe7dbdf2b1f397c["option_id"] ?? null) : null)] ?? null) : null))) {
                            // line 224
                            echo "                                ";
                            $context["collapsedGroup"] = (($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74 = (($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff = (($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918 = (($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5 = (($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219 = ($context["settings"] ?? null)) && is_array($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219) || $__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219 instanceof ArrayAccess ? ($__internal_75d475ff9edc93fda230c7c714c00f4c5dbb39fa0dbbcb262e9ab1617f92f219["behaviour"] ?? null) : null)) && is_array($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5) || $__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5 instanceof ArrayAccess ? ($__internal_b57c690297f5d1db403c4e65f613e450889065335a92d7f73f82e713f90b25e5["sections"] ?? null) : null)) && is_array($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918) || $__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918 instanceof ArrayAccess ? ($__internal_7f64f85f9301de90a5b045895fc6e5587d70b65ebc68918344f8c25d458d3918["options"] ?? null) : null)) && is_array($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff) || $__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff instanceof ArrayAccess ? ($__internal_4a561b149f190c3cf54242a61f4c7f0df2a717b925f2c7a775371ef55c39caff[(($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20 = $context["group"]) && is_array($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20) || $__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20 instanceof ArrayAccess ? ($__internal_5aa517627f62fe0421e8b859b10fd7903a81d7224c214373093f337db21ecc20["option_id"] ?? null) : null)] ?? null) : null)) && is_array($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74) || $__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74 instanceof ArrayAccess ? ($__internal_b34f576c9690a300f94652a12516183f72eacabacea74206fd0ebac5164ede74["collapsed"] ?? null) : null);
                            // line 225
                            echo "                            ";
                        } elseif (((($__internal_59583b2f460abbfb2a52836600eada7d270602d83eceef746b0a8a9b74fdad16 = $context["group"]) && is_array($__internal_59583b2f460abbfb2a52836600eada7d270602d83eceef746b0a8a9b74fdad16) || $__internal_59583b2f460abbfb2a52836600eada7d270602d83eceef746b0a8a9b74fdad16 instanceof ArrayAccess ? ($__internal_59583b2f460abbfb2a52836600eada7d270602d83eceef746b0a8a9b74fdad16["filter_id"] ?? null) : null) && (($__internal_eb210cb6135ea08a769b7294a890e7a98a83ae7e9aaa91aca4b3341ab5eedef0 = (($__internal_3c68232c931f587fa4dbd185c63e9c57f23a8dfaef6e79a75ec5650821da84b1 = (($__internal_8299895db9519c717c7e4fc5f671ace0c58ed9700678f3c82fe9e8d06a692008 = (($__internal_3558a217fd2ee5e5e39300afd7d24968a189797715bbe1595642d4b28fdafd00 = ($context["settings"] ?? null)) && is_array($__internal_3558a217fd2ee5e5e39300afd7d24968a189797715bbe1595642d4b28fdafd00) || $__internal_3558a217fd2ee5e5e39300afd7d24968a189797715bbe1595642d4b28fdafd00 instanceof ArrayAccess ? ($__internal_3558a217fd2ee5e5e39300afd7d24968a189797715bbe1595642d4b28fdafd00["behaviour"] ?? null) : null)) && is_array($__internal_8299895db9519c717c7e4fc5f671ace0c58ed9700678f3c82fe9e8d06a692008) || $__internal_8299895db9519c717c7e4fc5f671ace0c58ed9700678f3c82fe9e8d06a692008 instanceof ArrayAccess ? ($__internal_8299895db9519c717c7e4fc5f671ace0c58ed9700678f3c82fe9e8d06a692008["sections"] ?? null) : null)) && is_array($__internal_3c68232c931f587fa4dbd185c63e9c57f23a8dfaef6e79a75ec5650821da84b1) || $__internal_3c68232c931f587fa4dbd185c63e9c57f23a8dfaef6e79a75ec5650821da84b1 instanceof ArrayAccess ? ($__internal_3c68232c931f587fa4dbd185c63e9c57f23a8dfaef6e79a75ec5650821da84b1["filter"] ?? null) : null)) && is_array($__internal_eb210cb6135ea08a769b7294a890e7a98a83ae7e9aaa91aca4b3341ab5eedef0) || $__internal_eb210cb6135ea08a769b7294a890e7a98a83ae7e9aaa91aca4b3341ab5eedef0 instanceof ArrayAccess ? ($__internal_eb210cb6135ea08a769b7294a890e7a98a83ae7e9aaa91aca4b3341ab5eedef0[(($__internal_e4582927b18c273aab9fa9f2f830b340dbc1393d62fc92ee04c436056cc3e315 = $context["group"]) && is_array($__internal_e4582927b18c273aab9fa9f2f830b340dbc1393d62fc92ee04c436056cc3e315) || $__internal_e4582927b18c273aab9fa9f2f830b340dbc1393d62fc92ee04c436056cc3e315 instanceof ArrayAccess ? ($__internal_e4582927b18c273aab9fa9f2f830b340dbc1393d62fc92ee04c436056cc3e315["filter_id"] ?? null) : null)] ?? null) : null))) {
                            // line 226
                            echo "                                ";
                            $context["collapsedGroup"] = (($__internal_90f4de91f7794e33f5d5fc1a817f159833c6316cbb98068d30eea7b36ee580eb = (($__internal_fcbe1339b13cfceb7464b73e579a3d049ba6a61cd21bfa924a6104b1f9e70bde = (($__internal_030d86864426f94dd0e16219c86ce091338cf650d27fbbfbcd6a8a0a1595b8b5 = (($__internal_87681efb9ab969cc41312bbee69f5bb70f772c16b053746b0bf6082c25cf226f = (($__internal_075fde5d80e6f3ce1d25eb3926912924a44e2a84db14fc836f4fc19fcb57fc2b = ($context["settings"] ?? null)) && is_array($__internal_075fde5d80e6f3ce1d25eb3926912924a44e2a84db14fc836f4fc19fcb57fc2b) || $__internal_075fde5d80e6f3ce1d25eb3926912924a44e2a84db14fc836f4fc19fcb57fc2b instanceof ArrayAccess ? ($__internal_075fde5d80e6f3ce1d25eb3926912924a44e2a84db14fc836f4fc19fcb57fc2b["behaviour"] ?? null) : null)) && is_array($__internal_87681efb9ab969cc41312bbee69f5bb70f772c16b053746b0bf6082c25cf226f) || $__internal_87681efb9ab969cc41312bbee69f5bb70f772c16b053746b0bf6082c25cf226f instanceof ArrayAccess ? ($__internal_87681efb9ab969cc41312bbee69f5bb70f772c16b053746b0bf6082c25cf226f["sections"] ?? null) : null)) && is_array($__internal_030d86864426f94dd0e16219c86ce091338cf650d27fbbfbcd6a8a0a1595b8b5) || $__internal_030d86864426f94dd0e16219c86ce091338cf650d27fbbfbcd6a8a0a1595b8b5 instanceof ArrayAccess ? ($__internal_030d86864426f94dd0e16219c86ce091338cf650d27fbbfbcd6a8a0a1595b8b5["filter"] ?? null) : null)) && is_array($__internal_fcbe1339b13cfceb7464b73e579a3d049ba6a61cd21bfa924a6104b1f9e70bde) || $__internal_fcbe1339b13cfceb7464b73e579a3d049ba6a61cd21bfa924a6104b1f9e70bde instanceof ArrayAccess ? ($__internal_fcbe1339b13cfceb7464b73e579a3d049ba6a61cd21bfa924a6104b1f9e70bde[(($__internal_8a99126ee0c242cbea7a1e47fa82c7524b50d92010e4de19211e97cee1960d7d = $context["group"]) && is_array($__internal_8a99126ee0c242cbea7a1e47fa82c7524b50d92010e4de19211e97cee1960d7d) || $__internal_8a99126ee0c242cbea7a1e47fa82c7524b50d92010e4de19211e97cee1960d7d instanceof ArrayAccess ? ($__internal_8a99126ee0c242cbea7a1e47fa82c7524b50d92010e4de19211e97cee1960d7d["filter_id"] ?? null) : null)] ?? null) : null)) && is_array($__internal_90f4de91f7794e33f5d5fc1a817f159833c6316cbb98068d30eea7b36ee580eb) || $__internal_90f4de91f7794e33f5d5fc1a817f159833c6316cbb98068d30eea7b36ee580eb instanceof ArrayAccess ? ($__internal_90f4de91f7794e33f5d5fc1a817f159833c6316cbb98068d30eea7b36ee580eb["collapsed"] ?? null) : null);
                            // line 227
                            echo "                            ";
                        }
                        // line 228
                        echo "                            ";
                        $context["groupUID"] = (twig_slice($this->env, (($__internal_d4953b3c006295766d1c122e12bc1011d304aaceeaa5adaad0ddacee9b0ac73d = $context["section"]) && is_array($__internal_d4953b3c006295766d1c122e12bc1011d304aaceeaa5adaad0ddacee9b0ac73d) || $__internal_d4953b3c006295766d1c122e12bc1011d304aaceeaa5adaad0ddacee9b0ac73d instanceof ArrayAccess ? ($__internal_d4953b3c006295766d1c122e12bc1011d304aaceeaa5adaad0ddacee9b0ac73d["type"] ?? null) : null), 0, 1) . $context["groupId"]);
                        // line 229
                        echo "                            <div class=\"bf-attr-block";
                        if (twig_in_filter((($__internal_4d98e80b72602982463f469eb9bdc0f5c8104d67d420559aef3c263130076eb2 = $context["group"]) && is_array($__internal_4d98e80b72602982463f469eb9bdc0f5c8104d67d420559aef3c263130076eb2) || $__internal_4d98e80b72602982463f469eb9bdc0f5c8104d67d420559aef3c263130076eb2 instanceof ArrayAccess ? ($__internal_4d98e80b72602982463f469eb9bdc0f5c8104d67d420559aef3c263130076eb2["type"] ?? null) : null), [0 => "slider", 1 => "slider_lbl", 2 => "slider_lbl_inp"])) {
                            echo " bf-slider ";
                        }
                        echo "\">
                            <div class=\"bf-attr-header";
                        // line 230
                        if (((($__internal_9bf6299aac8f9c23c0d5210c076dcd6a9ada2688fdd89682c61bad4df90664b6 = $context["section"]) && is_array($__internal_9bf6299aac8f9c23c0d5210c076dcd6a9ada2688fdd89682c61bad4df90664b6) || $__internal_9bf6299aac8f9c23c0d5210c076dcd6a9ada2688fdd89682c61bad4df90664b6 instanceof ArrayAccess ? ($__internal_9bf6299aac8f9c23c0d5210c076dcd6a9ada2688fdd89682c61bad4df90664b6["collapsed"] ?? null) : null) || ($context["collapsedGroup"] ?? null))) {
                            echo " bf-collapse ";
                        }
                        if ( !$context["i"]) {
                            echo " bf-w-line ";
                        }
                        echo "\">
                                ";
                        // line 231
                        echo ((($__internal_831874d43c225a9c9f6df2b07573e383cb6d5e4c3cb2ac53d736653473ba264a = $context["group"]) && is_array($__internal_831874d43c225a9c9f6df2b07573e383cb6d5e4c3cb2ac53d736653473ba264a) || $__internal_831874d43c225a9c9f6df2b07573e383cb6d5e4c3cb2ac53d736653473ba264a instanceof ArrayAccess ? ($__internal_831874d43c225a9c9f6df2b07573e383cb6d5e4c3cb2ac53d736653473ba264a["name"] ?? null) : null) . " ");
                        echo "<span class=\"bf-arrow\"></span>
                            </div>
                            <div class=\"bf-attr-block-cont\">
                                    ";
                        // line 234
                        $context["group"] = ((twig_get_attribute($this->env, $this->source, $context["group"], "type", [], "array", true, true, false, 234)) ? (twig_array_merge($context["group"], ["type" => (($__internal_ed2b0be7f3a3dac5fb5105bb7e9c1e1f6415f5faf37af639bfc973c1dc249cee = $context["group"]) && is_array($__internal_ed2b0be7f3a3dac5fb5105bb7e9c1e1f6415f5faf37af639bfc973c1dc249cee) || $__internal_ed2b0be7f3a3dac5fb5105bb7e9c1e1f6415f5faf37af639bfc973c1dc249cee instanceof ArrayAccess ? ($__internal_ed2b0be7f3a3dac5fb5105bb7e9c1e1f6415f5faf37af639bfc973c1dc249cee["type"] ?? null) : null)])) : (twig_array_merge($context["group"], ["type" => "checkbox"])));
                        // line 235
                        echo "                                
                                ";
                        // line 236
                        if (((($__internal_ee4a9f489859b49aae8b28fc320a26b03d2394f3fb8bb9a69827e8f612baa523 = $context["group"]) && is_array($__internal_ee4a9f489859b49aae8b28fc320a26b03d2394f3fb8bb9a69827e8f612baa523) || $__internal_ee4a9f489859b49aae8b28fc320a26b03d2394f3fb8bb9a69827e8f612baa523 instanceof ArrayAccess ? ($__internal_ee4a9f489859b49aae8b28fc320a26b03d2394f3fb8bb9a69827e8f612baa523["type"] ?? null) : null) == "select")) {
                            // line 237
                            echo "                                
                                    <div class=\"bf-attr-filter bf-attr-";
                            // line 238
                            echo ($context["groupUID"] ?? null);
                            echo " bf-row\">
                                        <div class=\"bf-cell\">
                                            <select name=\"bfp_";
                            // line 240
                            echo ($context["groupUID"] ?? null);
                            echo "\">
                                                <option value=\"\" class=\"bf-default\">";
                            // line 241
                            echo ($context["default_value_select"] ?? null);
                            echo "</option>
                                                ";
                            // line 242
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable((($__internal_ea734457fe1ab835acb1bdc5f4d0c289028390941bde9830f5c3eb108557bddc = $context["group"]) && is_array($__internal_ea734457fe1ab835acb1bdc5f4d0c289028390941bde9830f5c3eb108557bddc) || $__internal_ea734457fe1ab835acb1bdc5f4d0c289028390941bde9830f5c3eb108557bddc instanceof ArrayAccess ? ($__internal_ea734457fe1ab835acb1bdc5f4d0c289028390941bde9830f5c3eb108557bddc["values"] ?? null) : null));
                            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                                // line 243
                                echo "                                                    ";
                                $context["isSelected"] = (twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 243) && twig_in_filter((($__internal_37780db4de3f0740e927a120a7d64820d6ae0626dadd9715b6d9b78b685d391a = $context["value"]) && is_array($__internal_37780db4de3f0740e927a120a7d64820d6ae0626dadd9715b6d9b78b685d391a) || $__internal_37780db4de3f0740e927a120a7d64820d6ae0626dadd9715b6d9b78b685d391a instanceof ArrayAccess ? ($__internal_37780db4de3f0740e927a120a7d64820d6ae0626dadd9715b6d9b78b685d391a["id"] ?? null) : null), (($__internal_62dd66dc46ba059ed877028590f47326fb4eecb7864459b5e0ea4616c00cec6f = ($context["selected"] ?? null)) && is_array($__internal_62dd66dc46ba059ed877028590f47326fb4eecb7864459b5e0ea4616c00cec6f) || $__internal_62dd66dc46ba059ed877028590f47326fb4eecb7864459b5e0ea4616c00cec6f instanceof ArrayAccess ? ($__internal_62dd66dc46ba059ed877028590f47326fb4eecb7864459b5e0ea4616c00cec6f[($context["groupUID"] ?? null)] ?? null) : null)));
                                // line 244
                                echo "                                                    <option value=\"";
                                echo (($__internal_1115d809b62c821caac3cea1eb5c12c7d5d7b0706ee2f9a2083d0622a3c9c39d = $context["value"]) && is_array($__internal_1115d809b62c821caac3cea1eb5c12c7d5d7b0706ee2f9a2083d0622a3c9c39d) || $__internal_1115d809b62c821caac3cea1eb5c12c7d5d7b0706ee2f9a2083d0622a3c9c39d instanceof ArrayAccess ? ($__internal_1115d809b62c821caac3cea1eb5c12c7d5d7b0706ee2f9a2083d0622a3c9c39d["id"] ?? null) : null);
                                echo "\" class=\"bf-attr-val\" ";
                                if (($context["isSelected"] ?? null)) {
                                    echo " selected=\"true\" ";
                                }
                                // line 245
                                echo "                                                        ";
                                if (((($context["totals"] ?? null) &&  !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 245), (($__internal_a03dccad40e2abc48e619918035c6639141684812cc4b646aa7da29f3910460e = $context["value"]) && is_array($__internal_a03dccad40e2abc48e619918035c6639141684812cc4b646aa7da29f3910460e) || $__internal_a03dccad40e2abc48e619918035c6639141684812cc4b646aa7da29f3910460e instanceof ArrayAccess ? ($__internal_a03dccad40e2abc48e619918035c6639141684812cc4b646aa7da29f3910460e["id"] ?? null) : null), [], "array", true, true, false, 245)) &&  !($context["isSelected"] ?? null))) {
                                    // line 246
                                    echo "                                                            disabled=\"disabled\"
                                                        ";
                                }
                                // line 248
                                echo "                                                        ";
                                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 248), (($__internal_6dde4d73810bf2ec655aae3765c099752155acd96383d2a8c4a895d4dd805f81 = $context["value"]) && is_array($__internal_6dde4d73810bf2ec655aae3765c099752155acd96383d2a8c4a895d4dd805f81) || $__internal_6dde4d73810bf2ec655aae3765c099752155acd96383d2a8c4a895d4dd805f81 instanceof ArrayAccess ? ($__internal_6dde4d73810bf2ec655aae3765c099752155acd96383d2a8c4a895d4dd805f81["id"] ?? null) : null), [], "array", true, true, false, 248) &&  !($context["isSelected"] ?? null))) {
                                    // line 249
                                    echo "                                                            data-totals=\"";
                                    echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 249), (($__internal_6a445f9d1af686ab12d6899f24bc3d7a6b02a4ac19e96d6949d37618ee93e47d = $context["value"]) && is_array($__internal_6a445f9d1af686ab12d6899f24bc3d7a6b02a4ac19e96d6949d37618ee93e47d) || $__internal_6a445f9d1af686ab12d6899f24bc3d7a6b02a4ac19e96d6949d37618ee93e47d instanceof ArrayAccess ? ($__internal_6a445f9d1af686ab12d6899f24bc3d7a6b02a4ac19e96d6949d37618ee93e47d["id"] ?? null) : null), [], "array", true, true, false, 249)) ? ((($__internal_3a9dc3313d7b7e5bd188b8c7855821892ed2495b7c38652392e95c46e3ce9786 = (($__internal_bc65b855468fc032933ffd32e86ec81770caf9414e1cc18f9eee5d4d0db749b4 = ($context["totals"] ?? null)) && is_array($__internal_bc65b855468fc032933ffd32e86ec81770caf9414e1cc18f9eee5d4d0db749b4) || $__internal_bc65b855468fc032933ffd32e86ec81770caf9414e1cc18f9eee5d4d0db749b4 instanceof ArrayAccess ? ($__internal_bc65b855468fc032933ffd32e86ec81770caf9414e1cc18f9eee5d4d0db749b4[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_3a9dc3313d7b7e5bd188b8c7855821892ed2495b7c38652392e95c46e3ce9786) || $__internal_3a9dc3313d7b7e5bd188b8c7855821892ed2495b7c38652392e95c46e3ce9786 instanceof ArrayAccess ? ($__internal_3a9dc3313d7b7e5bd188b8c7855821892ed2495b7c38652392e95c46e3ce9786[(($__internal_7a655eb775f25f1b3b03ef58e648554ab0e45601d2d581ae0732daf6c847cfb1 = $context["value"]) && is_array($__internal_7a655eb775f25f1b3b03ef58e648554ab0e45601d2d581ae0732daf6c847cfb1) || $__internal_7a655eb775f25f1b3b03ef58e648554ab0e45601d2d581ae0732daf6c847cfb1 instanceof ArrayAccess ? ($__internal_7a655eb775f25f1b3b03ef58e648554ab0e45601d2d581ae0732daf6c847cfb1["id"] ?? null) : null)] ?? null) : null)) : (0));
                                    echo "\"
                                                        ";
                                }
                                // line 250
                                echo " >
                                                        ";
                                // line 251
                                echo (($__internal_63945d420e9576d39ff82caebc45bb65b343c5aee618f6515ae6b08a084a61cc = $context["value"]) && is_array($__internal_63945d420e9576d39ff82caebc45bb65b343c5aee618f6515ae6b08a084a61cc) || $__internal_63945d420e9576d39ff82caebc45bb65b343c5aee618f6515ae6b08a084a61cc instanceof ArrayAccess ? ($__internal_63945d420e9576d39ff82caebc45bb65b343c5aee618f6515ae6b08a084a61cc["name"] ?? null) : null);
                                echo "
                                                    </option>
                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 254
                            echo "                                            </select>
                                        </div>
                                    </div>
                                
                                ";
                        } elseif (twig_in_filter((($__internal_4e544dec457aecf1443a68c323cd82366dea8ab2422d0d98936729a61565d1b6 =                         // line 258
$context["group"]) && is_array($__internal_4e544dec457aecf1443a68c323cd82366dea8ab2422d0d98936729a61565d1b6) || $__internal_4e544dec457aecf1443a68c323cd82366dea8ab2422d0d98936729a61565d1b6 instanceof ArrayAccess ? ($__internal_4e544dec457aecf1443a68c323cd82366dea8ab2422d0d98936729a61565d1b6["type"] ?? null) : null), [0 => "slider", 1 => "slider_lbl", 2 => "slider_lbl_inp"])) {
                            // line 259
                            echo "                                
                                <div class=\"bf-attr-filter bf-attr-";
                            // line 260
                            echo ($context["groupUID"] ?? null);
                            echo " bf-row\">
                                    <div class=\"bf-cell\">
                                        <div class=\"bf-slider-inputs\">
                                            ";
                            // line 263
                            $context["isMinSet"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 263), "min", [], "array", true, true, false, 263);
                            // line 264
                            echo "                                            ";
                            $context["isMaxSet"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 264), "max", [], "array", true, true, false, 264);
                            // line 265
                            echo "                                            ";
                            $context["sliderType"] = ((((($__internal_14e21981e7dd3b7329ce23316b7dd9a4a72bcc8fc5fb4bbbe07d78b2f34ad181 = $context["group"]) && is_array($__internal_14e21981e7dd3b7329ce23316b7dd9a4a72bcc8fc5fb4bbbe07d78b2f34ad181) || $__internal_14e21981e7dd3b7329ce23316b7dd9a4a72bcc8fc5fb4bbbe07d78b2f34ad181 instanceof ArrayAccess ? ($__internal_14e21981e7dd3b7329ce23316b7dd9a4a72bcc8fc5fb4bbbe07d78b2f34ad181["type"] ?? null) : null) === "slider_lbl_inp")) ? (3) : (((((($__internal_6d6262ed1b3576eed98294422f6c92124e958065b890ec1daeda373cca95f0b9 = $context["group"]) && is_array($__internal_6d6262ed1b3576eed98294422f6c92124e958065b890ec1daeda373cca95f0b9) || $__internal_6d6262ed1b3576eed98294422f6c92124e958065b890ec1daeda373cca95f0b9 instanceof ArrayAccess ? ($__internal_6d6262ed1b3576eed98294422f6c92124e958065b890ec1daeda373cca95f0b9["type"] ?? null) : null) === "slider_lbl")) ? (2) : (1))));
                            // line 266
                            echo "                                            <input type=\"hidden\" name=\"bfp_min_";
                            echo ($context["groupUID"] ?? null);
                            echo "\" value=\"";
                            echo ((($context["isMinSet"] ?? null)) ? ((($__internal_48ca1eafe1c0dafac733bebe2a1efca21a74359cd77ffeb182a204b16ff0f3af = (($__internal_9bcd89fd04c29d4f14b269195c2b0c44b1319099d5fd519cf088920bf6dad4ab = ($context["selected"] ?? null)) && is_array($__internal_9bcd89fd04c29d4f14b269195c2b0c44b1319099d5fd519cf088920bf6dad4ab) || $__internal_9bcd89fd04c29d4f14b269195c2b0c44b1319099d5fd519cf088920bf6dad4ab instanceof ArrayAccess ? ($__internal_9bcd89fd04c29d4f14b269195c2b0c44b1319099d5fd519cf088920bf6dad4ab[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_48ca1eafe1c0dafac733bebe2a1efca21a74359cd77ffeb182a204b16ff0f3af) || $__internal_48ca1eafe1c0dafac733bebe2a1efca21a74359cd77ffeb182a204b16ff0f3af instanceof ArrayAccess ? ($__internal_48ca1eafe1c0dafac733bebe2a1efca21a74359cd77ffeb182a204b16ff0f3af["min"] ?? null) : null)) : ("na"));
                            echo "\" class=\"bf-attr-min-";
                            echo ($context["groupUID"] ?? null);
                            echo "\" data-min-limit=\"";
                            echo (($__internal_3271ebe347264d8653ac8af5aa4e0337dd15e365e271de0737121c8fe688ab94 = (($__internal_131d1ad83be0db5885bd8f4d04b7758f816558c99eb0966bc3747496b619d05c = $context["group"]) && is_array($__internal_131d1ad83be0db5885bd8f4d04b7758f816558c99eb0966bc3747496b619d05c) || $__internal_131d1ad83be0db5885bd8f4d04b7758f816558c99eb0966bc3747496b619d05c instanceof ArrayAccess ? ($__internal_131d1ad83be0db5885bd8f4d04b7758f816558c99eb0966bc3747496b619d05c["min"] ?? null) : null)) && is_array($__internal_3271ebe347264d8653ac8af5aa4e0337dd15e365e271de0737121c8fe688ab94) || $__internal_3271ebe347264d8653ac8af5aa4e0337dd15e365e271de0737121c8fe688ab94 instanceof ArrayAccess ? ($__internal_3271ebe347264d8653ac8af5aa4e0337dd15e365e271de0737121c8fe688ab94["s"] ?? null) : null);
                            echo "\" />
                                            <input type=\"hidden\" name=\"bfp_max_";
                            // line 267
                            echo ($context["groupUID"] ?? null);
                            echo "\" value=\"";
                            echo ((($context["isMaxSet"] ?? null)) ? ((($__internal_e9e5d2ee8212027481d34dfd74c8b902eddcf0dfdf2b80d975ad6713d36eb2cd = (($__internal_6addec62ce993bb3687298b96e3697631e0e1f3c95c8f74f5d5273b46adb96a0 = ($context["selected"] ?? null)) && is_array($__internal_6addec62ce993bb3687298b96e3697631e0e1f3c95c8f74f5d5273b46adb96a0) || $__internal_6addec62ce993bb3687298b96e3697631e0e1f3c95c8f74f5d5273b46adb96a0 instanceof ArrayAccess ? ($__internal_6addec62ce993bb3687298b96e3697631e0e1f3c95c8f74f5d5273b46adb96a0[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_e9e5d2ee8212027481d34dfd74c8b902eddcf0dfdf2b80d975ad6713d36eb2cd) || $__internal_e9e5d2ee8212027481d34dfd74c8b902eddcf0dfdf2b80d975ad6713d36eb2cd instanceof ArrayAccess ? ($__internal_e9e5d2ee8212027481d34dfd74c8b902eddcf0dfdf2b80d975ad6713d36eb2cd["max"] ?? null) : null)) : ("na"));
                            echo "\" class=\"bf-attr-max-";
                            echo ($context["groupUID"] ?? null);
                            echo "\" data-max-limit=\"";
                            echo (($__internal_a6fb56e832ab03437ac2beb121972e8e79ced038d8ae45afc18634fa0d2a7c6b = (($__internal_2b03fe367704beb3823f261fb2f100c491f011ea8771fa697b7c2d827599a070 = $context["group"]) && is_array($__internal_2b03fe367704beb3823f261fb2f100c491f011ea8771fa697b7c2d827599a070) || $__internal_2b03fe367704beb3823f261fb2f100c491f011ea8771fa697b7c2d827599a070 instanceof ArrayAccess ? ($__internal_2b03fe367704beb3823f261fb2f100c491f011ea8771fa697b7c2d827599a070["max"] ?? null) : null)) && is_array($__internal_a6fb56e832ab03437ac2beb121972e8e79ced038d8ae45afc18634fa0d2a7c6b) || $__internal_a6fb56e832ab03437ac2beb121972e8e79ced038d8ae45afc18634fa0d2a7c6b instanceof ArrayAccess ? ($__internal_a6fb56e832ab03437ac2beb121972e8e79ced038d8ae45afc18634fa0d2a7c6b["s"] ?? null) : null);
                            echo "\" /> 
                                            ";
                            // line 268
                            if (((($__internal_27724619532b81f70e47f787f2d6ba2642ad645b9809ec31c41a0fc9a021bccf = $context["group"]) && is_array($__internal_27724619532b81f70e47f787f2d6ba2642ad645b9809ec31c41a0fc9a021bccf) || $__internal_27724619532b81f70e47f787f2d6ba2642ad645b9809ec31c41a0fc9a021bccf instanceof ArrayAccess ? ($__internal_27724619532b81f70e47f787f2d6ba2642ad645b9809ec31c41a0fc9a021bccf["type"] ?? null) : null) != "slider_lbl")) {
                                // line 269
                                echo "                                                ";
                                $context["minLbl"] = "";
                                // line 270
                                echo "                                                ";
                                $context["maxLbl"] = "";
                                // line 271
                                echo "                                                ";
                                if (($context["isMinSet"] ?? null)) {
                                    // line 272
                                    echo "                                                    ";
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable((($__internal_07189bdf422a336bd30a149f561299354deaa7140cee435438135bbbf04aeb1e = $context["group"]) && is_array($__internal_07189bdf422a336bd30a149f561299354deaa7140cee435438135bbbf04aeb1e) || $__internal_07189bdf422a336bd30a149f561299354deaa7140cee435438135bbbf04aeb1e instanceof ArrayAccess ? ($__internal_07189bdf422a336bd30a149f561299354deaa7140cee435438135bbbf04aeb1e["values"] ?? null) : null));
                                    foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
                                        // line 273
                                        echo "                                                        ";
                                        if (((($__internal_3aa537a936bb92157cefd8aa9fc620062d138d41031375eea95479ba2781b1b4 = $context["v"]) && is_array($__internal_3aa537a936bb92157cefd8aa9fc620062d138d41031375eea95479ba2781b1b4) || $__internal_3aa537a936bb92157cefd8aa9fc620062d138d41031375eea95479ba2781b1b4 instanceof ArrayAccess ? ($__internal_3aa537a936bb92157cefd8aa9fc620062d138d41031375eea95479ba2781b1b4["s"] ?? null) : null) == (($__internal_60c3b69b495786976923eb2f532157de97aa5c29dc9513da8198a9ceace81825 = (($__internal_0d30daf524bd3b24650f37db0a943c64fc1caa5ef72ffa19edd4e0046f222308 = ($context["selected"] ?? null)) && is_array($__internal_0d30daf524bd3b24650f37db0a943c64fc1caa5ef72ffa19edd4e0046f222308) || $__internal_0d30daf524bd3b24650f37db0a943c64fc1caa5ef72ffa19edd4e0046f222308 instanceof ArrayAccess ? ($__internal_0d30daf524bd3b24650f37db0a943c64fc1caa5ef72ffa19edd4e0046f222308[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_60c3b69b495786976923eb2f532157de97aa5c29dc9513da8198a9ceace81825) || $__internal_60c3b69b495786976923eb2f532157de97aa5c29dc9513da8198a9ceace81825 instanceof ArrayAccess ? ($__internal_60c3b69b495786976923eb2f532157de97aa5c29dc9513da8198a9ceace81825["min"] ?? null) : null))) {
                                            // line 274
                                            echo "                                                            ";
                                            $context["minLbl"] = (($__internal_6ffb32c3e396e273bf59227a1929f61725bd66b1599c95be2fe97e57076204eb = $context["v"]) && is_array($__internal_6ffb32c3e396e273bf59227a1929f61725bd66b1599c95be2fe97e57076204eb) || $__internal_6ffb32c3e396e273bf59227a1929f61725bd66b1599c95be2fe97e57076204eb instanceof ArrayAccess ? ($__internal_6ffb32c3e396e273bf59227a1929f61725bd66b1599c95be2fe97e57076204eb["n"] ?? null) : null);
                                            // line 275
                                            echo "                                                            ";
                                            echo ($context["break"] ?? null);
                                            echo "
                                                        ";
                                        }
                                        // line 277
                                        echo "                                                    ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 278
                                    echo "                                                ";
                                }
                                // line 279
                                echo "                                                ";
                                if (($context["isMaxSet"] ?? null)) {
                                    // line 280
                                    echo "                                                    ";
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable((($__internal_02e37451be1bc90685e7b07dc185b5bf55030db7e6476dc7784f39b57707e6d7 = $context["group"]) && is_array($__internal_02e37451be1bc90685e7b07dc185b5bf55030db7e6476dc7784f39b57707e6d7) || $__internal_02e37451be1bc90685e7b07dc185b5bf55030db7e6476dc7784f39b57707e6d7 instanceof ArrayAccess ? ($__internal_02e37451be1bc90685e7b07dc185b5bf55030db7e6476dc7784f39b57707e6d7["values"] ?? null) : null));
                                    foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
                                        // line 281
                                        echo "                                                        ";
                                        if (((($__internal_cfb39a2b011d1b5ceede989c48296b7be75cc4628ed22caaaa73f6a0511885d9 = $context["v"]) && is_array($__internal_cfb39a2b011d1b5ceede989c48296b7be75cc4628ed22caaaa73f6a0511885d9) || $__internal_cfb39a2b011d1b5ceede989c48296b7be75cc4628ed22caaaa73f6a0511885d9 instanceof ArrayAccess ? ($__internal_cfb39a2b011d1b5ceede989c48296b7be75cc4628ed22caaaa73f6a0511885d9["s"] ?? null) : null) == (($__internal_5dc29f10273e1b11a87047decc4959e4d24ebef705bf72ebd09d61c5983e7446 = (($__internal_6cce074a3e1015eb981f766905cbab506776514bac4c77d44f3ced836a67675d = ($context["selected"] ?? null)) && is_array($__internal_6cce074a3e1015eb981f766905cbab506776514bac4c77d44f3ced836a67675d) || $__internal_6cce074a3e1015eb981f766905cbab506776514bac4c77d44f3ced836a67675d instanceof ArrayAccess ? ($__internal_6cce074a3e1015eb981f766905cbab506776514bac4c77d44f3ced836a67675d[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_5dc29f10273e1b11a87047decc4959e4d24ebef705bf72ebd09d61c5983e7446) || $__internal_5dc29f10273e1b11a87047decc4959e4d24ebef705bf72ebd09d61c5983e7446 instanceof ArrayAccess ? ($__internal_5dc29f10273e1b11a87047decc4959e4d24ebef705bf72ebd09d61c5983e7446["max"] ?? null) : null))) {
                                            // line 282
                                            echo "                                                            ";
                                            $context["maxLbl"] = (($__internal_cb311c56761b7e1fef0833ade7151100fb45c3bb35a619e658ba3e7a5752a59e = $context["v"]) && is_array($__internal_cb311c56761b7e1fef0833ade7151100fb45c3bb35a619e658ba3e7a5752a59e) || $__internal_cb311c56761b7e1fef0833ade7151100fb45c3bb35a619e658ba3e7a5752a59e instanceof ArrayAccess ? ($__internal_cb311c56761b7e1fef0833ade7151100fb45c3bb35a619e658ba3e7a5752a59e["n"] ?? null) : null);
                                            // line 283
                                            echo "                                                            ";
                                            echo ($context["break"] ?? null);
                                            echo "
                                                        ";
                                        }
                                        // line 285
                                        echo "                                                    ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 286
                                    echo "                                                ";
                                }
                                // line 287
                                echo "                                            <input type=\"text\" name=\"\" class=\"bf-slider-text-inp-min bf-slider-input\" value=\"";
                                echo ($context["minLbl"] ?? null);
                                echo "\" placeholder=\"";
                                echo ($context["lang_empty_slider"] ?? null);
                                echo "\" />
                                            <span class=\"ndash\">&#8211;</span>
                                            <input type=\"text\" name=\"\" class=\"bf-slider-text-inp-max bf-slider-input\" value=\"";
                                // line 289
                                echo ($context["maxLbl"] ?? null);
                                echo "\" placeholder=\"";
                                echo ($context["lang_empty_slider"] ?? null);
                                echo "\" />
                                            ";
                            }
                            // line 291
                            echo "                                        </div>
                                        <div class=\"bf-slider-container-wrapper ";
                            // line 292
                            if (((($context["sliderType"] ?? null) === 2) || (($context["sliderType"] ?? null) === 3))) {
                                echo " bf-slider-with-labels ";
                            }
                            echo "\">
                                            <div class=\"bf-slider-container\" data-slider-group=\"";
                            // line 293
                            echo ($context["groupUID"] ?? null);
                            echo "\" data-slider-type=\"";
                            echo ($context["sliderType"] ?? null);
                            echo "\"></div>
                                        </div>
                                    </div>  
                                </div>
                                
                                ";
                        } elseif (((($__internal_74a3e8c2570a3732273c3ed74a6d3c8e042e35fc985e59c62d261815e380ee4f =                         // line 298
$context["group"]) && is_array($__internal_74a3e8c2570a3732273c3ed74a6d3c8e042e35fc985e59c62d261815e380ee4f) || $__internal_74a3e8c2570a3732273c3ed74a6d3c8e042e35fc985e59c62d261815e380ee4f instanceof ArrayAccess ? ($__internal_74a3e8c2570a3732273c3ed74a6d3c8e042e35fc985e59c62d261815e380ee4f["type"] ?? null) : null) === "grid")) {
                            // line 299
                            echo "                                
                                <div class=\"bf-attr-filter bf-attr-";
                            // line 300
                            echo ($context["groupUID"] ?? null);
                            echo " bf-row\">
                                    <div class=\"bf-grid\">
                                        ";
                            // line 302
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable((($__internal_e4c924f92b5a931b7f689155f599dbb26e4de7e6a4b41afc2c796c48ae5b4996 = $context["group"]) && is_array($__internal_e4c924f92b5a931b7f689155f599dbb26e4de7e6a4b41afc2c796c48ae5b4996) || $__internal_e4c924f92b5a931b7f689155f599dbb26e4de7e6a4b41afc2c796c48ae5b4996 instanceof ArrayAccess ? ($__internal_e4c924f92b5a931b7f689155f599dbb26e4de7e6a4b41afc2c796c48ae5b4996["values"] ?? null) : null));
                            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                                // line 303
                                echo "                                            ";
                                $context["valueId"] = (($__internal_3d5c46d4c5bb0cede8218ba97273f45d5f9ce5a263c36c0e2decf8c65d457330 = $context["value"]) && is_array($__internal_3d5c46d4c5bb0cede8218ba97273f45d5f9ce5a263c36c0e2decf8c65d457330) || $__internal_3d5c46d4c5bb0cede8218ba97273f45d5f9ce5a263c36c0e2decf8c65d457330 instanceof ArrayAccess ? ($__internal_3d5c46d4c5bb0cede8218ba97273f45d5f9ce5a263c36c0e2decf8c65d457330["id"] ?? null) : null);
                                // line 304
                                echo "                                        <div class=\"bf-grid-item\">
                                            <input id=\"bf-attr-";
                                // line 305
                                echo ((((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                                echo "\" class=\"bf-hidden\"
                                                    data-filterid=\"bf-attr-";
                                // line 306
                                echo ((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null));
                                echo "\"
                                                    type=\"radio\" 
                                                    name=\"\"bfp_";
                                // line 308
                                echo ($context["groupUID"] ?? null);
                                echo "\"
                                                    value=\"";
                                // line 309
                                echo ($context["valueId"] ?? null);
                                echo "\" 
                                                    ";
                                // line 310
                                if ((twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 310) && twig_in_filter(($context["valueId"] ?? null), (($__internal_ce652b03c8acd7b858d9c93e7f44511cc72e40ab64d98a8e15d94d28735197ae = ($context["selected"] ?? null)) && is_array($__internal_ce652b03c8acd7b858d9c93e7f44511cc72e40ab64d98a8e15d94d28735197ae) || $__internal_ce652b03c8acd7b858d9c93e7f44511cc72e40ab64d98a8e15d94d28735197ae instanceof ArrayAccess ? ($__internal_ce652b03c8acd7b858d9c93e7f44511cc72e40ab64d98a8e15d94d28735197ae[($context["groupUID"] ?? null)] ?? null) : null)))) {
                                    echo " checked=\"true\" ";
                                }
                                echo " />
                                            <label for=\"bf-attr-";
                                // line 311
                                echo ((((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                                echo "\">
                                                <img src=\"image/";
                                // line 312
                                echo (($__internal_8e4986aabe98b063bb5d5a2c95610ac751695fced15cdacfec53f70e7d4ea409 = $context["value"]) && is_array($__internal_8e4986aabe98b063bb5d5a2c95610ac751695fced15cdacfec53f70e7d4ea409) || $__internal_8e4986aabe98b063bb5d5a2c95610ac751695fced15cdacfec53f70e7d4ea409 instanceof ArrayAccess ? ($__internal_8e4986aabe98b063bb5d5a2c95610ac751695fced15cdacfec53f70e7d4ea409["image"] ?? null) : null);
                                echo "\" alt=\"";
                                echo (($__internal_e858097da87330889509c21892ee85ee8c960bfd805aa0f525a9610eda0b6c26 = $context["value"]) && is_array($__internal_e858097da87330889509c21892ee85ee8c960bfd805aa0f525a9610eda0b6c26) || $__internal_e858097da87330889509c21892ee85ee8c960bfd805aa0f525a9610eda0b6c26 instanceof ArrayAccess ? ($__internal_e858097da87330889509c21892ee85ee8c960bfd805aa0f525a9610eda0b6c26["name"] ?? null) : null);
                                echo "\" />
                                            </label>
                                            <span class=\"bf-hidden bf-attr-val\">";
                                // line 314
                                echo ($context["valueId"] ?? null);
                                echo "</span>
                                        </div>
                                        ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 317
                            echo "                                    </div>
                                </div>
                                
                                ";
                        } else {
                            // line 321
                            echo "                                
                                    ";
                            // line 322
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable((($__internal_65e7e4520674d98f13dd3aeeb92cde937cf6924ee55b1cd714dab21b56e90b8d = $context["group"]) && is_array($__internal_65e7e4520674d98f13dd3aeeb92cde937cf6924ee55b1cd714dab21b56e90b8d) || $__internal_65e7e4520674d98f13dd3aeeb92cde937cf6924ee55b1cd714dab21b56e90b8d instanceof ArrayAccess ? ($__internal_65e7e4520674d98f13dd3aeeb92cde937cf6924ee55b1cd714dab21b56e90b8d["values"] ?? null) : null));
                            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                                // line 323
                                echo "                                        ";
                                $context["valueId"] = (($__internal_f4b318c9f161980c086cd791a68c6b3948e695aff50a75fb3267d44f228c7408 = $context["value"]) && is_array($__internal_f4b318c9f161980c086cd791a68c6b3948e695aff50a75fb3267d44f228c7408) || $__internal_f4b318c9f161980c086cd791a68c6b3948e695aff50a75fb3267d44f228c7408 instanceof ArrayAccess ? ($__internal_f4b318c9f161980c086cd791a68c6b3948e695aff50a75fb3267d44f228c7408["id"] ?? null) : null);
                                // line 324
                                echo "                                    <div class=\"bf-attr-filter bf-attr-";
                                echo ($context["groupUID"] ?? null);
                                echo " bf-row
                                        ";
                                // line 325
                                if ((array_key_exists("totals", $context) && (($__internal_83fc86d2371dda9aa0b3b082e73f39d0e9a5bb564db226a358e9a4a4194ea0c0 = (($__internal_c254c72d5963764194d309f18e90f4170c597a27593b6d70e5664f7497676f7b = ($context["settings"] ?? null)) && is_array($__internal_c254c72d5963764194d309f18e90f4170c597a27593b6d70e5664f7497676f7b) || $__internal_c254c72d5963764194d309f18e90f4170c597a27593b6d70e5664f7497676f7b instanceof ArrayAccess ? ($__internal_c254c72d5963764194d309f18e90f4170c597a27593b6d70e5664f7497676f7b["behaviour"] ?? null) : null)) && is_array($__internal_83fc86d2371dda9aa0b3b082e73f39d0e9a5bb564db226a358e9a4a4194ea0c0) || $__internal_83fc86d2371dda9aa0b3b082e73f39d0e9a5bb564db226a358e9a4a4194ea0c0 instanceof ArrayAccess ? ($__internal_83fc86d2371dda9aa0b3b082e73f39d0e9a5bb564db226a358e9a4a4194ea0c0["hide_empty"] ?? null) : null))) {
                                    // line 326
                                    echo "                                            ";
                                    $context["inStock"] = (($context["postponedCount"] ?? null) || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 326), ($context["valueId"] ?? null), [], "array", true, true, false, 326) && (($__internal_dc32df57a76148af0ad93ed5fa8b904808a8f1945335b4257f8bf2afc20e0084 = (($__internal_0b241b0e70009b9c2e73ff26e0e7a21de5c494898ed5e481203a038ce17059cb = ($context["totals"] ?? null)) && is_array($__internal_0b241b0e70009b9c2e73ff26e0e7a21de5c494898ed5e481203a038ce17059cb) || $__internal_0b241b0e70009b9c2e73ff26e0e7a21de5c494898ed5e481203a038ce17059cb instanceof ArrayAccess ? ($__internal_0b241b0e70009b9c2e73ff26e0e7a21de5c494898ed5e481203a038ce17059cb[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_dc32df57a76148af0ad93ed5fa8b904808a8f1945335b4257f8bf2afc20e0084) || $__internal_dc32df57a76148af0ad93ed5fa8b904808a8f1945335b4257f8bf2afc20e0084 instanceof ArrayAccess ? ($__internal_dc32df57a76148af0ad93ed5fa8b904808a8f1945335b4257f8bf2afc20e0084[($context["valueId"] ?? null)] ?? null) : null)));
                                    // line 327
                                    echo "                                            ";
                                    $context["inSelected"] = (twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 327) && twig_in_filter(($context["valueId"] ?? null), (($__internal_ef6a922f76b02213de0c05cd958ab517168c2607c5c5040d9267fac459d99a6b = ($context["selected"] ?? null)) && is_array($__internal_ef6a922f76b02213de0c05cd958ab517168c2607c5c5040d9267fac459d99a6b) || $__internal_ef6a922f76b02213de0c05cd958ab517168c2607c5c5040d9267fac459d99a6b instanceof ArrayAccess ? ($__internal_ef6a922f76b02213de0c05cd958ab517168c2607c5c5040d9267fac459d99a6b[($context["groupUID"] ?? null)] ?? null) : null)));
                                    // line 328
                                    echo "                                            ";
                                    if (( !($context["inStock"] ?? null) &&  !($context["inSelected"] ?? null))) {
                                        // line 329
                                        echo "                                                bf-disabled
                                            ";
                                    }
                                    // line 331
                                    echo "                                        ";
                                }
                                echo "\">
                                        <span class=\"bf-cell bf-c-1\">
                                            <input id=\"bf-attr-";
                                // line 333
                                echo ((((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                                echo "\"
                                                   data-filterid=\"bf-attr-";
                                // line 334
                                echo ((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null));
                                echo "\"
                                                   type=\"";
                                // line 335
                                echo (($__internal_ff3c01cf8a16468f6290b067ec94792a07f91c1ce87051d4f8c20747a7f040c9 = $context["group"]) && is_array($__internal_ff3c01cf8a16468f6290b067ec94792a07f91c1ce87051d4f8c20747a7f040c9) || $__internal_ff3c01cf8a16468f6290b067ec94792a07f91c1ce87051d4f8c20747a7f040c9 instanceof ArrayAccess ? ($__internal_ff3c01cf8a16468f6290b067ec94792a07f91c1ce87051d4f8c20747a7f040c9["type"] ?? null) : null);
                                echo "\" 
                                                   name=\"bfp_";
                                // line 336
                                echo ($context["groupUID"] ?? null);
                                if (((($__internal_fe481f7d149bb1599128a3bbfcac96d70f30ec5ba8af5b1ecaf5fbb191254698 = $context["group"]) && is_array($__internal_fe481f7d149bb1599128a3bbfcac96d70f30ec5ba8af5b1ecaf5fbb191254698) || $__internal_fe481f7d149bb1599128a3bbfcac96d70f30ec5ba8af5b1ecaf5fbb191254698 instanceof ArrayAccess ? ($__internal_fe481f7d149bb1599128a3bbfcac96d70f30ec5ba8af5b1ecaf5fbb191254698["type"] ?? null) : null) === "checkbox")) {
                                    echo ("_" . ($context["valueId"] ?? null));
                                    echo " ";
                                }
                                echo "\"
                                                   value=\"";
                                // line 337
                                echo ($context["valueId"] ?? null);
                                echo "\" 
                                                   ";
                                // line 338
                                if ((twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 338) && twig_in_filter(($context["valueId"] ?? null), (($__internal_92fcb416502b1fa6b6aa86d134b215b0a0167587977f219666cd2b1758225f4f = ($context["selected"] ?? null)) && is_array($__internal_92fcb416502b1fa6b6aa86d134b215b0a0167587977f219666cd2b1758225f4f) || $__internal_92fcb416502b1fa6b6aa86d134b215b0a0167587977f219666cd2b1758225f4f instanceof ArrayAccess ? ($__internal_92fcb416502b1fa6b6aa86d134b215b0a0167587977f219666cd2b1758225f4f[($context["groupUID"] ?? null)] ?? null) : null)))) {
                                    echo " checked=\"true\" ";
                                }
                                echo " />
                                        </span>
                                        <span class=\"bf-cell bf-c-2 ";
                                // line 340
                                if (((($__internal_3fa297d61bb53522f6d7d26865df531fe2c9cae5868ad00df3d7a18c25fe92a1 = $context["section"]) && is_array($__internal_3fa297d61bb53522f6d7d26865df531fe2c9cae5868ad00df3d7a18c25fe92a1) || $__internal_3fa297d61bb53522f6d7d26865df531fe2c9cae5868ad00df3d7a18c25fe92a1 instanceof ArrayAccess ? ($__internal_3fa297d61bb53522f6d7d26865df531fe2c9cae5868ad00df3d7a18c25fe92a1["type"] ?? null) : null) == "rating")) {
                                    echo " ";
                                    echo ("bf-rating-" . ($context["valueId"] ?? null));
                                    echo " ";
                                }
                                echo "\">
                                            <span class=\"bf-hidden bf-attr-val\">";
                                // line 341
                                echo ($context["valueId"] ?? null);
                                echo "</span>
                                            <label for=\"bf-attr-";
                                // line 342
                                echo ((((($context["groupUID"] ?? null) . "_") . ($context["valueId"] ?? null)) . "_") . ($context["layout_id"] ?? null));
                                echo "\">
                                                ";
                                // line 343
                                if (((($__internal_333cfa7af4c7ecc6093fcfedc8157b964c096bbb3ba6a10d8cc88e54d983911a = $context["section"]) && is_array($__internal_333cfa7af4c7ecc6093fcfedc8157b964c096bbb3ba6a10d8cc88e54d983911a) || $__internal_333cfa7af4c7ecc6093fcfedc8157b964c096bbb3ba6a10d8cc88e54d983911a instanceof ArrayAccess ? ($__internal_333cfa7af4c7ecc6093fcfedc8157b964c096bbb3ba6a10d8cc88e54d983911a["type"] ?? null) : null) === "option")) {
                                    // line 344
                                    echo "                                                    ";
                                    if ((((($__internal_6391199dedb1e4756012dc154b4a82c23dc574efa73d05e7c8a74a972a0cd12c = $context["group"]) && is_array($__internal_6391199dedb1e4756012dc154b4a82c23dc574efa73d05e7c8a74a972a0cd12c) || $__internal_6391199dedb1e4756012dc154b4a82c23dc574efa73d05e7c8a74a972a0cd12c instanceof ArrayAccess ? ($__internal_6391199dedb1e4756012dc154b4a82c23dc574efa73d05e7c8a74a972a0cd12c["mode"] ?? null) : null) === "img") || ((($__internal_749c2c9fadf4e0ad0ec949be406986f08318112ea607adc63270dc234ee0a8d7 = $context["group"]) && is_array($__internal_749c2c9fadf4e0ad0ec949be406986f08318112ea607adc63270dc234ee0a8d7) || $__internal_749c2c9fadf4e0ad0ec949be406986f08318112ea607adc63270dc234ee0a8d7 instanceof ArrayAccess ? ($__internal_749c2c9fadf4e0ad0ec949be406986f08318112ea607adc63270dc234ee0a8d7["mode"] ?? null) : null) === "img_label"))) {
                                        // line 345
                                        echo "                                                        <img src=\"image/";
                                        echo (($__internal_60b5864c736e546c33a0f9b12abb5977adf552ba2293b4dc12dad0a2be607351 = $context["value"]) && is_array($__internal_60b5864c736e546c33a0f9b12abb5977adf552ba2293b4dc12dad0a2be607351) || $__internal_60b5864c736e546c33a0f9b12abb5977adf552ba2293b4dc12dad0a2be607351 instanceof ArrayAccess ? ($__internal_60b5864c736e546c33a0f9b12abb5977adf552ba2293b4dc12dad0a2be607351["image"] ?? null) : null);
                                        echo "\" alt=\"";
                                        echo (($__internal_a2f5b9ff51b59bc9f3b025aa1e1c1e1b0fc91f36187c566a410229bf83ca6d15 = $context["value"]) && is_array($__internal_a2f5b9ff51b59bc9f3b025aa1e1c1e1b0fc91f36187c566a410229bf83ca6d15) || $__internal_a2f5b9ff51b59bc9f3b025aa1e1c1e1b0fc91f36187c566a410229bf83ca6d15 instanceof ArrayAccess ? ($__internal_a2f5b9ff51b59bc9f3b025aa1e1c1e1b0fc91f36187c566a410229bf83ca6d15["name"] ?? null) : null);
                                        echo "\" />
                                                    ";
                                    }
                                    // line 347
                                    echo "                                                    ";
                                    if ((((($__internal_e321fd256ed911201cceb1f23ee74e2ae71106dc9d38b9aa9d3ae1c4726dc46f = $context["group"]) && is_array($__internal_e321fd256ed911201cceb1f23ee74e2ae71106dc9d38b9aa9d3ae1c4726dc46f) || $__internal_e321fd256ed911201cceb1f23ee74e2ae71106dc9d38b9aa9d3ae1c4726dc46f instanceof ArrayAccess ? ($__internal_e321fd256ed911201cceb1f23ee74e2ae71106dc9d38b9aa9d3ae1c4726dc46f["mode"] ?? null) : null) === "label") || ((($__internal_7a86decbee0e83646d077b28cb4023bf5ce8a6941db44d2a095f3e5c30673f40 = $context["group"]) && is_array($__internal_7a86decbee0e83646d077b28cb4023bf5ce8a6941db44d2a095f3e5c30673f40) || $__internal_7a86decbee0e83646d077b28cb4023bf5ce8a6941db44d2a095f3e5c30673f40 instanceof ArrayAccess ? ($__internal_7a86decbee0e83646d077b28cb4023bf5ce8a6941db44d2a095f3e5c30673f40["mode"] ?? null) : null) === "img_label"))) {
                                        // line 348
                                        echo "                                                        ";
                                        echo (($__internal_9765bc41907280d6fcc28ada485cfb51e7ac5fc636224af251b78cc914882ea8 = $context["value"]) && is_array($__internal_9765bc41907280d6fcc28ada485cfb51e7ac5fc636224af251b78cc914882ea8) || $__internal_9765bc41907280d6fcc28ada485cfb51e7ac5fc636224af251b78cc914882ea8 instanceof ArrayAccess ? ($__internal_9765bc41907280d6fcc28ada485cfb51e7ac5fc636224af251b78cc914882ea8["name"] ?? null) : null);
                                        echo "
                                                    ";
                                    }
                                    // line 350
                                    echo "                                                ";
                                } else {
                                    // line 351
                                    echo "                                                    ";
                                    echo (($__internal_64a8779ac6c861d53bff0e05418b6bf70fe527205a28bcc347da1c47628d6316 = $context["value"]) && is_array($__internal_64a8779ac6c861d53bff0e05418b6bf70fe527205a28bcc347da1c47628d6316) || $__internal_64a8779ac6c861d53bff0e05418b6bf70fe527205a28bcc347da1c47628d6316 instanceof ArrayAccess ? ($__internal_64a8779ac6c861d53bff0e05418b6bf70fe527205a28bcc347da1c47628d6316["name"] ?? null) : null);
                                    echo "
                                                ";
                                }
                                // line 353
                                echo "                                            </label>
                                        </span>
                                        <span class=\"bf-cell bf-c-3\">
                                            ";
                                // line 356
                                if (array_key_exists("totals", $context)) {
                                    // line 357
                                    echo "                                                ";
                                    if (array_key_exists("totals", $context)) {
                                        // line 358
                                        echo "                                                    ";
                                        if (( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 358), ($context["valueId"] ?? null), [], "array", true, true, false, 358) &&  !twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 358))) {
                                            // line 359
                                            echo "                                                        ";
                                            echo "";
                                            echo "
                                                    ";
                                        } else {
                                            // line 361
                                            echo "                                                        ";
                                            $context["total"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["totals"] ?? null), ($context["groupUID"] ?? null), [], "array", false, true, false, 361), ($context["valueId"] ?? null), [], "array", true, true, false, 361)) ? ((($__internal_bdeae13b61c48a9bb7053d83339903da184f04d6cf2c1c6bf65ecb00a4978b68 = (($__internal_33af9458a3ae54268b64fc86c4f2aabeb171e5df6853a739a882d22db3c7a6a3 = ($context["totals"] ?? null)) && is_array($__internal_33af9458a3ae54268b64fc86c4f2aabeb171e5df6853a739a882d22db3c7a6a3) || $__internal_33af9458a3ae54268b64fc86c4f2aabeb171e5df6853a739a882d22db3c7a6a3 instanceof ArrayAccess ? ($__internal_33af9458a3ae54268b64fc86c4f2aabeb171e5df6853a739a882d22db3c7a6a3[($context["groupUID"] ?? null)] ?? null) : null)) && is_array($__internal_bdeae13b61c48a9bb7053d83339903da184f04d6cf2c1c6bf65ecb00a4978b68) || $__internal_bdeae13b61c48a9bb7053d83339903da184f04d6cf2c1c6bf65ecb00a4978b68 instanceof ArrayAccess ? ($__internal_bdeae13b61c48a9bb7053d83339903da184f04d6cf2c1c6bf65ecb00a4978b68[($context["valueId"] ?? null)] ?? null) : null)) : (0));
                                            // line 362
                                            echo "                                                        ";
                                            $context["addPlusSign"] = twig_get_attribute($this->env, $this->source, ($context["selected"] ?? null), ($context["groupUID"] ?? null), [], "array", true, true, false, 362);
                                            // line 363
                                            echo "                                                        <span class=\"bf-count ";
                                            echo (( !($context["total"] ?? null)) ? ("bf-empty") : (""));
                                            echo "\">";
                                            echo ((($context["addPlusSign"] ?? null)) ? ("+") : (""));
                                            echo ($context["total"] ?? null);
                                            echo "</span>
                                                    ";
                                        }
                                        // line 365
                                        echo "                                                ";
                                    }
                                    // line 366
                                    echo "                                            ";
                                }
                                // line 367
                                echo "                                        </span>
                                    </div>
                                    ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 370
                            echo "                                ";
                        }
                        // line 371
                        echo "                            </div>
                            </div>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['groupId'], $context['group'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 374
                    echo "                    ";
                }
                // line 375
                echo "                    
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 377
            echo "                ";
            if (( !($context["isHorizontal"] ?? null) || ((($__internal_8a0762d4aa3dc8c3807c671517580f54686cc5ac5c8e1cef30934bfb6f7bb267 = (($__internal_ffa8d8104e4c05f0e520b22e02d102606b28cadd1d676232462d37155205d29d = ($context["settings"] ?? null)) && is_array($__internal_ffa8d8104e4c05f0e520b22e02d102606b28cadd1d676232462d37155205d29d) || $__internal_ffa8d8104e4c05f0e520b22e02d102606b28cadd1d676232462d37155205d29d instanceof ArrayAccess ? ($__internal_ffa8d8104e4c05f0e520b22e02d102606b28cadd1d676232462d37155205d29d["submission"] ?? null) : null)) && is_array($__internal_8a0762d4aa3dc8c3807c671517580f54686cc5ac5c8e1cef30934bfb6f7bb267) || $__internal_8a0762d4aa3dc8c3807c671517580f54686cc5ac5c8e1cef30934bfb6f7bb267 instanceof ArrayAccess ? ($__internal_8a0762d4aa3dc8c3807c671517580f54686cc5ac5c8e1cef30934bfb6f7bb267["submit_type"] ?? null) : null) == "button"))) {
                echo " <div class=\"bf-buttonclear-box\"";
                if ((($context["isHorizontal"] ?? null) && ((($__internal_995009f84643f33c31b1bfeabfe54d442df1ec39134811b9fee93a3a100dc10f = (($__internal_a87a332bdd9c4920fcff67e0a0fbf2338bf0fc502dc18935fe14cb0266f92768 = ($context["settings"] ?? null)) && is_array($__internal_a87a332bdd9c4920fcff67e0a0fbf2338bf0fc502dc18935fe14cb0266f92768) || $__internal_a87a332bdd9c4920fcff67e0a0fbf2338bf0fc502dc18935fe14cb0266f92768 instanceof ArrayAccess ? ($__internal_a87a332bdd9c4920fcff67e0a0fbf2338bf0fc502dc18935fe14cb0266f92768["submission"] ?? null) : null)) && is_array($__internal_995009f84643f33c31b1bfeabfe54d442df1ec39134811b9fee93a3a100dc10f) || $__internal_995009f84643f33c31b1bfeabfe54d442df1ec39134811b9fee93a3a100dc10f instanceof ArrayAccess ? ($__internal_995009f84643f33c31b1bfeabfe54d442df1ec39134811b9fee93a3a100dc10f["submit_button_type"] ?? null) : null) == "float"))) {
                    echo " style=\"display:none;\" ";
                }
                echo ">
                         <input type=\"button\" value=\"";
                // line 378
                echo ($context["lang_submit"] ?? null);
                echo "\" class=\"btn btn-primary bf-buttonsubmit\" onclick=\"BrainyFilter.sendRequest(jQuery(this));BrainyFilter.loadingAnimation();return false;\" ";
                if ((((($__internal_dc4207045c42a84741452c486f47b9a66b720393ce7199ed6472faf87641355a = (($__internal_70cdb879b02cf9ec1d6611c4992631759ee2013ffb59f0362533ea6cd4a4af18 = ($context["settings"] ?? null)) && is_array($__internal_70cdb879b02cf9ec1d6611c4992631759ee2013ffb59f0362533ea6cd4a4af18) || $__internal_70cdb879b02cf9ec1d6611c4992631759ee2013ffb59f0362533ea6cd4a4af18 instanceof ArrayAccess ? ($__internal_70cdb879b02cf9ec1d6611c4992631759ee2013ffb59f0362533ea6cd4a4af18["submission"] ?? null) : null)) && is_array($__internal_dc4207045c42a84741452c486f47b9a66b720393ce7199ed6472faf87641355a) || $__internal_dc4207045c42a84741452c486f47b9a66b720393ce7199ed6472faf87641355a instanceof ArrayAccess ? ($__internal_dc4207045c42a84741452c486f47b9a66b720393ce7199ed6472faf87641355a["submit_button_type"] ?? null) : null) != "fix") && ((($__internal_9aadb88279782a1b4d289d7bb548995bfd16cca473494d531f0f60860c3b52ea = (($__internal_959b182e30208310347773cbfa5488d1029c8453da4b2f37a3e5b73d2741d052 = ($context["settings"] ?? null)) && is_array($__internal_959b182e30208310347773cbfa5488d1029c8453da4b2f37a3e5b73d2741d052) || $__internal_959b182e30208310347773cbfa5488d1029c8453da4b2f37a3e5b73d2741d052 instanceof ArrayAccess ? ($__internal_959b182e30208310347773cbfa5488d1029c8453da4b2f37a3e5b73d2741d052["submission"] ?? null) : null)) && is_array($__internal_9aadb88279782a1b4d289d7bb548995bfd16cca473494d531f0f60860c3b52ea) || $__internal_9aadb88279782a1b4d289d7bb548995bfd16cca473494d531f0f60860c3b52ea instanceof ArrayAccess ? ($__internal_9aadb88279782a1b4d289d7bb548995bfd16cca473494d531f0f60860c3b52ea["submit_type"] ?? null) : null) != "button"))) {
                    echo "style=\"display:none;\" ";
                }
                echo " />
                   ";
                // line 379
                if ( !($context["isHorizontal"] ?? null)) {
                    echo "<input type=\"reset\" class=\"bf-buttonclear\" onclick=\"BrainyFilter.reset();return false;\" value=\"";
                    echo ($context["reset"] ?? null);
                    echo "\" />";
                }
                echo "  
                </div> ";
            }
            // line 381
            echo "            </form>
        </div>
    </div>
</div>
<script>
var bfLang = {
    show_more : '";
            // line 387
            echo ($context["lang_show_more"] ?? null);
            echo "',
    show_less : '";
            // line 388
            echo ($context["lang_show_less"] ?? null);
            echo "',
    empty_list : '";
            // line 389
            echo ($context["lang_empty_list"] ?? null);
            echo "'
};
BrainyFilter.requestCount = BrainyFilter.requestCount || ";
            // line 391
            echo (((($__internal_b9462c1f56277055090818f5a4daafd35ea82a3e856d1b2071bb5db5f2435766 = (($__internal_0bc5bd0f7070e93681192084f4740838129e7147f243e5ed200b32e12e562b16 = ($context["settings"] ?? null)) && is_array($__internal_0bc5bd0f7070e93681192084f4740838129e7147f243e5ed200b32e12e562b16) || $__internal_0bc5bd0f7070e93681192084f4740838129e7147f243e5ed200b32e12e562b16 instanceof ArrayAccess ? ($__internal_0bc5bd0f7070e93681192084f4740838129e7147f243e5ed200b32e12e562b16["behaviour"] ?? null) : null)) && is_array($__internal_b9462c1f56277055090818f5a4daafd35ea82a3e856d1b2071bb5db5f2435766) || $__internal_b9462c1f56277055090818f5a4daafd35ea82a3e856d1b2071bb5db5f2435766 instanceof ArrayAccess ? ($__internal_b9462c1f56277055090818f5a4daafd35ea82a3e856d1b2071bb5db5f2435766["product_count"] ?? null) : null)) ? ("true") : ("false"));
            echo ";
BrainyFilter.requestPrice = BrainyFilter.requestPrice || ";
            // line 392
            echo (((($__internal_0618bff272785d9ca218bcde12692d8b010ba60a51d81b60287249d9ff8d7998 = (($__internal_4c854f7a9d11d0069289b2df19f3ea555711707d46b9293dd1465b5eae1edc8d = (($__internal_47295a5b064451f3b0c4eed397e0efce1d611c4efe3f296b9c01ee7f8e94b4bb = (($__internal_76887dd057b58ee524dd12da71c28991ba8e6b511ce7ddd7c4b24e56dd53f584 = ($context["settings"] ?? null)) && is_array($__internal_76887dd057b58ee524dd12da71c28991ba8e6b511ce7ddd7c4b24e56dd53f584) || $__internal_76887dd057b58ee524dd12da71c28991ba8e6b511ce7ddd7c4b24e56dd53f584 instanceof ArrayAccess ? ($__internal_76887dd057b58ee524dd12da71c28991ba8e6b511ce7ddd7c4b24e56dd53f584["behaviour"] ?? null) : null)) && is_array($__internal_47295a5b064451f3b0c4eed397e0efce1d611c4efe3f296b9c01ee7f8e94b4bb) || $__internal_47295a5b064451f3b0c4eed397e0efce1d611c4efe3f296b9c01ee7f8e94b4bb instanceof ArrayAccess ? ($__internal_47295a5b064451f3b0c4eed397e0efce1d611c4efe3f296b9c01ee7f8e94b4bb["sections"] ?? null) : null)) && is_array($__internal_4c854f7a9d11d0069289b2df19f3ea555711707d46b9293dd1465b5eae1edc8d) || $__internal_4c854f7a9d11d0069289b2df19f3ea555711707d46b9293dd1465b5eae1edc8d instanceof ArrayAccess ? ($__internal_4c854f7a9d11d0069289b2df19f3ea555711707d46b9293dd1465b5eae1edc8d["price"] ?? null) : null)) && is_array($__internal_0618bff272785d9ca218bcde12692d8b010ba60a51d81b60287249d9ff8d7998) || $__internal_0618bff272785d9ca218bcde12692d8b010ba60a51d81b60287249d9ff8d7998 instanceof ArrayAccess ? ($__internal_0618bff272785d9ca218bcde12692d8b010ba60a51d81b60287249d9ff8d7998["enabled"] ?? null) : null)) ? ("true") : ("false"));
            echo ";
BrainyFilter.separateCountRequest = BrainyFilter.separateCountRequest || ";
            // line 393
            echo ((($context["postponedCount"] ?? null)) ? ("true") : ("false"));
            echo ";
BrainyFilter.min = BrainyFilter.min || ";
            // line 394
            echo ($context["priceMin"] ?? null);
            echo ";
BrainyFilter.max = BrainyFilter.max || ";
            // line 395
            echo ($context["priceMax"] ?? null);
            echo ";
BrainyFilter.lowerValue = BrainyFilter.lowerValue || ";
            // line 396
            echo ($context["lowerlimit"] ?? null);
            echo "; 
BrainyFilter.higherValue = BrainyFilter.higherValue || ";
            // line 397
            echo ($context["upperlimit"] ?? null);
            echo ";
BrainyFilter.currencySymb = BrainyFilter.currencySymb || '";
            // line 398
            echo ($context["currency_symbol"] ?? null);
            echo "';
BrainyFilter.hideEmpty = BrainyFilter.hideEmpty || ";
            // line 399
            echo twig_number_format_filter($this->env, (($__internal_66a8ad5333bed581c6de7652503f12928c838cf5f4f0a3cae03a065039303fb4 = (($__internal_2628c9615b3426b7c5874694384b0df9e9f6ab7c6f879785f124f6e3a2a94777 = ($context["settings"] ?? null)) && is_array($__internal_2628c9615b3426b7c5874694384b0df9e9f6ab7c6f879785f124f6e3a2a94777) || $__internal_2628c9615b3426b7c5874694384b0df9e9f6ab7c6f879785f124f6e3a2a94777 instanceof ArrayAccess ? ($__internal_2628c9615b3426b7c5874694384b0df9e9f6ab7c6f879785f124f6e3a2a94777["behaviour"] ?? null) : null)) && is_array($__internal_66a8ad5333bed581c6de7652503f12928c838cf5f4f0a3cae03a065039303fb4) || $__internal_66a8ad5333bed581c6de7652503f12928c838cf5f4f0a3cae03a065039303fb4 instanceof ArrayAccess ? ($__internal_66a8ad5333bed581c6de7652503f12928c838cf5f4f0a3cae03a065039303fb4["hide_empty"] ?? null) : null));
            echo ";
BrainyFilter.baseUrl = BrainyFilter.baseUrl || \"";
            // line 400
            echo ($context["base"] ?? null);
            echo "\";
BrainyFilter.currentRoute = BrainyFilter.currentRoute || \"";
            // line 401
            echo ($context["currentRoute"] ?? null);
            echo "\";
BrainyFilter.selectors = BrainyFilter.selectors || {
    'container' : '";
            // line 403
            echo (($__internal_e92494c1e9346ed6fde9c084e4ea58556c55d398ce6060d0eee0fc3b6a9957d0 = (($__internal_bfdb517baffa5879712c366e54201851165d2f75a825d71b872b9ee3500d3727 = ($context["settings"] ?? null)) && is_array($__internal_bfdb517baffa5879712c366e54201851165d2f75a825d71b872b9ee3500d3727) || $__internal_bfdb517baffa5879712c366e54201851165d2f75a825d71b872b9ee3500d3727 instanceof ArrayAccess ? ($__internal_bfdb517baffa5879712c366e54201851165d2f75a825d71b872b9ee3500d3727["behaviour"] ?? null) : null)) && is_array($__internal_e92494c1e9346ed6fde9c084e4ea58556c55d398ce6060d0eee0fc3b6a9957d0) || $__internal_e92494c1e9346ed6fde9c084e4ea58556c55d398ce6060d0eee0fc3b6a9957d0 instanceof ArrayAccess ? ($__internal_e92494c1e9346ed6fde9c084e4ea58556c55d398ce6060d0eee0fc3b6a9957d0["containerSelector"] ?? null) : null);
            echo "',
    'paginator' : '";
            // line 404
            echo (($__internal_407bb9aeafcd6398c87e06fb1089319d79808a6f218ff275a7e1ac05b4977834 = (($__internal_30f275f6b43b69c3ccb2c7888a55c741eab0a2c2a37f330377bee8710af1ecc5 = ($context["settings"] ?? null)) && is_array($__internal_30f275f6b43b69c3ccb2c7888a55c741eab0a2c2a37f330377bee8710af1ecc5) || $__internal_30f275f6b43b69c3ccb2c7888a55c741eab0a2c2a37f330377bee8710af1ecc5 instanceof ArrayAccess ? ($__internal_30f275f6b43b69c3ccb2c7888a55c741eab0a2c2a37f330377bee8710af1ecc5["behaviour"] ?? null) : null)) && is_array($__internal_407bb9aeafcd6398c87e06fb1089319d79808a6f218ff275a7e1ac05b4977834) || $__internal_407bb9aeafcd6398c87e06fb1089319d79808a6f218ff275a7e1ac05b4977834 instanceof ArrayAccess ? ($__internal_407bb9aeafcd6398c87e06fb1089319d79808a6f218ff275a7e1ac05b4977834["paginatorSelector"] ?? null) : null);
            echo "'
};
";
            // line 406
            if (($context["redirectToUrl"] ?? null)) {
                // line 407
                echo "BrainyFilter.redirectTo = BrainyFilter.redirectTo || \"";
                echo ($context["redirectToUrl"] ?? null);
                echo "\";
";
            }
            // line 409
            echo "jQuery(function() {
    if (!BrainyFilter.isInitialized) {
        BrainyFilter.isInitialized = true;
        var def = jQuery.Deferred();
        def.then(function() {
            if('ontouchend' in document && jQuery.ui) {
                jQuery('head').append('<script src=\"catalog/view/javascript/jquery.ui.touch-punch.min.js\"></script' + '>');
            }
        });
        if (typeof jQuery.fn.slider === 'undefined') {
            jQuery.getScript('catalog/view/javascript/jquery-ui.slider.min.js', function(){
                def.resolve();
                jQuery('head').append('<link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/jquery-ui.slider.min.css\" type=\"text/css\" />');
                BrainyFilter.init();
            });
        } else {
            def.resolve();
            BrainyFilter.init();
        }
    }
});
BrainyFilter.sliderValues = BrainyFilter.sliderValues || {};
";
            // line 431
            if (twig_length_filter($this->env, ($context["filters"] ?? null))) {
                // line 432
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["filters"] ?? null));
                foreach ($context['_seq'] as $context["i"] => $context["section"]) {
                    // line 433
                    echo "        ";
                    if ((twig_get_attribute($this->env, $this->source, $context["section"], "array", [], "array", true, true, false, 433) && twig_length_filter($this->env, (($__internal_23461e0e4984a2eb0fed4b90cb0344d8c51f61050ea9af4f7083eb7f2ea90452 = $context["section"]) && is_array($__internal_23461e0e4984a2eb0fed4b90cb0344d8c51f61050ea9af4f7083eb7f2ea90452) || $__internal_23461e0e4984a2eb0fed4b90cb0344d8c51f61050ea9af4f7083eb7f2ea90452 instanceof ArrayAccess ? ($__internal_23461e0e4984a2eb0fed4b90cb0344d8c51f61050ea9af4f7083eb7f2ea90452["array"] ?? null) : null)))) {
                        // line 434
                        echo "            ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((($__internal_dbd15bc5fa55fd4d250df7010a0c5825bb9aa7750495542417887f4417c0ac93 = $context["section"]) && is_array($__internal_dbd15bc5fa55fd4d250df7010a0c5825bb9aa7750495542417887f4417c0ac93) || $__internal_dbd15bc5fa55fd4d250df7010a0c5825bb9aa7750495542417887f4417c0ac93 instanceof ArrayAccess ? ($__internal_dbd15bc5fa55fd4d250df7010a0c5825bb9aa7750495542417887f4417c0ac93["array"] ?? null) : null));
                        foreach ($context['_seq'] as $context["groupId"] => $context["group"]) {
                            // line 435
                            echo "                ";
                            $context["groupUID"] = (twig_slice($this->env, (($__internal_fd64a930b0e9c0b02a2d61bbaa0ba6ba24cefb14d851328b69201ef4500dcbe0 = $context["section"]) && is_array($__internal_fd64a930b0e9c0b02a2d61bbaa0ba6ba24cefb14d851328b69201ef4500dcbe0) || $__internal_fd64a930b0e9c0b02a2d61bbaa0ba6ba24cefb14d851328b69201ef4500dcbe0 instanceof ArrayAccess ? ($__internal_fd64a930b0e9c0b02a2d61bbaa0ba6ba24cefb14d851328b69201ef4500dcbe0["type"] ?? null) : null), 0, 1) . $context["groupId"]);
                            // line 436
                            echo "                ";
                            if (twig_in_filter((($__internal_fec47a4f10ac9f3da75d681d729bf1a57a27b279b5cc771769bb059673cb18b1 = $context["group"]) && is_array($__internal_fec47a4f10ac9f3da75d681d729bf1a57a27b279b5cc771769bb059673cb18b1) || $__internal_fec47a4f10ac9f3da75d681d729bf1a57a27b279b5cc771769bb059673cb18b1 instanceof ArrayAccess ? ($__internal_fec47a4f10ac9f3da75d681d729bf1a57a27b279b5cc771769bb059673cb18b1["type"] ?? null) : null), [0 => "slider", 1 => "slider_lbl", 2 => "slider_lbl_inp"])) {
                                // line 437
                                echo "                    BrainyFilter.sliderValues['";
                                echo ($context["groupUID"] ?? null);
                                echo "'] = ";
                                echo json_encode((($__internal_52e779c9aa70e56e92aba2ce631519b7ea3c549e9a38275eb12d9ae0e739aead = $context["group"]) && is_array($__internal_52e779c9aa70e56e92aba2ce631519b7ea3c549e9a38275eb12d9ae0e739aead) || $__internal_52e779c9aa70e56e92aba2ce631519b7ea3c549e9a38275eb12d9ae0e739aead instanceof ArrayAccess ? ($__internal_52e779c9aa70e56e92aba2ce631519b7ea3c549e9a38275eb12d9ae0e739aead["values"] ?? null) : null));
                                echo ";
                ";
                            }
                            // line 439
                            echo "            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['groupId'], $context['group'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 440
                        echo "        ";
                    }
                    // line 441
                    echo "    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['i'], $context['section'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 443
            echo "</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/brainyfilter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1394 => 443,  1387 => 441,  1384 => 440,  1378 => 439,  1370 => 437,  1367 => 436,  1364 => 435,  1359 => 434,  1356 => 433,  1351 => 432,  1349 => 431,  1325 => 409,  1319 => 407,  1317 => 406,  1312 => 404,  1308 => 403,  1303 => 401,  1299 => 400,  1295 => 399,  1291 => 398,  1287 => 397,  1283 => 396,  1279 => 395,  1275 => 394,  1271 => 393,  1267 => 392,  1263 => 391,  1258 => 389,  1254 => 388,  1250 => 387,  1242 => 381,  1233 => 379,  1225 => 378,  1216 => 377,  1209 => 375,  1206 => 374,  1198 => 371,  1195 => 370,  1187 => 367,  1184 => 366,  1181 => 365,  1172 => 363,  1169 => 362,  1166 => 361,  1160 => 359,  1157 => 358,  1154 => 357,  1152 => 356,  1147 => 353,  1141 => 351,  1138 => 350,  1132 => 348,  1129 => 347,  1121 => 345,  1118 => 344,  1116 => 343,  1112 => 342,  1108 => 341,  1100 => 340,  1093 => 338,  1089 => 337,  1081 => 336,  1077 => 335,  1073 => 334,  1069 => 333,  1063 => 331,  1059 => 329,  1056 => 328,  1053 => 327,  1050 => 326,  1048 => 325,  1043 => 324,  1040 => 323,  1036 => 322,  1033 => 321,  1027 => 317,  1018 => 314,  1011 => 312,  1007 => 311,  1001 => 310,  997 => 309,  993 => 308,  988 => 306,  984 => 305,  981 => 304,  978 => 303,  974 => 302,  969 => 300,  966 => 299,  964 => 298,  954 => 293,  948 => 292,  945 => 291,  938 => 289,  930 => 287,  927 => 286,  921 => 285,  915 => 283,  912 => 282,  909 => 281,  904 => 280,  901 => 279,  898 => 278,  892 => 277,  886 => 275,  883 => 274,  880 => 273,  875 => 272,  872 => 271,  869 => 270,  866 => 269,  864 => 268,  854 => 267,  843 => 266,  840 => 265,  837 => 264,  835 => 263,  829 => 260,  826 => 259,  824 => 258,  818 => 254,  809 => 251,  806 => 250,  800 => 249,  797 => 248,  793 => 246,  790 => 245,  783 => 244,  780 => 243,  776 => 242,  772 => 241,  768 => 240,  763 => 238,  760 => 237,  758 => 236,  755 => 235,  753 => 234,  747 => 231,  738 => 230,  731 => 229,  728 => 228,  725 => 227,  722 => 226,  719 => 225,  716 => 224,  713 => 223,  710 => 222,  707 => 221,  704 => 220,  701 => 219,  698 => 218,  696 => 217,  691 => 216,  688 => 215,  685 => 214,  680 => 213,  678 => 212,  675 => 211,  669 => 207,  666 => 206,  658 => 203,  655 => 202,  646 => 200,  643 => 199,  640 => 198,  634 => 196,  631 => 195,  629 => 194,  622 => 190,  618 => 189,  614 => 188,  610 => 187,  603 => 185,  599 => 184,  592 => 183,  588 => 182,  584 => 181,  580 => 180,  574 => 178,  570 => 176,  567 => 175,  564 => 174,  561 => 173,  559 => 172,  554 => 171,  551 => 170,  546 => 169,  540 => 165,  530 => 162,  524 => 161,  521 => 160,  518 => 159,  515 => 158,  510 => 157,  508 => 156,  499 => 155,  496 => 154,  493 => 153,  489 => 152,  485 => 151,  481 => 150,  475 => 148,  473 => 147,  470 => 146,  468 => 145,  462 => 142,  452 => 141,  448 => 139,  446 => 138,  437 => 132,  429 => 127,  419 => 126,  413 => 125,  410 => 124,  408 => 123,  399 => 117,  392 => 116,  387 => 114,  382 => 113,  376 => 110,  372 => 109,  367 => 107,  363 => 106,  360 => 105,  358 => 104,  351 => 100,  341 => 99,  334 => 98,  331 => 97,  328 => 96,  326 => 95,  323 => 94,  319 => 93,  316 => 92,  310 => 90,  307 => 89,  301 => 87,  298 => 86,  294 => 84,  290 => 82,  288 => 81,  283 => 79,  279 => 78,  275 => 77,  271 => 76,  267 => 75,  263 => 74,  259 => 73,  255 => 72,  251 => 71,  244 => 70,  239 => 69,  235 => 68,  229 => 67,  222 => 65,  212 => 64,  206 => 63,  193 => 60,  191 => 59,  188 => 58,  181 => 54,  177 => 53,  173 => 52,  168 => 50,  164 => 49,  160 => 48,  155 => 46,  151 => 45,  147 => 44,  142 => 42,  138 => 41,  134 => 40,  129 => 38,  125 => 37,  120 => 35,  116 => 34,  112 => 33,  107 => 31,  103 => 30,  99 => 29,  96 => 28,  90 => 26,  88 => 25,  84 => 24,  81 => 23,  75 => 21,  73 => 20,  69 => 19,  64 => 17,  60 => 16,  56 => 15,  51 => 13,  47 => 12,  43 => 10,  41 => 9,  39 => 8,  37 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("{#
 * Brainy Filter Pro 5.1.3 OC3, September 18, 2017 / brainyfilter.com 
 * Copyright 2015-2017 Giant Leap Lab / www.giantleaplab.com 
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store. 
 * Support: http://support.giantleaplab.com 
#}
{% set isHorizontal = layout_position is same as ('content_top') or layout_position is same as ('content_bottom') %}
{% set isResponsive = settings['style']['responsive']['enabled'] ? true : false %}
{% set responsivePos = settings['style']['responsive']['position'] is same as ('right') ? 'bf-right' : 'bf-left' %}

<style type=\"text/css\">
    .bf-responsive.bf-active.bf-layout-id-{{ layout_id }} .bf-check-position {
        top: {{ settings['style']['responsive']['offset']|number_format }}px;
    }
    .bf-responsive.bf-active.bf-layout-id-{{ layout_id }} .bf-btn-show, 
    .bf-responsive.bf-active.bf-layout-id-{{ layout_id }} .bf-btn-reset {
        top: {{ settings['style']['responsive']['offset']|number_format }}px;
    }
    .bf-layout-id-{{ layout_id }} .bf-btn-show {
        {% if settings['style']['resp_show_btn_color']['val'] is defined %}
            background: {{ settings['style']['resp_show_btn_color']['val'] }};
        {% endif %}
    }
    .bf-layout-id-{{ layout_id }} .bf-btn-reset {
        {% if settings['style']['resp_reset_btn_color']['val'] is defined %}
            background: {{ settings['style']['resp_reset_btn_color']['val'] }};
        {% endif %}
    }
    .bf-layout-id-{{ layout_id }} .bf-attr-header{
        {{ settings['style']['block_header_background']['val'] is defined  ? 'background: ' ~ settings['style']['block_header_background']['val'] ~ ';':'' }}
        {{ settings['style']['block_header_text']['val'] is defined ? 'color: ' ~ settings['style']['block_header_text']['val'] ~ ';':'' }} 
    }
    .bf-layout-id-{{ layout_id }} .bf-count{
        {{ settings['style']['product_quantity_background']['val'] is defined ? 'background: ' ~ settings['style']['product_quantity_background']['val'] ~ ';':'' }} 
        {{ settings['style']['product_quantity_text']['val'] is defined ? 'color: ' ~ settings['style']['product_quantity_text']['val'] ~ ';':'' }} 
    }
   .bf-layout-id-{{ layout_id }} .ui-widget-header {
        {{ settings['style']['price_slider_area_background']['val'] is defined ? 'background: ' ~ settings['style']['price_slider_area_background']['val'] ~ ';':'' }} 
    }
   .bf-layout-id-{{ layout_id }} .ui-widget-content {
        {{ settings['style']['price_slider_background']['val'] is defined ? 'background: ' ~ settings['style']['price_slider_background']['val'] ~ ';':'' }} 
        {{ settings['style']['price_slider_border']['val'] is defined ? 'border:1px solid' ~ settings['style']['price_slider_border']['val'] ~ ';':'' }} 
    }
    .bf-layout-id-{{ layout_id }} .ui-state-default {
        {{ settings['style']['price_slider_handle_background']['val'] is defined ? 'background:' ~ settings['style']['price_slider_handle_background']['val'] ~ ';':'' }} 
        {{ settings['style']['price_slider_handle_border']['val'] is defined ? 'border:1px solid' ~ settings['style']['price_slider_handle_border']['val'] ~ ';':'' }} 
   }
    .bf-layout-id-{{ layout_id }} .bf-attr-group-header{
        {{ settings['style']['group_block_header_background']['val'] is defined ? 'background:' ~ settings['style']['group_block_header_background']['val'] ~ ';':'' }} 
        {{ settings['style']['group_block_header_text']['val'] is defined ? 'color:' ~ settings['style']['group_block_header_text']['val'] ~ ';':'' }} 
    }
    {% if settings['behaviour']['hide_empty'] %}>
        .bf-layout-id-{{ layout_id }} .bf-row.bf-disabled, 
        .bf-layout-id-{{ layout_id }} .bf-horizontal .bf-row.bf-disabled {
            display: none;
        }
    {% endif %}
</style>
{% if filters|length %}
<div class=\"bf-panel-wrapper{% if isResponsive %} bf-responsive {% endif %} {{ responsivePos }} bf-layout-id-{{ layout_id }}\">
    <div class=\"bf-btn-show\"></div>
    <a class=\"bf-btn-reset\" onclick=\"BrainyFilter.reset();\"></a>
    <div class=\"box bf-check-position {% if isHorizontal %} bf-horizontal {% endif %}\">
        <div class=\"box-heading\">{{ lang_block_title }} {% if isHorizontal %} <a class=\"bf-toggle-filter-arrow\"></a><input type=\"reset\" class=\"bf-buttonclear\" onclick=\"BrainyFilter.reset();\" value=\"{{ reset }}\" /> {% endif %}</div>
        <div class=\"brainyfilter-panel box-content {% if settings['submission']['hide_panel'] %} bf-hide-panel {% endif %}\">
            <form class=\"bf-form 
                    {% if settings['behaviour']['product_count'] %} bf-with-counts {% endif %} 
                    {% if sliding %} bf-with-sliding {% endif %}
                    {% if settings['submission']['submit_type'] is same as ('button') and settings['submission']['submit_button_type'] is same as ('float') %} bf-with-float-btn {% endif %}
                    {% if limit_height %} bf-with-height-limit {% endif %}\"
                    data-height-limit=\"{{ limit_height_opts }}\"
                    data-visible-items=\"{{ slidingOpts }}\"
                    data-hide-items=\"{{ slidingMin }}\"
                    data-submit-type=\"{{ settings['submission']['submit_type'] }}\"
                    data-submit-delay=\"{{ settings['submission']['submit_delay_time']|number_format }}\"
                    data-submit-hide-panel =\"{{ settings['submission']['hide_panel']|number_format }}\"
                    data-resp-max-width=\"{{ settings['style']['responsive']['max_width']|number_format }}\"
                    data-resp-collapse=\"{{ settings['style']['responsive']['collapsed']|number_format }}\"
                    data-resp-max-scr-width =\"{{ settings['style']['responsive']['max_screen_width']|number_format }}\"
                    method=\"get\" action=\"index.php\">
                {% if currentRoute is same as ('product/search') %}
                    <input type=\"hidden\" name=\"route\" value=\"product/search\" />
                {% else %}
                    <input type=\"hidden\" name=\"route\" value=\"product/category\" />
                {% endif %}
                {% if currentPath %}
                    <input type=\"hidden\" name=\"path\" value=\"{{ currentPath }}\" />
                {% endif %}
                {% if manufacturerId %}
                    <input type=\"hidden\" name=\"manufacturer_id\" value=\"{{ manufacturerId }}\" />
                {% endif %}

                {% for i, section in filters %}
                        
                    {% if section['type'] == 'price' %}
                        {% set sliderType = section['control'] is same as ('slider_lbl_inp') ? 3 : (section['control'] is same as ('slider_lbl') ? 2 : 1) %}
                        {% set inputType  = sliderType in [1, 3] ? 'text' : 'hidden' %}
                        <div class=\"bf-attr-block bf-price-filter {% if isHorizontal and filters[i + 1] is defined and filters[i + 1]['type'] is same as ('search') %}bf-left-half{% endif %}\">
                        <div class=\"bf-attr-header {% if section['collapsed'] %} bf-collapse {% endif %} {% if not i %} bf-w-line {% endif %}\">
                            {{ lang_price }}<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            <div class=\"bf-price-container box-content bf-attr-filter\">
                                {% if sliderType in [1, 3] %}
                                <div class=\"bf-cur-symb\">
                                    <span class=\"bf-cur-symb-left\">{{ currency_symbol }}</span>
                                    <input type=\"text\" class=\"bf-range-min\" name=\"bfp_price_min\" value=\"{{ lowerlimit }}\" size=\"4\" />
                                    <span class=\"ndash\">&#8211;</span>
                                    <span class=\"bf-cur-symb-left\">{{ currency_symbol }}</span>
                                    <input type=\"text\" class=\"bf-range-max\" name=\"bfp_price_max\" value=\"{{ upperlimit }}\" size=\"4\" /> 
                                </div>
                                {% else %}
                                <input type=\"hidden\" class=\"bf-range-min\" name=\"bfp_price_min\" value=\"{{ lowerlimit }}\" />
                                <input type=\"hidden\" class=\"bf-range-max\" name=\"bfp_price_max\" value=\"{{ upperlimit }}\" /> 
                                {% endif %}
                                <div class=\"bf-price-slider-container {% if sliderType is same as (2) or sliderType is same as (3) %} bf-slider-with-labels {% endif %}\">
                                    <div class=\"bf-slider-range\" data-slider-type=\"{{ sliderType }}\"></div>
                                </div>
                            </div>
                        </div>
                        </div>
                
                    {% elseif section['type'] == 'search' %}
                
                        <div class=\"bf-attr-block bf-keywords-filter {% if isHorizontal and filters[i + 1] is defined and filters[i + 1]['type'] is same as ('price') %} bf-left-half {% endif %}\">
                        <div class=\"bf-attr-header{% if section['collapsed'] %} bf-collapse {% endif %} {% if not i %} bf-w-line {% endif %}\">
                            {{ lang_search }}<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            <div class=\"bf-search-container bf-attr-filter\">
                                <div>
                                    <input type=\"text\" class=\"bf-search\" name=\"bfp_search\" value=\"{{ bfSearch }}\" /> 
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    {% elseif section['type'] == 'category' %}
                        
                        <div class=\"bf-attr-block\">
                        <div class=\"bf-attr-header{% if section['collapsed'] %} bf-collapse {% endif %} {% if not i %} bf-w-line {% endif %}\">
                            {{ lang_categories }}<span class=\"bf-arrow\"></span>
                        </div>
                        <div class=\"bf-attr-block-cont\">
                            {% set groupUID = 'c0' %}

                            {% if section['control'] == 'select' %}
                            <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row\">
                                <div class=\"bf-cell\">
                                    <select name=\"bfp_{{ groupUID }}\">
                                        <option value=\"\" class=\"bf-default\">{{ default_value_select }}</option>
                                        {% for cat in section['values'] %}
                                            {% set catId = cat['id'] %}
                                            {% set isSelected = selected[groupUID] is defined and catId in selected[groupUID] %}
                                            <option value=\"{{ catId }}\" class=\"bf-attr-val\" {% if isSelected %} selected=\"true\" {% endif %}>
                                                {% set level = '' %}
                                                {% for i in 0..cat['level'] %}
                                                    {% if i %}
                                                        {% set level  = level ~ '-' %}
                                                    {% endif %}
                                                {% endfor %}
                                                {{level ~ ' ' ~ cat['name']}}
                                            </option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                            {% else %}
                                {% for cat in section['values'] %}
                                    {% set catId = cat['id'] %}
                                    <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row
                                        {% if totals is defined and settings['behaviour']['hide_empty'] %}
                                            {% set inStock = postponedCount or (totals[groupUID][catId] is defined and totals[groupUID][catId]) %}
                                            {% set inSelected = selected[groupUID] is defined and catId in selected[groupUID] %}
                                            {% if not inStock and not inSelected %}
                                                bf-disabled
                                            {% endif %}
                                        {% endif %}\">
                                    <span class=\"bf-cell bf-c-1\">
                                        <input id=\"bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}\"
                                               data-filterid=\"bf-attr-{{ groupUID ~ '_' ~ catId }}\"
                                               type=\"{{ section['control'] }}\" 
                                               name=\"bfp_{{ groupUID }}{% if section['control'] is same as ('checkbox') %}{{ '_' ~ catId }}{% endif %}\"
                                               value=\"{{ catId }}\" 
                                               {% if selected[groupUID] is defined and catId in selected[groupUID] %} checked=\"true\" {% endif %} />
                                    </span>
                                    <span class=\"bf-cell bf-c-2 bf-cascade-{{ cat['level'] }}\">
                                        <span class=\"bf-hidden bf-attr-val\">{{ catId }}</span>
                                        <label for=\"bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}\">
                                            {{ cat['name'] }}
                                        </label>
                                    </span>
                                            <span class=\"bf-cell bf-c-3\">
                                                {% if totals is defined %}
                                                    {% if totals[groupUID][catId] is not defined and selected[groupUID] is not defined %}
                                                        {{ '' }}
                                                    {% else %}
                                                        {% set total = totals[groupUID][catId] is defined ? totals[groupUID][catId] : 0 %}
                                                        {% set addPlusSign = selected[groupUID] is defined %}
                                                        <span class=\"bf-count {{ not total ? 'bf-empty' : '' }}\">{{ addPlusSign ? '+' : '' }}{{ total }}</span>
                                                    {% endif %}
                                                {% endif %}
                                            </span>
                                </div>
                                {% endfor %}
                            {% endif %}
                        </div>
                        </div>
                
                    {% else %}
                        
                        {% set curGroupId = null %}
                        {% for groupId, group in section['array'] %}
                            {% if group['group_id'] is defined and settings['behaviour']['attribute_groups'] %}
                                {% if curGroupId != group['group_id'] %}
                                    <div class=\"bf-attr-group-header\">{{ group['group'] }}</div>
                                    {% set curGroupId = group['group_id'] %}
                                {% endif %}
                            {% endif %}
                            {% set collapsedGroup = false %}
                            {% if group['attr_id'] and settings['behaviour']['sections']['attribute'][group['attr_id']] %}
                                {% set collapsedGroup = settings['behaviour']['sections']['attribute'][group['attr_id']]['collapsed'] %}
                            {% elseif group['option_id'] and settings['behaviour']['sections']['options'][group['option_id']] %}
                                {% set collapsedGroup = settings['behaviour']['sections']['options'][group['option_id']]['collapsed'] %}
                            {% elseif group['filter_id'] and settings['behaviour']['sections']['filter'][group['filter_id']] %}
                                {% set collapsedGroup = settings['behaviour']['sections']['filter'][group['filter_id']]['collapsed'] %}
                            {% endif %}
                            {% set groupUID = section['type']|slice(0, 1) ~ groupId %}
                            <div class=\"bf-attr-block{% if group['type'] in ['slider', 'slider_lbl', 'slider_lbl_inp'] %} bf-slider {% endif %}\">
                            <div class=\"bf-attr-header{% if section['collapsed']  or collapsedGroup %} bf-collapse {% endif %}{% if not i %} bf-w-line {% endif %}\">
                                {{ group['name'] ~ ' ' }}<span class=\"bf-arrow\"></span>
                            </div>
                            <div class=\"bf-attr-block-cont\">
                                    {% set group = group['type'] is defined ? group|merge({'type': group['type']}) : group|merge({'type': 'checkbox'}) %}
                                
                                {% if group['type'] == 'select' %}
                                
                                    <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row\">
                                        <div class=\"bf-cell\">
                                            <select name=\"bfp_{{groupUID}}\">
                                                <option value=\"\" class=\"bf-default\">{{ default_value_select }}</option>
                                                {% for value in group['values'] %}
                                                    {% set isSelected = selected[groupUID] is defined and value['id'] in selected[groupUID] %}
                                                    <option value=\"{{ value['id'] }}\" class=\"bf-attr-val\" {% if isSelected %} selected=\"true\" {% endif %}
                                                        {% if totals and totals[groupUID][value['id']] is not defined and  not isSelected %}
                                                            disabled=\"disabled\"
                                                        {% endif %}
                                                        {% if totals[groupUID][value['id']] is defined and not isSelected %}
                                                            data-totals=\"{{ totals[groupUID][value['id']] is defined ? totals[groupUID][value['id']] : 0 }}\"
                                                        {% endif %} >
                                                        {{ value['name'] }}
                                                    </option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                    </div>
                                
                                {% elseif group['type'] in ['slider', 'slider_lbl', 'slider_lbl_inp'] %}
                                
                                <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row\">
                                    <div class=\"bf-cell\">
                                        <div class=\"bf-slider-inputs\">
                                            {% set isMinSet = selected[groupUID]['min'] is defined %}
                                            {% set isMaxSet = selected[groupUID]['max'] is defined %}
                                            {% set sliderType = group['type'] is same as ('slider_lbl_inp') ? 3 : (group['type'] is same as ('slider_lbl') ? 2 : 1) %}
                                            <input type=\"hidden\" name=\"bfp_min_{{ groupUID }}\" value=\"{{ isMinSet ? selected[groupUID]['min'] : 'na' }}\" class=\"bf-attr-min-{{ groupUID }}\" data-min-limit=\"{{ group['min']['s'] }}\" />
                                            <input type=\"hidden\" name=\"bfp_max_{{ groupUID }}\" value=\"{{ isMaxSet ? selected[groupUID]['max'] : 'na' }}\" class=\"bf-attr-max-{{ groupUID }}\" data-max-limit=\"{{ group['max']['s'] }}\" /> 
                                            {% if group['type'] != 'slider_lbl' %}
                                                {% set minLbl = '' %}
                                                {% set maxLbl = '' %}
                                                {% if isMinSet %}
                                                    {% for v in group['values'] %}
                                                        {% if v['s'] == selected[groupUID]['min'] %}
                                                            {% set minLbl = v['n'] %}
                                                            {{ break }}
                                                        {% endif %}
                                                    {% endfor %}
                                                {% endif %}
                                                {% if isMaxSet %}
                                                    {% for v in group['values'] %}
                                                        {% if v['s'] == selected[groupUID]['max'] %}
                                                            {% set maxLbl = v['n'] %}
                                                            {{ break }}
                                                        {% endif %}
                                                    {% endfor %}
                                                {% endif %}
                                            <input type=\"text\" name=\"\" class=\"bf-slider-text-inp-min bf-slider-input\" value=\"{{ minLbl }}\" placeholder=\"{{ lang_empty_slider }}\" />
                                            <span class=\"ndash\">&#8211;</span>
                                            <input type=\"text\" name=\"\" class=\"bf-slider-text-inp-max bf-slider-input\" value=\"{{ maxLbl }}\" placeholder=\"{{ lang_empty_slider }}\" />
                                            {% endif %}
                                        </div>
                                        <div class=\"bf-slider-container-wrapper {% if sliderType is same as(2) or sliderType is same as (3) %} bf-slider-with-labels {% endif %}\">
                                            <div class=\"bf-slider-container\" data-slider-group=\"{{ groupUID }}\" data-slider-type=\"{{ sliderType }}\"></div>
                                        </div>
                                    </div>  
                                </div>
                                
                                {% elseif group['type'] is same as ('grid') %}
                                
                                <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row\">
                                    <div class=\"bf-grid\">
                                        {% for value in group['values'] %}
                                            {% set valueId  = value['id'] %}
                                        <div class=\"bf-grid-item\">
                                            <input id=\"bf-attr-{{ groupUID ~ '_' ~ valueId ~ '_' ~ layout_id }}\" class=\"bf-hidden\"
                                                    data-filterid=\"bf-attr-{{ groupUID ~ '_' ~ valueId }}\"
                                                    type=\"radio\" 
                                                    name=\"\"bfp_{{ groupUID }}\"
                                                    value=\"{{ valueId }}\" 
                                                    {% if selected[groupUID] is defined and valueId in selected[groupUID] %} checked=\"true\" {% endif %} />
                                            <label for=\"bf-attr-{{ groupUID ~ '_' ~ valueId ~ '_' ~ layout_id }}\">
                                                <img src=\"image/{{ value['image'] }}\" alt=\"{{ value['name'] }}\" />
                                            </label>
                                            <span class=\"bf-hidden bf-attr-val\">{{ valueId }}</span>
                                        </div>
                                        {% endfor %}
                                    </div>
                                </div>
                                
                                {% else %}
                                
                                    {% for value in group['values'] %}
                                        {% set valueId  = value['id'] %}
                                    <div class=\"bf-attr-filter bf-attr-{{ groupUID }} bf-row
                                        {% if totals is defined and settings['behaviour']['hide_empty'] %}
                                            {% set inStock = postponedCount or (totals[groupUID][valueId] is defined and totals[groupUID][valueId]) %}
                                            {% set inSelected = selected[groupUID] is defined and valueId in selected[groupUID] %}
                                            {% if not inStock and not inSelected %}
                                                bf-disabled
                                            {% endif %}
                                        {% endif %}\">
                                        <span class=\"bf-cell bf-c-1\">
                                            <input id=\"bf-attr-{{ groupUID ~ '_' ~ valueId ~ '_' ~ layout_id }}\"
                                                   data-filterid=\"bf-attr-{{ groupUID ~ '_' ~ valueId }}\"
                                                   type=\"{{ group['type'] }}\" 
                                                   name=\"bfp_{{ groupUID }}{% if group['type'] is same as ('checkbox') %}{{ '_' ~ valueId }} {% endif %}\"
                                                   value=\"{{ valueId }}\" 
                                                   {% if selected[groupUID] is defined and valueId in selected[groupUID] %} checked=\"true\" {% endif %} />
                                        </span>
                                        <span class=\"bf-cell bf-c-2 {% if section['type'] == 'rating' %} {{ 'bf-rating-' ~ valueId }} {% endif %}\">
                                            <span class=\"bf-hidden bf-attr-val\">{{ valueId }}</span>
                                            <label for=\"bf-attr-{{ groupUID ~ '_' ~ valueId ~ '_' ~ layout_id }}\">
                                                {% if section['type'] is same as ('option') %}
                                                    {% if group['mode'] is same as ('img') or group['mode'] is same as('img_label') %}
                                                        <img src=\"image/{{ value['image'] }}\" alt=\"{{ value['name'] }}\" />
                                                    {% endif %}
                                                    {% if group['mode'] is same as ('label') or group['mode'] is same as ('img_label') %}
                                                        {{ value['name'] }}
                                                    {% endif %}
                                                {% else %}
                                                    {{ value['name'] }}
                                                {% endif %}
                                            </label>
                                        </span>
                                        <span class=\"bf-cell bf-c-3\">
                                            {% if totals is defined %}
                                                {% if totals is defined %}
                                                    {% if totals[groupUID][valueId] is not defined and selected[groupUID] is not defined %}
                                                        {{ '' }}
                                                    {% else %}
                                                        {% set total = totals[groupUID][valueId] is defined ? totals[groupUID][valueId] : 0 %}
                                                        {% set addPlusSign = selected[groupUID] is defined %}
                                                        <span class=\"bf-count {{ not total ? 'bf-empty' : '' }}\">{{ addPlusSign ? '+' : '' }}{{ total }}</span>
                                                    {% endif %}
                                                {% endif %}
                                            {% endif %}
                                        </span>
                                    </div>
                                    {% endfor %}
                                {% endif %}
                            </div>
                            </div>
                        {% endfor %}
                    {% endif %}
                    
                {% endfor %}
                {% if not isHorizontal or settings['submission']['submit_type'] == 'button' %} <div class=\"bf-buttonclear-box\"{% if isHorizontal and settings['submission']['submit_button_type'] == 'float' %} style=\"display:none;\" {% endif %}>
                         <input type=\"button\" value=\"{{ lang_submit }}\" class=\"btn btn-primary bf-buttonsubmit\" onclick=\"BrainyFilter.sendRequest(jQuery(this));BrainyFilter.loadingAnimation();return false;\" {% if settings['submission']['submit_button_type'] != 'fix' and settings['submission']['submit_type'] != 'button' %}style=\"display:none;\" {% endif %} />
                   {% if not isHorizontal %}<input type=\"reset\" class=\"bf-buttonclear\" onclick=\"BrainyFilter.reset();return false;\" value=\"{{ reset }}\" />{% endif %}  
                </div> {% endif %}
            </form>
        </div>
    </div>
</div>
<script>
var bfLang = {
    show_more : '{{ lang_show_more }}',
    show_less : '{{ lang_show_less }}',
    empty_list : '{{ lang_empty_list }}'
};
BrainyFilter.requestCount = BrainyFilter.requestCount || {{ settings['behaviour']['product_count'] ? 'true' : 'false' }};
BrainyFilter.requestPrice = BrainyFilter.requestPrice || {{ settings['behaviour']['sections']['price']['enabled'] ? 'true' : 'false' }};
BrainyFilter.separateCountRequest = BrainyFilter.separateCountRequest || {{ postponedCount ? 'true' : 'false' }};
BrainyFilter.min = BrainyFilter.min || {{ priceMin }};
BrainyFilter.max = BrainyFilter.max || {{ priceMax }};
BrainyFilter.lowerValue = BrainyFilter.lowerValue || {{ lowerlimit }}; 
BrainyFilter.higherValue = BrainyFilter.higherValue || {{ upperlimit }};
BrainyFilter.currencySymb = BrainyFilter.currencySymb || '{{ currency_symbol }}';
BrainyFilter.hideEmpty = BrainyFilter.hideEmpty || {{ settings['behaviour']['hide_empty']|number_format }};
BrainyFilter.baseUrl = BrainyFilter.baseUrl || \"{{ base }}\";
BrainyFilter.currentRoute = BrainyFilter.currentRoute || \"{{ currentRoute }}\";
BrainyFilter.selectors = BrainyFilter.selectors || {
    'container' : '{{ settings['behaviour']['containerSelector'] }}',
    'paginator' : '{{ settings['behaviour']['paginatorSelector'] }}'
};
{% if redirectToUrl %}
BrainyFilter.redirectTo = BrainyFilter.redirectTo || \"{{ redirectToUrl }}\";
{% endif %}
jQuery(function() {
    if (!BrainyFilter.isInitialized) {
        BrainyFilter.isInitialized = true;
        var def = jQuery.Deferred();
        def.then(function() {
            if('ontouchend' in document && jQuery.ui) {
                jQuery('head').append('<script src=\"catalog/view/javascript/jquery.ui.touch-punch.min.js\"></script' + '>');
            }
        });
        if (typeof jQuery.fn.slider === 'undefined') {
            jQuery.getScript('catalog/view/javascript/jquery-ui.slider.min.js', function(){
                def.resolve();
                jQuery('head').append('<link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/jquery-ui.slider.min.css\" type=\"text/css\" />');
                BrainyFilter.init();
            });
        } else {
            def.resolve();
            BrainyFilter.init();
        }
    }
});
BrainyFilter.sliderValues = BrainyFilter.sliderValues || {};
{% if filters|length %}
    {% for i, section in filters %}
        {% if section['array'] is defined and section['array']|length %}
            {% for groupId, group in section['array'] %}
                {% set groupUID = section['type']|slice(0, 1) ~ groupId %}
                {% if group['type'] in ['slider', 'slider_lbl', 'slider_lbl_inp'] %}
                    BrainyFilter.sliderValues['{{ groupUID }}'] = {{ group['values']|json_encode() }};
                {% endif %}
            {% endfor %}
        {% endif %}
    {% endfor %}
{% endif %}
</script>
{% endif %}", "default/template/extension/module/brainyfilter.twig", "");
    }
}
