<?php

class ControllerExtensionModuleUpdatePrices extends Controller {
	private $error = array();
	private $updated_cnt = 0;
	private $inserted_cnt = 0;
	private $language_id = 1; //current language


	public function index() {

		$this->load->language('extension/module/update_prices');

		$this->load->model('setting/setting');

		$this->load->model('extension/module/update_prices');

		//кастом
		//$this->document->addScript('/admin/view/javascript/ajax.js');
		//кастом
		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_update_prices', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/update_prices', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/update_prices', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_update_prices_status'])) {
			$data['module_update_prices_status'] = $this->request->post['update_prices_module_status'];
		} else {
			$data['module_update_prices_status'] = $this->config->get('update_prices_module_status');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		//$data['update_prices_link'] = 'https://b2b-sandi.com.ua/export_xml/4fb01c5bf3957d849de460be7eb84821';

		//$data['import_xml'] = (HTTPS_SERVER . 'index.php?route=extension/module/xml_module/export_xml&token=' . $this->session->data['user_token']);

		$data['user_token'] = $this->session->data['user_token'];

		//$data['export_xml'] = (HTTPS_SERVER . 'controller/update_prices/export.php');
		$data['all_manufacturers'] = $this->show_all_manufacturers();

		$table_data = $this->check_table();
		$table_rows = [];
		$k = 0;
		foreach($table_data as $item){
			$manufacturer_name = $this->model_extension_module_update_prices->get_manufacturer_name($item["manufacturer_id"]);
			$table_rows[$k] = [
				'name' => $manufacturer_name['name'],
				'percent' => $item['percent'],
				'side' => $item['side']
			];
			$k++;
		}
		$data['table_data'] = $table_rows;


        $table_data_all = $this->check_table_all();
        $table_rows_all = [];
        $k_all = 0;
        foreach($table_data_all as $item_all){

            $table_rows_all[$k_all] = [
                'percent' => $item_all['percent'],
                'side' => $item_all['side']
            ];
            $k_all++;
        }
        $data['table_data_all'] = $table_rows_all;


		$data['new_products'] = $this->model_extension_module_update_prices->get_new_products_from_temp_table_last30days();

		$this->response->setOutput($this->load->view('extension/module/update_prices', $data));

		//
		//$this->show_all_manufacturers();


	}

	public function show_all_manufacturers(){
		$this->load->model('extension/module/update_prices');
		$all_manufacturers = $this->model_extension_module_update_prices->get_all_manufacturers();
		return $all_manufacturers;

		/*foreach ($all_manufacturers as $manufacturer){
			echo '<pre>';
			var_dump($manufacturer['name'].'<br>'.$manufacturer['manufacturer_id']);
			echo '</pre>';
		}*/
	}

/*	function add_percent($price, $percent) {
		return $value + ($price * $percent / 100);
	}*/

	public function update_prices_up(){
		$this->load->model('extension/module/update_prices');
		$product_ids = $this->model_extension_module_update_prices->select_products_by_manufacturer($_POST['manufacturer']);
		//$test2 = $this->model_extension_module_update_prices->count_products_by_manufacturer($_POST['manufacturer']);
		//$product_id["product_id"] = 50;

		if($_POST["percent"] == ""){
			exit('не введен процент');
		} else {
			echo 'Обновление цен прошло успешно.';
		}
		$persent = $_POST["percent"] / 100;
		foreach($product_ids as $product_id) {
			$this->model_extension_module_update_prices->update_product_prices_up($product_id["product_id"], $persent);
			$this->model_extension_module_update_prices->write_percent_up($product_id["product_id"], $persent);
		}

		$manufacturer_id = $_POST['manufacturer'];
		$this->model_extension_module_update_prices->clean_before_write($manufacturer_id);
		$this->model_extension_module_update_prices->write_updates_up($manufacturer_id, $persent);


	}


	public function update_prices_down(){
		$this->load->model('extension/module/update_prices');
		$product_ids = $this->model_extension_module_update_prices->select_products_by_manufacturer($_POST['manufacturer']);
		//$test2 = $this->model_extension_module_update_prices->count_products_by_manufacturer($_POST['manufacturer']);
		//$product_id["product_id"] = 50;
		if($_POST["percent"] == ""){
			exit('не введен процент');
		} else {
			echo 'Обновление цен прошло успешно.';
		}

		$persent = $_POST["percent"] / 100;
		foreach($product_ids as $product_id) {
			$this->model_extension_module_update_prices->update_product_prices_down($product_id["product_id"], $persent);
			$this->model_extension_module_update_prices->write_percent_down($product_id["product_id"], $persent);
		}

		$manufacturer_id = $_POST['manufacturer'];
		$this->model_extension_module_update_prices->clean_before_write($manufacturer_id);
		$this->model_extension_module_update_prices->write_updates_down($manufacturer_id, $persent);
	}
	//all >
    public function update_prices_up_all(){
        $this->load->model('extension/module/update_prices');

        if($_POST["percent"] == ""){
            exit('не введен процент');
        } else {
            echo 'Обновление цен прошло успешно.';
        }
        $persent = $_POST["percent"] / 100;

        $this->model_extension_module_update_prices->update_product_prices_up_all($persent);
        $this->model_extension_module_update_prices->write_percent_up_all($persent);


        $this->model_extension_module_update_prices->clean_before_write_all();
        $this->model_extension_module_update_prices->write_updates_up_all($persent);


    }


    public function update_prices_down_all(){
        $this->load->model('extension/module/update_prices');

        if($_POST["percent"] == ""){
            exit('не введен процент');
        } else {
            echo 'Обновление цен прошло успешно.';
        }

        $persent = $_POST["percent"] / 100;

        $this->model_extension_module_update_prices->update_product_prices_down_all($persent);
        $this->model_extension_module_update_prices->write_percent_down_all($persent);



        $this->model_extension_module_update_prices->clean_before_write_all();
        $this->model_extension_module_update_prices->write_updates_down_all($persent);
    }
    public function disable_updates_all(){
        $this->load->model('extension/module/update_prices');
        $this->model_extension_module_update_prices->disable_updates_clean_table_all();
        $this->model_extension_module_update_prices->disable_persents_all();
    }
    //<all

	public function disable_updates(){
		$this->load->model('extension/module/update_prices');
		$this->model_extension_module_update_prices->disable_updates_clean_table();
		$this->model_extension_module_update_prices->disable_persents();
	}

	public function check_table(){
		$this->load->model('extension/module/update_prices');
		$data = $this->model_extension_module_update_prices->check_percents_table();
		return $data;
	}

    public function check_table_all(){
        $this->load->model('extension/module/update_prices');
        $data = $this->model_extension_module_update_prices->check_percents_table_all();
        return $data;
    }


	public function file_upload(){
		set_time_limit(300);

		$ftp_server = 'service.russvet.ru';
		$ftp_user_name = "pricat";
		$ftp_user_pass = "vFtg23x";

		$conn_id =ftp_connect($ftp_server,21021,90) or die ('connect error');
// проверка имени пользователя и пароля
		$login_result = ftp_login($conn_id,$ftp_user_name,$ftp_user_pass);
		ftp_pasv($conn_id, true);
		if ((!$conn_id) || (!$login_result)) {
			echo "error FTP connect";
			echo "Try to FTP connect $ftp_server name $ftp_user_name!";
			exit;
		} else {
			echo "connection FTP  $ftp_server name $ftp_user_name";
		}
// получить содержимое текущей директории
		$contents = ftp_nlist($conn_id, "/siberia");

		$local_file = $_SERVER['DOCUMENT_ROOT'].'/PRICAT_DOWNLOAD.xml';
		$server_file = array_pop($contents);
		if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
			echo "write $local_file\n";
		} else {
			echo "error write\n";
		}
// закрытие соединения
		ftp_close($conn_id);
	}

	public function file_prodat_upload(){
		set_time_limit(300);

		$ftp_server = 'service.russvet.ru';
		$ftp_user_name = "prodat";
		$ftp_user_pass = "bT6tsv3";

		$conn_id =ftp_connect($ftp_server,21021,90) or die ('connect error');
// проверка имени пользователя и пароля
		$login_result = ftp_login($conn_id,$ftp_user_name,$ftp_user_pass);
		ftp_pasv($conn_id, true);
		if ((!$conn_id) || (!$login_result)) {
			echo "error FTP connect";
			echo "Try to FTP connect $ftp_server name $ftp_user_name!";
			exit;
		} else {
			echo "connection FTP  $ftp_server name $ftp_user_name";
		}
// получить содержимое текущей директории
		$contents = ftp_nlist($conn_id, "/sklad/SIB");

		$local_file = $_SERVER['DOCUMENT_ROOT'].'/PRODAT_DOWNLOAD.zip';
		$server_file = array_pop($contents);
		if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
			echo "write $local_file\n";
		} else {
			echo "error write\n";
		}
		/*$zip = new ZipArchive;
		if ($zip->open('/PRODAT_DOWNLOAD.zip') === TRUE) {
			$zip->extractTo('/');
			$zip->close();
			// удача
		} else {
			// неудача
		}*/

// закрытие соединения
		ftp_close($conn_id);
	}

	public function check_status_now(){
		$this->load->model('extension/module/update_prices');
		header('Content-Type', 'application/json');
		$rows = $this->model_extension_module_update_prices->check_status();
		if(empty($rows)){
			echo '<span style="color: green">Текущих загрузок нет, можно загружать файл</span>';
		} else {
			echo '<span style="color: red">данные загружаются в базу</span>';
		}
	}

	public function data_upload(){
		$this->load->model('extension/module/update_prices');
		set_time_limit(1200);
		ini_set("memory_limit", "2056M");

		$this->load->model('setting/setting');
		//$directory = $_SERVER["DOCUMENT_ROOT"].'/IMPORT_PRICAT';
		//$file = array_pop(scandir($directory));
		//$xmlfile = $_SERVER["DOCUMENT_ROOT"].'/IMPORT_PRICAT/'.$file;
		$file = 'PRICAT_DOWNLOAD.xml';
		$xmlfile = $_SERVER["DOCUMENT_ROOT"].'/'.$file;

		$z = new XMLReader;
		$z->open($xmlfile);
		$doc = new DOMDocument;

		$exist_rows = $this->model_extension_module_update_prices->check_exist_rows();
		$data_rows = $this->model_extension_module_update_prices->check_temp_table();


		if(!empty($data_rows) && $exist_rows["count(id)"] != 0){
			return;
		}

		while ($z->read() && $z->name !== 'DocDetail');
		$id = 1;
		while ($z->name === 'DocDetail')
		{

			$node = simplexml_import_dom($doc->importNode($z->expand(), true));
			$quantity = $node->QTY;
			$SenderPrdCode = $node->SenderPrdCode; //артикуль, по нему связаны pricat и prodat
			$clean_name = str_replace(array('\'', '"'), '', $node->ProductName);
			$DocDetailOptions = $node->DocDetailOptions;

			//if($id >= $start && $id <= $finish){
			/*цена базовая*/
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "RetailPrice"){
					$RetailPrice = (float)$Value;
					//$this->db->query("INSERT INTO " . DB_PREFIX . "temp_products SET id = '" .$id. "', upc = '" . $SenderPrdCode . "', quantity = '" . $quantity . "', price = '" . $RetailPrice .  "'");
					//$this->db->query("INSERT INTO " . DB_PREFIX . "temp_products SET id = '" .$id. "', upc = '" . $SenderPrdCode . "', quantity = '" . $quantity . "', price = '" . $RetailPrice .  "', product_name = '".$clean_name."'");
				} /*else {
					continue;
				}*/

			}
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "ProductGroup"){
					$ProductGroup = str_replace(array('\'', '"'), '', $Value);
				}
			}
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "Width"){
					$Width = $Value;
				}
			}
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "Height"){
					$Height = $Value;
				}
			}
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "Weight"){
					$Weight = $Value;
				}
			}
			foreach ( $DocDetailOptions->xpath( 'DocOption' ) as $element ){
				$Name = (string)$element->Name[0];
				$Value = (string)$element->Value[0];
				if($Name == "Brand"){
					$Brand = $Value;
				}
			}
			//}
			$this->db->query("INSERT INTO " . DB_PREFIX . "temp_products SET 
					id = '" .$id. "', 
					upc = '" . $SenderPrdCode . "', 
					quantity = '" . $quantity . "', 
					price = '" . $RetailPrice .  "', 
					product_group = '".$ProductGroup."', 
					width = '".$Width."', 
					height = '".$Height."', 
					weight = '".$Weight."', 
					brand = '".$Brand."', 
					product_name = '".$clean_name."'");
			$id ++;
			$z->next('DocDetail');
		}

		return true;

	}

	public function prodat_unzip(){
		$zip = new ZipArchive;
		if ($zip->open(__DIR__ . '/PRODAT_DOWNLOAD.zip') === TRUE) {
			$zip->extractTo(__DIR__);
			$zip->close();
			echo 'удача';
		} else {
			echo 'неудача';
		}
	}


	public function prodat_import(){
//$start, $finish
		set_time_limit(1200);
		ini_set("memory_limit", "2056M");
		$xmlfile = $_SERVER["DOCUMENT_ROOT"].'/PRODAT_DOWNLOAD/PRODAT_369147_685666990.xml';
		//$directory = $_SERVER["DOCUMENT_ROOT"].'/PRODAT_DOWNLOAD';
		//$xmlfile = array_pop(scandir($directory));



		$z = new XMLReader;
		$z->open($xmlfile);
		$doc = new DOMDocument;
		//$count = 0;
		// move to the first <product /> node
		while ($z->read() && $z->name !== 'DocDetail');
		// now that we're at the right depth, hop to the next <product/> until the end of the tree
		//$id = 1;
		while ($z->name === 'DocDetail')
		{
			//$node = new SimpleXMLElement($z->readOuterXML());
			$node = simplexml_import_dom($doc->importNode($z->expand(), true));

			//echo $node->ProductName.'<br>';
			$SenderPrdCode = $node->SenderPrdCode; //артикуль, по нему связаны pricat и prodat
			$this->db->query("INSERT INTO " . DB_PREFIX . "temp_prodat SET product_code = '" .$SenderPrdCode. "'");

			//if($id >= $start && $id <= $finish) {
				//echo $node->Image->Value.'<br>';
				//$this->db->query("INSERT INTO " . DB_PREFIX . "temp_images SET id = '" .$id. "', upc = '" . $SenderPrdCode . "', img = '" . $node->Image->Value . "'");


				//echo '<img src='.$node->Image->Value.' alt=""/>';
				//foreach ( $node->FeatureETIMDetails->FeatureETIM as $key => $object ) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "temp_attributes SET id = '" .$id. "', upc = '" . $SenderPrdCode . "', attr_code = '" . $object->FeatureCode . "', attr_name = '" . $object->FeatureName .  "', attr_value = '" . $object->FeatureValue . "', attr_uom = '" . $object->FeatureUom . "'");
					//SELECT COUNT(DISTINCT upc) FROM oc_temp_attributes
					//echo $object->FeatureCode.'<br>';
					//echo $object->FeatureName.'<br>';
					//echo $object->FeatureValue.'<br>';
					//echo $object->FeatureUom . '<br>';

				//}
			//}

			//$clean_name = str_replace(array('\'', '"'), '', $node->ProductName);
			//$language_id = 1;
			//$model = str_replace(array('\'', '"'), '', $node->ProductName);
			//$image = $node->Image->Value;
			//$quantity = 1;
			//$price = 1;
			/*if($node->VendorProdNum){
				$UPC = $node->VendorProdNum;
			}*/

			//VendorProdNum
			/*foreach ($node->Weight as $key => $object){
				$weight = $object->Value;
			}*/
			//$status = 1;




			//$this->model_extension_module_xml_module->add_product_desc($id , $language_id, $clean_name);
			//$this->model_extension_module_xml_module->add_product_ids($id, $model, $UPC, $quantity, $image, $price, $weight, $status);
			//$this->model_extension_module_xml_module->add_product_img($product_image_id, $SenderPrdCode, $image);
			//$this->model_extension_module_xml_module->add_product_sku($id, $SenderPrdCode);
			//$this->model_extension_module_xml_module->add_product_to_cat_test($id, 59);
			//$this->model_extension_module_xml_module->add_product_to_store($id, 0);
			//$this->model_extension_module_xml_module->add_product_upc($id, 0);
			//$id++;
			//$product_image_id++;





			/*echo $node->EAN.'<br>';
			echo $node->SenderPrdCode.'<br>';
			echo $node->ProductName.'<br>';
			echo $node->ProductStatus.'<br>';
			echo $node->UOM.'<br>';
			echo $node->ItemsPerUnit.'<br>';
			echo $node->Multiplicity.'<br>';
			echo $node->ParentProdCode.'<br>';
			echo $node->ParentProdGroup.'<br>';
			echo $node->ProductCode.'<br>';
			echo $node->ProductGroup.'<br>';
			echo $node->VendorProdNum.'<br>';
			echo $node->Brand.'<br>';
			foreach ($node->Weight as $key => $object){
				echo $object->WeightUnit.'<br>';
				echo $object->Value.'<br>';
			}
			foreach ($node->Dimension as $key => $object){
				echo $object->DimensionUnit.'<br>';
				echo $object->Depth.'<br>';
				echo $object->Width.'<br>';
				echo $object->Height.'<br>';
			}
			foreach ($node->FeatureETIMDetails->FeatureETIM as $key => $object){

				echo $object->FeatureCode.'<br>';
				echo $object->FeatureName.'<br>';
				echo $object->FeatureValue.'<br>';
				echo $object->FeatureUom.'<br>';

			}*/
			//echo $node->Image->Value.'<br>';
			//echo '<img src='.$node->Image->Value.' alt=""/>';
			//echo $node->Image->Value;
			//echo $node->CertificateNum.'<br>';
			//die('123');
			/*echo '<pre>';
			var_dump($node->DocDetailOptions->DocOption);
			echo '</pre>';*/


			/*echo '<pre>';
			var_dump($node);
			echo '</pre>';*/
			//die('123');
			//$count++;
			//echo '<br>'.$count;
			//echo '<hr>';

			// go to next <product />
			//$id ++;
			$z->next('DocDetail');
		}

	}

	public function find_new_products_prodat(){
		$this->load->model('extension/module/update_prices');
		$new_products_prodat = $this->model_extension_module_update_prices->find_new_products_prodat();
		echo '<pre>';
		var_dump($new_products_prodat);
		echo '</pre>';
	}

	public function find_new_products_pricat(){
		$this->load->model('extension/module/update_prices');
		$new_products_pricat = $this->model_extension_module_update_prices->find_new_products_pricat();
		$product_id_arr = $this->model_extension_module_update_prices->get_last_product_id();

		$product_id = (int)$product_id_arr['MAX(`product_id`)'];

		foreach($new_products_pricat as $new_product) {
			$product_id ++;
			$image = $this->model_extension_module_update_prices->get_product_img($new_product['upc']);
			$this->model_extension_module_update_prices->insert_new_product($product_id, $new_product['upc'], $new_product['quantity'], $new_product['product_name'], $new_product['price'], $new_product['brand'], $new_product['width'], $new_product['height'], $new_product['product_group'], $image['img']);
		}
		/*echo '<pre>';
		var_dump($new_products_pricat);
		echo '</pre>'*/;

		//new products with price != 0
		$count_products = count($new_products_pricat);
		echo $count_products .' новых товаров';

	}

	public function get_new_atributes(){
		$this->load->model('extension/module/update_prices');
		$new_products_ids = $this->model_extension_module_update_prices->get_new_products_ids_for_attrs();
		if(!empty($new_products_ids)){
			foreach ($new_products_ids as $new_products_id) {
				$attrs = $this->model_extension_module_update_prices->parse_module_temp_attributes($new_products_id["upc"]);
				foreach ($attrs as $arttribute) {
					$element = $this->model_extension_module_update_prices->get_module_attr_id($arttribute['attr_name']);
					if(!empty($element) && $arttribute['attr_value'] != ''){
						$this->model_extension_module_update_prices->update_module_attributes($new_products_id["product_id"], (int)$element["attribute_id"], $arttribute['attr_value']);
					}
				}
				$this->model_extension_module_update_prices->mark_attr_added($new_products_id["product_id"]);
			}
			echo 'Атрибуты добавлены';
		} else {
			echo 'Новых атрибутов не найдено';
			return false;
		}
	}

	public function add_new_products_module(){
		$this->load->model('extension/module/update_prices');
		$new_products = $this->model_extension_module_update_prices->get_new_products_from_temp_table();
		/*echo '<pre>';
		var_dump($new_products);
		echo '</pre>';
		die('123');*/
		$count_new_products = count($new_products);
		$language_id = 1;
		$description ='';
		$store_id = 0;
		$sort_order = 0;
		//24493 deleted
		if(!empty($new_products)){
			foreach ($new_products as $new_product) {
				//die('дописать запрос'); //дописать запрос на поиск родительской категории
				//$image = $this->model_extension_module_update_prices->get_product_img($new_product['upc']);
				$category_id = $this->model_extension_module_update_prices->get_module_category_id($new_product['product_group']);
				//$parent_category_id = $this->model_extension_module_update_prices->get_module_parent_category_id($category_id);
				if($new_product['brand'] == 'LEDVANCE'){
					$new_product['brand'] = 'LEDVANCE OSRAM';
				}
				$manufacturer_id = $this->model_extension_module_update_prices->get_module_manufacturer_id($new_product['brand']);
				if($manufacturer_id["manufacturer_id"] === NULL) {
					$manufacturer_id["manufacturer_id"] = 400;
				}
				$this->model_extension_module_update_prices->add_new_products_to_oc_product($new_product['upc'], $new_product['product_name'], $new_product['quantity'], $manufacturer_id["manufacturer_id"], $new_product['price'], $new_product['width'], $new_product['height'], $new_product['weight']);

				$prod_id = $this->model_extension_module_update_prices->get_module_product_id($new_product['upc']);

				$this->model_extension_module_update_prices->add_new_products_to_oc_product_description((int)$prod_id['product_id'], $language_id, $new_product['product_name'], $description);
				$this->model_extension_module_update_prices->add_new_products_to_oc_product_to_category((int)$prod_id['product_id'], (int)$category_id);
				/*if($parent_category_id) {
					$this->model_extension_module_update_prices->add_new_products_to_oc_product_to_category($prod_id['product_id'], (int)$parent_category_id);
				}*/
				$this->model_extension_module_update_prices->add_new_products_to_oc_product_to_store((int)$prod_id['product_id'], $store_id);
				$this->model_extension_module_update_prices->add_seo_url('product_id='.$prod_id['product_id'], $this->generate_translit($new_product['product_name']));
				$this->model_extension_module_update_prices->mark_updated_module($new_product['upc']);
				//if($image['img'] != ''){
				if($new_product['image'] != ''){
					$this->model_extension_module_update_prices->add_new_products_to_oc_product_image((int)$new_product['upc'], $new_product['image'], $sort_order);
				}
			}
			echo 'Добавлено '.$count_new_products.' новых товаров';
		} else {
			echo 'Нет новых товаров';
			return false;
		}

	}

	public function generate_translit($s){
		$s = (string) $s; // преобразуем в строковое значение
		$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
		$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
		return $s.'_'.rand(1, 99); // возвращаем результат
	}


	public function import_manufacturer(){
		$this->load->language('extension/module/update_prices');
		$this->load->model('extension/module/update_prices');
	}



	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/update_prices')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
